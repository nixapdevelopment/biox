<?php

if (!function_exists('site_url'))
{
    function site_url($uri = '', $get = [], $combine_get = false, $protocol = NULL)
    {
        $ar = explode('/', $_SERVER['REQUEST_URI']);
        $ar = array_filter($ar);
        
        $langs = get_instance()->config->item('languages');
        
        if (isset($ar[1]))
        {
            foreach ($langs as $langID => $lang)
            {
                if ($ar[1] == $lang['Slug'])
                {
                    $uri = $lang['Slug'] . '/' . $uri;
                    break;
                }
            }
        }
        
        $url = get_instance()->config->site_url($uri, $protocol);
        $segments = explode('?', $url);
        
        if (count($segments) > 1)
        {
            throw new ErrorException('Set get params as second parametr in site_url()');
        }
        
        $query_string = '';
        if (count($get) > 0)
        {
            if ($combine_get)
            {
                $get = array_merge($_GET, $get);
            }
            
            foreach ($get as $key => $val)
            {
                if ($val == '')
                {
                    unset($get[$key]);
                }
            }
            
            $arr = [];
            foreach ($get as $key => $val)
            {
                if (is_array($val))
                {
                    foreach ($val as $k => $v)
                    {
                        $arr[] = $key . '[' . $k . ']=' . $v;
                    }
                }
                else
                {
                    $arr[] = $key . '=' . $val;
                }
            }

            $query_string = '?' . implode('&', $arr);
        }
        
        return $url . $query_string;
    }
}

if (!function_exists('str_to_url'))
{
    function str_to_url($str, $delimiter = '-')
    {
        $replace = [
            'ă' => 'a',
            'î' => 'i',
            'â' => 'a',
            'ț' => 't',
            'ș' => 's',
            'Ă' => 'a',
            'Î' => 'i',
            'Â' => 'a',
            'Ț' => 't',
            'Ș' => 's',
            
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            '.' => '-', ' ' =>'-'
        ];
        
        $str = str_replace(array_keys($replace), array_values($replace), $str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
}

if (!function_exists('breadscrumbs'))
{
    function breadscrumbs($data = [])
    {        
        if (empty($data))
        {
            return '';
        }
        
        $return = '<ul class="ul-inline">';
        foreach ($data as $link => $name)
        {
            if ($link == site_url())
            {
                $return .= '<li><a href="/">BIOX COMERT</a></li>';
            }
            else
            {
                if (empty($link))
                {
                    $return .= '<li>' . $name . '</li>';
                }
                else
                {
                    $return .= '<li><a href="' . $link . '">' . $name . '</a></li>';
                }
            }
        }
        
        return $return . '</ul>';
    }
}

if (!function_exists('menu'))
{
    function menu($menu_data, $class = null)
    {
        $class ? $class = " class='" . $class . "'" : "";
        $return = '';
        foreach ($menu_data as $link => $label)
        {
            $return .= '<li><a href="' . site_url($link) . '">' . $label . '</a></li>';
        }
        $return .= '';
        
        return $return;
    }
}

if (!function_exists('switchLang'))
{
    function switchLang($langSlug)
    {       
        $uri = $_SERVER['REQUEST_URI'];
        $langs = get_instance()->config->item('languages');        
        $curentLang = get_instance()->config->item('language');
        $curentLangSlug = "ro";
        
        foreach ($langs as $lID => $lVal){
            if($lVal['LangFile'] == $curentLang){
                $curentLangSlug = $lVal['Slug'];
            }
        }
        
        $clean_uri = str_replace('/'.$curentLangSlug.'/', '/', $uri);
        
        return '/'.$langSlug.$clean_uri;    
    }    
}
