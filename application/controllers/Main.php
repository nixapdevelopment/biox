<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends FrontController
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('CategoryModel');
        $this->data['categories'] = $this->CategoryModel->getTree($this->langID, true);
        $this->data['categoriesArray'] = $this->CategoryModel->getCategories($this->langID, true);        
    }

    public function index()
    {
        $this->load->model('NewsModel');
        $this->data['news'] = $this->NewsModel->getNews($this->langID, true);
        
        $this->load->model('ProjectsModel');
        $this->data['projects'] = $this->ProjectsModel->getProjects($this->langID, true);
        
        $data['slides'] = $this->db->select('*')->from('Slider')->where('Type', 'slider')->order_by('Position')->get()->result();
        $data['banners'] = $this->db->select('*')->from('Slider')->where('Type', 'banner')->order_by('Position')->limit(2)->get()->result();
        
        $data['page'] = $this->db->select('p.ID, pl.Title, pl.Text, pl.Keywords, pl.Description')
            ->from('Page as p')
            ->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT')
            ->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'", 'LEFT')
            ->where('p.ID', 1)
            ->get()->row();
         
        $this->Title = $data['page']->Title;
        $this->Keywords = $data['page']->Keywords;
        $this->Description = $data['page']->Description;
                        
        $this->render('home', $data);
    }
    
    public function login()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'required|valid_email');
        $this->form_validation->set_rules('Password', lang('Password'), 'required');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('Password', '', 'callback_do_user_login');
            
            $this->form_validation->run();
        }
        
        $this->addBreadscrumb('', lang('Login'));
        
        $this->render('login');
    }
    
    public function do_user_login()
    {
        $this->load->model('UserModel');
        
        $username = $this->input->post('UserName');
        $password = $this->input->post('Password');
        
        $user = $this->UserModel->getForLogin($username, $password);
        
        if (isset($user->ID))
        {
            $this->session->set_userdata('UserID', $user->ID);
            
            switch ($user->Type)
            {
                case 'Retail':
                    redirect(site_url('user'));
                    break;
                case 'Angro':
                    if ($user->Status == 'Active')
                    {
                        redirect(site_url('user'));
                    }
                    redirect('wait-confirmation');
                    break;
                case 'Admin':
                    redirect(site_url('admin'));
                    break;
                case 'Manager':
                    redirect(site_url('admin/index'));
                    break;
            }
        }
        
        $this->form_validation->set_message('do_user_login', lang('InvalidLogin'));
        
        return false;
    }

    public function password_recovery()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'callback_do_password_recovery');
            
            $this->form_validation->run();
        }
        
        $this->addBreadscrumb('', lang('PasswordRecovery'));
        
        $this->render('password_recovery');
    }
    
    public function do_password_recovery()
    {
        $this->load->model('UserModel');
        
        $username = $this->input->post('UserName');
        
        $user = $this->UserModel->getByUsername($username);
        
        if (isset($user->ID))
        {
            $token = md5(uniqid());
            $this->db->update('User', ['Token' => $token], ['ID' => $user->ID]);
            
            $this->load->library('email');
            $this->email->from('system@agm.md', 'AGM');
            $this->email->to($user->UserName);
            $this->email->subject(lang('RecoveryMessageSubject'));
            $this->email->message('<a href="' . site_url('reset-password', ['token' => $token]) . '">Link</a> Token: ' . $token);
            $this->email->send();
            
            redirect('reset-password');
        }
        
        $this->form_validation->set_message('do_password_recovery', lang('EmailNotRegistered'));
        
        return false;
    }
    
    public function registration()
    {
        $this->config->load('regions');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
        $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        $this->form_validation->set_rules('ClientType', lang('ClientType'), 'required');
        $this->form_validation->set_rules('Region', lang('Region'), 'required|is_natural');
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('ClientType') == 'Angro' ? 'Angro' : 'Retail';
            $address = $this->input->post('Address');
            
            if (is_null($address))
            {
                $address = $this->config->item($this->input->post('Region'), 'regions');
            }
            
            $this->db->insert('User', [
                'UserName' => $this->input->post('UserName'),
                'Password' => $this->input->post('Password'),
                'Name' => $this->input->post('Name'),
                'Type' => $clientType,
                'Status' => $clientType == 'Angro' ? 'NotConfirmed' : 'Active',
                'Region' => $this->input->post('Region'),
                'Address' => $address,
                'CompanyName' => $this->input->post('CompanyName'),
                'Phone' => $this->input->post('Phone'),
                'Zip' => $this->input->post('Zip'),
                'Activitation' => $this->input->post('Activitation'),
                'RegDate' => date('c')
            ]);
            $user_id = $this->db->insert_id();
            
            if (!empty($_FILES['Files']))
            {
                $config = array(
                    'upload_path'   => 'public/uploads/files',
                    'allowed_types' => 'pdf|doc|docx|xls|xlsx|png|jpg|gif|tiff|tif',
                    'encrypt_name' => true
                );
                $this->load->library('upload', $config);

                $files_insert = array();
                foreach ($_FILES['Files']['name'] as $key => $name)
                {
                    $_FILES['file']['name'] = $_FILES['Files']['name'][$key];
                    $_FILES['file']['type'] = $_FILES['Files']['type'][$key];
                    $_FILES['file']['tmp_name'] = $_FILES['Files']['tmp_name'][$key];
                    $_FILES['file']['error'] = $_FILES['Files']['error'][$key];
                    $_FILES['file']['size'] = $_FILES['Files']['size'][$key];

                    if ($this->upload->do_upload('file'))
                    {
                        $file_data = $this->upload->data();
                        $files_insert[] = [
                            'EntityID' => $user_id,
                            'EntityType' => 'User',
                            'Path' => $file_data['file_name'],
                            'Name' => $file_data['client_name']
                        ];
                    }
                }
                
                if (count($files_insert) > 0)
                {
                    $this->db->insert_batch('File', $files_insert);
                }
            }
            
            $this->session->set_userdata('UserID', $user_id);
            
            if ($clientType == 'Angro')
            {
                redirect('wait-confirmation');
            }
            
            redirect('user');
        }
        
        $this->addBreadscrumb('', lang('Registration'));
        
        $this->render('registration');
    }
    
    public function wait_confirmation()
    {
        $this->addBreadscrumb('', lang('WaitConfirmation'));
        
        $this->render('wait_confirmation');
    }
    
    public function logout()
    {
        $this->session->unset_userdata('UserID');
        redirect();
    }
    
    public function reset_password()
    {
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('Token', lang('RecoveryToken'), 'trim|required|callback_check_recovery_token');
        $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        
        if ($this->form_validation->run())
        {
            $token = $this->input->post('Token');
            
            $update = [];
            $update['Token'] = '';
            $update['Password'] = $this->input->post('Password');
            
            $this->db->update('User', $update, ['Token' => $token]);
            
            $this->session->set_flashdata('password_recovered', true);
            
            redirect('login');
        }
        
        $this->addBreadscrumb('', lang('ResetPassword'));
        
        $this->render('reset_password');
    }
    
    public function check_recovery_token()
    {
        $token = $this->input->post('Token');
        
        $user = $this->db->get_where('User', ['Token' => $token])->row();
        
        if (isset($user->ID))
        {
            return true;
        }
        
        $this->form_validation->set_message('check_recovery_token', lang('InvalidRecoveryToken'));
        
        return false;
    }
    
    public function subscribe()
    {
        $email = $this->input->post('Email');
        
        $subscribe = $this->db->get_where('Subscribe', ['Email' => $email], 1)->row();
        
        if (isset($subscribe->Email))
        {
            exit('<div class="text-danger">' . lang('AllreadySubscribed') . '</div>');
        }
        
        $this->db->insert('Subscribe', ['Email' => $email]);
        
        exit('<div class="text-success">' . lang('SubscribeSuccess') . '</div>');
    }
    
    public function router($link = '/', $link2 = '')
    {
        $url = $this->db->get_where('Url', ['Link' => $link], 1)->row();
        
        // has category slug
        $cat_url = [];
        if (!empty($link2))
        {
            $url = $this->db->get_where('Url', ['Link' => $link2], 1)->row();
            $cat_url = $this->db->get_where('Url', ['Link' => $link], 1)->row();
        }
        
        if (empty($url->ID))
        {
            show_404();
        }
        
        switch ($url->Type) 
        {
            case 'Page':
                $this->page($url->ObjectID);
                break;
            case 'News':
                $this->article($url->ObjectID);
                break;
            case 'Project':
                $this->projectArticle($url->ObjectID);
                break;
            case 'Product':
                $this->product($url->ObjectID, $cat_url);
                break;
            case 'Category':
                $this->category($url->ObjectID);
                break;
            default:
                show_404();
                break;
        }
    }
    
    private function page($id)
    {
        // get page
        $this->db->select('*, p.ID');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'", 'LEFT');
        $this->db->where('p.ID', $id);
        $this->db->limit(1);
        $page = $this->db->get()->row();
        
        // if page not found or not active
        if (empty($page->ID) || $page->Status != 'Active')
        {
            show_404();
        }
        
        $this->Title = $page->Title;
        $this->Keywords = $page->Keywords;
        $this->Description = $page->Description;
        
        $data['page'] = $page;
        
        
        // get images
        $data['images'] = $this->db->get_where('PageImage', ['PageID' => $page->ID])->result();
        
        // get files
        $data['files'] = $this->db->get_where('File', ['EntityID' => $page->ID, 'EntityType' => 'Page'])->result();
        
        $this->addBreadscrumb('', $page->Title);
        
        switch ($page->Template)
        {
            case 'home';
                $this->index($data);
                break;
            case 'catalog';
                $this->catalog($data);
                break;
            case 'news';                
                $this->news($data);
                break;
            case 'projects';                
                $this->projects($data);
                break;
            case 'contacts';
                $this->contacts($data);
                break;
            case 'search';
                $this->search();
                break;
            default:
                $this->render('page', $data);
                break;
        }
    }
    

//    private function search(){       
//        $data['search'] = !empty($_POST['q']) ? $_POST['q'] : (!empty($_GET['q']) ? $_GET['q'] : redirect());
//        $data['products_per_page'] = (int) $this->db->get_where("Settings", ['ID' => 1])->row("Value");
//                
//        // get products
//        $this->db->select('p.ID, p.IsPromo, p.DiscountPriceStart, p.DiscountPriceEnd, p.DiscountPrice, p.Price');
//        $this->db->from('Product as p');
//        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'LEFT');
//        $this->db->where('p.Status', 'Active');
//        $this->db->where_in('p.Type', ['All', $this->shopType]);
//        $this->db->group_start();
//        $this->db->like('pl.Name', $data['search']);
//        $this->db->or_like('p.Sku', $data['search']);
//        $this->db->or_like('p.Part_nr', $data['search']);
//        $this->db->or_like('p.Part_nr_orig', $data['search']);
//        $this->db->group_end();
//      
//        $res = $this->db->get()->result();
//               
//                
//        $this->addBreadscrumb('', lang("Search"));        
//        $this->render('search', $data);        
//    }

    private function news($data)
    {
        $limit = (int)$this->data['settings']['news_per_page'];
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = current_url();
        $config['total_rows'] = $this->db->count_all_results('News');
        $config['per_page'] = $limit ? $limit : 9;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->model('NewsModel');
        $data['last_news'] = $this->NewsModel->getLast($this->langID, $limit, $offset);
                        
        $this->render('news', $data);
    }
        
    
        private function article($id)
    {
        $this->load->model('NewsModel');
        $data['NewsArticle'] = $this->NewsModel->getByID($id, $this->langID);
        $data['NewsImages'] = $this->NewsModel->getImagesByNewsID($id);
        $data['NewsFiles'] = $this->NewsModel->getFilesByNewsID($id);
        
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID);
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'");
        $this->db->where('p.ID', 3);
        $news_page = $this->db->get()->row();
        
        $this->addBreadscrumb($news_page->Link, $news_page->Title);
        $this->addBreadscrumb('', $data['NewsArticle']->Title);
        
        $this->Title = $data['NewsArticle']->Title;
        $this->Keywords = $data['NewsArticle']->Keywords;
        $this->Description = $data['NewsArticle']->Description;
        
        $this->render('news_item', $data);
    }
    
        private function projects($data)
    {
        $limit = (int)$this->data['settings']['news_per_page'];
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = current_url();
        $config['total_rows'] = $this->db->count_all_results('News');
        $config['per_page'] = $limit ? $limit : 9;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->model('ProjectsModel');
        $data['last_projects'] = $this->ProjectsModel->getLast($this->langID, $limit, $offset);
                        
        $this->render('projects', $data);
    }
    
       
    
    private function projectArticle($id)
    {
        $this->load->model('ProjectsModel');
        $data['ProjectArticle'] = $this->ProjectsModel->getByID($id, $this->langID);
        $data['ProjectImages'] = $this->ProjectsModel->getImagesByProjectID($id);
        $data['ProjectFiles'] = $this->ProjectsModel->getFilesByProjectID($id);
        
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID);
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'");
        $this->db->where('p.ID', 3);
        $project_page = $this->db->get()->row();
        
        $this->addBreadscrumb($project_page->Link, $project_page->Title);
        $this->addBreadscrumb('', $data['ProjectArticle']->Title);
        
        $this->Title = $data['ProjectArticle']->Title;
        $this->Keywords = $data['ProjectArticle']->Keywords;
        $this->Description = $data['ProjectArticle']->Description;
        
        $this->render('project_item', $data);
    }
    
    private function category($id)
    {
        $this->load->model('CategoryModel');
        
        // get category
        $data['category'] = $this->CategoryModel->getByID($id, $this->langID);
        
        // get subcategories
        $data['subcategories'] = $this->CategoryModel->getByParent($id, $this->langID);
        
        // menu
        //$data['categories_menu'] = $this->CategoryModel->getTree($this->langID, true, $data['category']->ID);
        $data['categories'] = $this->CategoryModel->getCategories($this->langID);
        
        // get category products
        $this->db->select("*, p.ID as ID, 
        CASE d.PromoType 
            WHEN 'Percent' THEN ROUND(p.Price - (p.Price * d.Value / 100), 2) 
            WHEN 'Amount' THEN ROUND(p.Price - d.Value, 2) 
            WHEN 'Price' THEN ROUND(d.Value, 2)
            ELSE ROUND(p.Price, 2)
        END AS `ProductPrice`");
        $this->db->from('Product as p');
        $this->db->join("Discount as d", "d.EntityID = p.ID and d.EntityType = 'Product' and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1", 'LEFT');
        $this->db->where('p.CategoryID', $id);
        $this->db->where('p.Status', 'Active');
        $this->db->where_in('p.Type', ['All', $this->shopType]);
        $res = $this->db->get()->result();
               
        $this->addBreadscrumb('', $data['category']->Name);
        
        $this->Title = $data['category']->Title;
        $this->Keywords = $data['category']->Keywords;
        $this->Description = $data['category']->Description;
        
        $this->render('category', $data);
    }
    
    
    public function ajaxProducts()
    {
        if (!$this->input->is_ajax_request()) exit;
        
        $categoryID = $this->input->post('categoryID');
        
        $where_arr = [0];
        parse_str($this->input->post('query'), $where_arr);
                
        $products_per_page = (int)$this->data['settings']['products_per_page'];
        $limit = $products_per_page ? $products_per_page : 18;
        $offset = ((int)$where_arr['page'] - 1) * $limit;
        
        $where_category='';
        if($categoryID == 1 || $categoryID == 3) {
            $where_category = " WHERE c.ParentID = ".$categoryID;
            $categoryID=false;
        }
                
        
        $SUPER_QUERY = "
            SELECT SQL_CALC_FOUND_ROWS count(p.ID) AS `count`, p.ID, p.ID as ProductID, u.Link, pl.Name, pi.Thumb, p.CategoryID, p.Status AS ProductStatus, cu.Link as CategoryLink
                FROM Product AS p 
            LEFT JOIN ProductLang AS pl ON pl.ProductID = p.ID AND pl.LangID = $this->langID 
            LEFT JOIN ProductImage AS pi ON pi.ProductID = p.ID AND pi.IsMain = 1 
            LEFT JOIN Category AS c ON c.ID = p.CategoryID
            LEFT JOIN Url AS u ON u.ObjectID = p.ID AND u.`Type` = 'Product' 
            LEFT JOIN Url AS cu ON cu.ObjectID = p.CategoryID AND cu.`Type` = 'Category'            
            $where_category        
            GROUP BY p.ID 
            HAVING p.`Status` = 'Active' " . ($categoryID ? " AND p.CategoryID={$categoryID}" : "") . "        
            LIMIT $limit 
            OFFSET $offset
        ";
        
//        exit($SUPER_QUERY);
        
        $products = $this->db->query($SUPER_QUERY)->result();
        
        $count_all = $this->db->query('select FOUND_ROWS() as `all`')->row()->all;
        
        echo $this->load->view('ajax_products', [
            'products' => $products,
            'count' => $count_all,
            'page' => $where_arr['page'],
            'per_page' => $limit
        ], true);
    }

    private function product($id, $cat_url)
    {
        $this->load->model('ProductModel');
        $product = $this->ProductModel->getByID($id, $this->langID);
                                
        if (empty($product->ProductID)) redirect();
                
        // category
        $this->load->model('CategoryModel');
        $category = $this->CategoryModel->getByID($cat_url->ObjectID, $this->langID);
        
        // images
        $this->db->order_by('IsMain', 'DESC');
        $images = $this->db->get_where('ProductImage', ['ProductID' => $product->ProductID])->result();
             
        
        $this->addBreadscrumb(site_url($cat_url->Link), $category->Name);
        $this->addBreadscrumb('', $product->Name);
        
        $this->Title = $product->Name;
        $this->Keywords = $product->Keywords;
        $this->Description = $product->Description;
        
        $this->render('product', [
            'product' => $product,
            'category' => $category,
            'images' => $images
        ]);
    }
    
   
    public function catalog($data)
    {
        $this->db->select('c.ID, cl.Name, c.ParentID, u.Link, c.Image');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID =' . $this->langID);
        $this->db->join('Url as u', "u.ObjectID = c.ID and u.`Type` = 'Category'");
        //$this->db->where('c.ParentID', 0);
        $this->db->order_by('cl.Name');
        $categories = $this->db->get()->result_array();
        
        $this->render('catalog', [
            'categories' => $categories
        ]);
    }
    
    
    public function contacts($data)
    {
        $this->addBreadscrumb(site_url(""), lang('Contacts'));
        
        $this->render('contacts', $data);
    }
    
//    public function new_products($data)
//    {
//        $this->load->model('ProductModel');
//        
//        $limit = 9;
//        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
//        
//        $data['products'] = $this->ProductModel->getLastProducts($this->langID, $limit, $offset);
//        
//        $total_products = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
//        
//        $this->load->library('pagination');
//        $config['base_url'] = site_url($data['page']->Link, [], true);
//        $config['total_rows'] = $total_products;
//        $config['per_page'] = $limit;
//        $config['use_page_numbers'] = true;
//        $config['page_query_string'] = true;
//        $config['query_string_segment'] = 'page';
//        $config['full_tag_open'] = "<ul class='pagination'>";
//        $config['full_tag_close'] ="</ul>";
//        $config['num_tag_open'] = '<li>';
//        $config['num_tag_close'] = '</li>';
//        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
//        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
//        $config['next_tag_open'] = "<li>";
//        $config['next_tag_close'] = "</li>";
//        $config['prev_tag_open'] = "<li>";
//        $config['prev_tag_close'] = "</li>";
//        $config['first_tag_open'] = "<li>";
//        $config['first_tag_close'] = "</li>";
//        $config['last_tag_open'] = "<li>";
//        $config['last_tag_close'] = "</li>";
//        $config['num_links'] = 4;
//        $get = $this->input->get();
//        unset($get['page']);
//        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
//        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
//        $this->pagination->initialize($config); 
//        $data['pagination'] = $this->pagination->create_links();
//        
//        $this->render('new_products', $data);
//    }
    
//    public function feedback()
//    {
//        $Name = $this->input->post('Name');
//        $Email = $this->input->post('Email');
//        $Phone = $this->input->post('Phone');
//        $Message = $this->input->post('Message');
//        
//        $feedback_email = $this->data['settings']['email_feedback'];
//        
//        $email_message = "
//            Nume: $Name<br>
//            Email: $Email<br>
//            Telefon: $Phone<br>
//            Mesaj: $Message
//        ";
//        
//        $this->load->library('email', ['mailtype' => 'html']);
//        $this->email->to($feedback_email); 
//        $this->email->subject('Feedback ' . site_url());
//        $this->email->message($email_message);
//        $this->email->send();
//        
//        $this->db->insert('Feedback', [
//            'Name' => $Name,
//            'Email' => $Email,
//            'Phone' => $Phone,
//            'Message' => strip_tags($Message)
//        ]);
//    }
    
    

    
}