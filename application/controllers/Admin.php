<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController
{
    
    public $menuType = '';
    
    public function __construct()
    {
        parent::__construct();
        
        $this->menuType = $this->_user->Type;
    }

    public function index()
    {
        $data['product_count'] = $this->db->where('Status <>', 'Deleted')->get('Product')->num_rows();
        $data['category_count'] = $this->db->where('Status <>', 'Deleted')->get('Category')->num_rows();
        $data['client_count'] = $this->db->where('Status <>', 'Deleted')->where_in('Type', ['Angro', 'Retail'])->get('User')->num_rows();
        
        $this->db->select('*, o.ID');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID');
        
        if ($this->_user->Type == 'Manager')
        {
            $this->db->where('od.Region', $this->_user->Region);
        }
        
        $this->db->order_by('o.Date', 'DESC');
        $this->db->limit(10);
        $data['orders'] = $this->db->get()->result();
        
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('Feedback', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Feedback a fost eliminat</div>');
            redirect(site_url('admin/feedback'));
        }
        
        $data['feedbacks'] = $this->db->select('*')->order_by('Date', 'DESC')->get('Feedback')->result();
        
        $this->render('admin/index', $data);
    }
    
    public function categories()
    {
        $nodeList = array();
        $tree     = array();

        $categories = $this->db->simple_query("select c.ID, c.ParentID, cl.Name from `Category` as c left join `CategoryLang` as cl on cl.LangID = " . $this->langID . " and c.ID = cl.CategoryID");
        
        foreach ($categories as $row)
        {
            $nodeList[$row['ID']] = array_merge($row, array('children' => array()));
        }

        foreach ($nodeList as $nodeId => &$node)
        {
            if (!$node['ParentID'] || !array_key_exists($node['ParentID'], $nodeList))
            {
                $tree[] = &$node;
            }
            else
            {
                $nodeList[$node['ParentID']]['children'][] = &$node;
            }
        }
        unset($node);
        unset($nodeList);
        
        $tree = $this->draw_tree($tree);
        
        if ($this->input->is_ajax_request())
        {
            exit($tree);
        }
        
        $this->render('admin/categories', [
            'tree' => $tree
        ]);
    }

    private function draw_tree($tree)
    {
        $return = '<ul>';
        foreach ($tree as $row)
        {
            $return .= '<li cat-id="' . $row['ID'] . '">';
            $return .= $row['Name'];
            if (!empty($row['children']))
            {
                $return .= $this->draw_tree($row['children']);
            }
            $return .= '</li>';
        }
        $return .= '</ul>';
        return $return;
    }

    public function get_category_form()
    {
        $this->load->helper('form');
        
        $categoryID = (int)$this->input->post('categoryID');
        $parentID = (int)$this->input->post('parentID');
        
        $data = [];
        $data['parentID'] = $parentID;
        $data['categoryID'] = $categoryID;
        $data['category_link'] = '';
        
        if ($categoryID > 0)
        {
            // get category
            $this->db->select('*');
            $this->db->from('Category');
            $this->db->where('ID', $categoryID);
            $this->db->where('Status !=', 'Deleted');
            $data['category'] = $this->db->get()->row();
            
            // get category langs
            $this->db->select('*');
            $this->db->from('CategoryLang');
            $this->db->where('CategoryID', $categoryID);
            $res = $this->db->get()->result();
            
            $data['category_langs'] = [];
            foreach ($res as $row)
            {
                $data['category_langs'][$row->LangID] = $row;
            }
            
            // get category url
            $url = $this->db->get_where('Url', ['Type' => 'Category', 'ObjectID' => $categoryID], 1)->row();
            $data['category_link'] = isset($url->Link) ? $url->Link : '';
        }
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][0] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }

        if ($categoryID > 0)
        {
            unset($data['all_categories'][$categoryID]);
        }
        
        echo $this->load->view('admin/category_form', $data, true);
    }
    
    public function save_category()
    {
        // get data
        $categoryID = $this->input->post('CategoryID');
        $parentID = $this->input->post('ParentID');
        $names = $this->input->post('Name');
        $titles = $this->input->post('Title');
        $texts = $this->input->post('Text');
        $keywords = $this->input->post('Keywords');
        $descriptions = $this->input->post('Description');
        $link = $this->input->post('Link');
        $status = $this->input->post('Status') == 'Disabled' ? 'Disabled' : 'Active';
        $type = $this->input->post('Type');
                //print_r($_POST); exit;
        // save category
        $category = [
            'ParentID' => $parentID,
            'Status' => $status,
            'Type' => $type
        ];
        
        // upload category image
        $config['upload_path'] = 'public/uploads/categories';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']	= '10000';
        $config['max_width']  = '4000';
        $config['max_height']  = '4000';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload('Image'))
        {
            $file = $this->upload->data();
            $category['Image'] = $file['file_name'];
        }
        
        if ($categoryID > 0)
        {
            $this->db->update('Category', $category, ['ID' => $categoryID]);
        }
        else
        {
            $this->db->insert('Category', $category);
            $categoryID = $this->db->insert_id();
        }
        
        // save url
        if (empty($link))
        {
            $link = str_to_url($names[1]);
        }
        
        $this->db->select('*');
        $this->db->from('Url');
        $res = $this->db->get()->result_array();
        
        $all_urls = [];
        $all_urls_ot = [];
        foreach ($res as $row)
        {
            $all_urls[$row['Link']] = $row;
            $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
        }
        
        if (isset($all_urls[$link]))
        {
            if ($all_urls[$link]['ObjectID'] != $categoryID || $all_urls[$link]['Type'] != 'Category')
            {
                $i = 1;
                $str = $link;
                $links = array_keys($all_urls);
                while (in_array($str, $links))
                {
                    $str .= '-' . $i;
                    $i++;
                }
                $link .= '-' . $i;
            }
        }
        
        if (isset($all_urls_ot['Category'][$categoryID]))
        {
            $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Category'][$categoryID]['ID']]);
        }
        else
        {
            $this->db->insert('Url', [
                'Link' => $link,
                'ObjectID' => $categoryID,
                'Type' => 'Category'
            ]);
        }
        
        // save category langs
        $this->db->delete('CategoryLang', ['CategoryID' => $categoryID]);
        
        $insert = [];
        foreach ($names as $langID => $name)
        {
            $insert[] = [
                'CategoryID' => $categoryID,
                'LangID' => $langID,
                'Name' => $names[$langID],
                'Title' => $titles[$langID],
                'Text' => $texts[$langID],
                'Keywords' => $keywords[$langID],
                'Description' => $descriptions[$langID]
            ];
        }
        $this->db->insert_batch('CategoryLang', $insert);
        
        exit(json_encode([
            'categoryId' => $categoryID,
            'ParentID' => $parentID,
            'insets' => $insert
        ]));
    }

    public function delete_category()
    {
        $categoryID = (int)$this->input->post('categoryID');
        
        $this->db->delete('Category', [
            'ID' => $categoryID
        ]);
        
        $this->db->delete('CategoryLang', [
            'CategoryID' => $categoryID
        ]);
        
        $this->db->delete('Url', [
            'ObjectID' => $categoryID,
            'Type' => 'Category'
        ], 1);
        
        $this->delete_subcategories($categoryID);
    }
    
    private function delete_subcategories($parentID)
    {
        $subcategories = $this->db->get_where('Category', ['ParentID' => $parentID])->result();
        
        $this->db->delete('Category', [
            'ParentID' => $parentID
        ]);
        
        if (count($subcategories) > 0)
        {
            foreach ($subcategories as $subcat)
            {
                $this->delete_subcategories($subcat->ID);
                $this->db->delete('CategoryLang', [
                    'CategoryID' => $subcat->ID
                ]);
                $this->db->delete('Url', [
                    'ObjectID' => $subcat->ID,
                    'Type' => 'Category'
                ], 1);
            }
        }
    }
    
    public function filters()
    {
        $this->load->helper('form');
        
        if (isset($_GET['delId']))
        {
            $this->db->delete('Filter', ['ID' => (int)$_GET['delId']]);
            $this->db->delete('FilterLang', ['FilterID' => (int)$_GET['delId']]);
            
            $this->session->set_flashdata('success', lang('FilterDeleted'));
            
            redirect('admin/filters');
        }
        
        if (count($_POST) > 0)
        {
            $names = $this->input->post('Name');
            $type = $this->input->post('Type');
            $filterID = (int)$this->input->post('FilterID');
            
            if ($filterID > 0)
            {
                $this->db->update('Filter', ['Type' => $type], ['ID' => $filterID]);
                $this->db->delete('FilterLang', ['FilterID' => $filterID]);
            }
            else
            {
                $this->db->insert('Filter', [
                    'Type' => $type
                ]);
                $filterID = $this->db->insert_id();
            }
            
            $insert = [];
            foreach ($names as $langID => $name)
            {
                $insert[] = [
                    'FilterID' => $filterID,
                    'LangID' => $langID,
                    'Name' => $name
                ];
            }
            $this->db->insert_batch('FilterLang', $insert);
            
            $this->session->set_flashdata('success', lang('FilterSaved'));
            
            redirect('admin/filters');
        }
        
        $this->db->select('*');
        $this->db->from('Filter as f');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID');
        $this->db->order_by('f.ID');
        $res = $this->db->get()->result();
        
        $data['filters'] = [];
        foreach ($res as $row)
        {
            $data['filters'][$row->ID]['Name'][$row->LangID] = $row->Name;
            $data['filters'][$row->ID]['Type'] = $row->Type;
            $data['filters'][$row->ID]['System'] = $row->System;
        }
        
        if (!empty($_GET['id']))
        {
            $id = (int)$_GET['id'];
            $data['filter'] = $data['filters'][$id];
        }
        
        $this->render('admin/filters', $data);
    }
    
    public function products()
    {
        $this->load->helper('form');
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][0] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }
        
        $limit = 10;
        $page = $this->input->get('page');
        $categories = $this->input->get('Category[]');
        $name = $this->input->get('Name');
        $sort = $this->input->get('Sort');
        $is_promo = $this->input->get('IsPromo');
        $sku = $this->input->get('Sku');
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * $limit : 0;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, p.ID as ID, d.ID as DiscountID, pl.ProductID as ProductID', false);
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
        $this->db->join('Discount as d', 'd.EntityID = p.ID and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1', 'LEFT');
        
        if (!empty($categories))
        {
            $this->db->where_in('p.CategoryID', $categories);
        }
        
        if (!empty($name))
        {
            $this->db->like('pl.Name', $name);
        }
        
        if (!empty($sort))
        {
            switch ($sort)
            {
                case 'az':
                    $this->db->order_by('pl.Name asc');
                    break;
                case 'za':
                    $this->db->order_by('pl.Name desc');
                    break;
                case 'price_d':
                    $this->db->order_by('ProductPrice desc');
                    break;
                case 'price_a':
                    $this->db->order_by('ProductPrice asc');
                    break;
                default:
                    $this->db->order_by('ID asc');
                    break;
            }
        }
        
        if (!empty($is_promo))
        {
            $this->db->where('d.ID IS NOT NULL', null, false);
        }
        
        if (!empty($sku))
        {
            $this->db->like('Sku', $sku, 'after');
        }
        
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result();
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/products');
        $config['total_rows'] = $this->db->select('FOUND_ROWS() AS `all`', false)->get()->row()->all;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $data['products'] = [];
        foreach ($products as $product)
        {
            $data['products'][$product->ID] = $product;
        }
        
        $this->render('admin/products', $data);
    }
    
    public function delete_product()
    {
        $id = $this->input->get('id');
        
        $this->db->delete('Product', ['ID' => $id], 1);
        $this->db->delete('ProductLang', ['ProductID' => $id]);
        $this->db->delete('Discount', ['EntityID' => $id, 'EntityType' => 'Product']);
        $this->db->delete('ProductImage', ['ProductID' => $id]);
        $this->db->delete('ProductFilter', ['ProductID' => $id]);
        $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'Product'], 1);
        
        $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Produs a fost eliminat</div>');
        redirect(site_url('admin/products', $_GET, true));
    }

    public function update_product_price()
    {
        $pID = $this->input->post('pid');
        $price = $this->input->post('price');
        $price = $price > 0 ? $price : 0;
        
        $this->db->update('Product', ['Price' => $price], ['ID' => $pID], 1);
    }
    
    public function update_product_stock()
    {
        $pID = $this->input->post('pid');
        $stock = (int)$this->input->post('stock');
        $stock = $stock > 0 ? $stock : 0;
        
        $this->db->update('Product', ['Stock' => $stock], ['ID' => $pID], 1);
    }
    
    public function edit_product()
    {
        $id = (int)$this->input->get('id');
        
        $this->load->helper('form');
        $this->config->load('promotion_types');
        
        if (count($_POST) > 0)
        {
            $category_id = $this->input->post('CategoryID');
            $link = $this->input->post('Link');
            $sku = $this->input->post('Sku');
            $analog = $this->input->post('AnalogGroup');
            $partnr = $this->input->post('Part_nr');
            $partnr_orig = $this->input->post('Part_nr_orig');
            $status = $this->input->post('Status');
            $type = $this->input->post('Type');
            $names = $this->input->post('Name');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $texts = $this->input->post('Text');
            $price = $this->input->post('Price');
            $price1 = $this->input->post('Price1');
            $price2 = $this->input->post('Price2');
            $price3 = $this->input->post('Price3');
            $price4 = $this->input->post('Price4');
            $price5 = $this->input->post('Price5');
            $price6 = $this->input->post('Price6');
            $price7 = $this->input->post('Price7');
            $price8 = $this->input->post('Price8');
            $price9 = $this->input->post('Price9');
            $price10 = $this->input->post('Price10');
            $price2p = $this->input->post('Price2p');
            $stock = $this->input->post('Stock');
            $is_promo = $this->input->post('IsPromo') == 1 ? 1 : 0;
            $discount_price = $this->input->post('DiscountPrice');
            $discount_price_start = $this->input->post('DiscountPriceStart');
            $discount_price_end = $this->input->post('DiscountPriceEnd');
            $primary_image = $this->input->post('PrimaryImage');
            $filter_values = $this->input->post('filter_value');
            $auto_values = $this->input->post('auto_value');            
                        
            if (empty($link))
            {
                $link = str_to_url($names[1]);
            }
            
            // save product
            $product = [
                'CategoryID' => $category_id,
                'Sku' => $sku,
                'AnalogGroup' => $analog,
                'Part_nr' => $partnr,
                'Part_nr_orig' => $partnr_orig,
                'Stock' => $stock,
                'Price' => $price,
                'Price_cat_1' => $price1,
                'Price_cat_2' => $price2,
                'Price_cat_3' => $price3,
                'Price_cat_4' => $price4,
                'Price_cat_5' => $price5,
                'Price_cat_6' => $price6,
                'Price_cat_7' => $price7,
                'Price_cat_8' => $price8,
                'Price_cat_9' => $price9,
                'Price_cat_10' => $price10,
                'Price_2_percent' => $price2p,
                'DiscountPrice' => $discount_price,
                'DiscountPriceStart' => date('c', strtotime($discount_price_start)),
                'DiscountPriceEnd' => date('c', strtotime($discount_price_end)),
                'IsPromo' => $is_promo,
                'Type' => $type,
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('Product', $product, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Product', $product);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('ProductLang', ['ProductID' => $id]);
            
            $insert = [];
            foreach ($names as $langID => $name)
            {
                $insert[] = [
                    'ProductID' => $id,
                    'LangID' => $langID,
                    'Name' => $names[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID],
                    'Text' => $texts[$langID]
                ];
            }
            $this->db->insert_batch('ProductLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Product')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $id;
                }
            }

            if (isset($all_urls_ot['Product'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Product'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'Product'
                ]);
            }
            
            // save filters
            $this->db->delete('ProductFilter', ['ProductID' => $id]);
            
            if (!empty($filter_values))
            {
                $insert = [];
                foreach ($filter_values as $filterID => $filterData)
                {
                    $this->db->insert('ProductFilter', [
                        'ProductID' => $id,
                        'FilterID' => $filterID
                    ]);
                    $productFilterID = $this->db->insert_id();
                    foreach ($this->config->item('languages') as $langID => $lang)
                    {
                        $insert[] = [
                            'ProductFilterID' => $productFilterID,
                            'LangID' => $langID,
                            'Value' => !empty($filter_values[$filterID][$langID]) ? $filter_values[$filterID][$langID] : $filter_values[$filterID][1]
                        ];
                    }
                }
                $this->db->insert_batch('ProductFilterLang', $insert);
            }
            
            if(!empty($auto_values))
            {
                $insert = [];
                $insertA = [];
                $insertM = [];
                foreach ($auto_values['mark'] as $aID => $aVal)
                {   
                    if(!$aVal) continue;
                    $insertA["_".$aID] = [
                        'productID' => $id,
                        'markID' => $aVal
                    ];                                        
                }
                foreach ($auto_values['model'] as $mID => $mVal)
                {   
                    if(!$mVal) continue;
                    $insertM["_".$mID] = [
                        'modelID' => $mVal
                    ];                                                                            
                }
                 
                $insert = array_merge_recursive($insertM, $insertA);
                $this->db->delete('ProductAuto', ['productID' => $id]);
                $this->db->insert_batch('ProductAuto', $insert);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('ProductImage', ['IsMain' => 0], ['ProductID' => $id]);
                $this->db->update('ProductImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/products',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/products/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'ProductID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('ProductImage', $insert);
                }
            }
            
            // save discounts
            $this->db->delete('Discount', ['EntityID' => $id, 'EntityType' => 'Product']);
            
            if (!empty($_POST['Discount']))
            {
                $discount_insert = [];
                foreach ($_POST['Discount']['StartDate'] as $key => $val)
                {
                    $discount_insert[] = [
                        'EntityID' => $id,
                        'EntityType' => 'Product',
                        'PromoType' => $_POST['Discount']['PromoType'][$key],
                        'Value' => $_POST['Discount']['Value'][$key],
                        'StartDate' => date('c', strtotime($_POST['Discount']['StartDate'][$key])),
                        'EndDate' => date('c', strtotime($_POST['Discount']['EndDate'][$key])),
                        'PromoStatus' => $_POST['Discount']['PromoStatus'][$key]
                    ];
                }
                $this->db->insert_batch('Discount', $discount_insert);
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('ProductSaved') . '</div>');
                
            $red = site_url('admin/edit_product', ['id' => $id], true);
            redirect($red);
        }
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][''] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }
        
        // get all filters
        $this->db->select('f.ID, fl.Name');
        $this->db->from('Filter as f');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID and fl.LangID = ' . $this->langID);
        $this->db->where_in('f.Type', ['Number', 'String']);
        $this->db->order_by('fl.Name');
        $res = $this->db->get();
        
        $data['all_filters'] = [];
        foreach ($res->result() as $row)
        {
            $data['all_filters'][$row->ID] = $row->Name;
        }
        
        $data['product'] = [];
        $data['images'] = [];
        $data['filters'] = [];
        $data['product_filters'] = [];
        $data['product_filter_values'] = [];
        $data['product_autos'] = [];
        $data['product_models'] = [];
        $data['all_autos'] = [];
        $data['product_link'] = '';
        $data['discounts'] = [];
        
        if ($id > 0)
        {
            // get product
            $this->db->select('*');
            $this->db->from('Product');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $data['product'] = $this->db->get()->row();
            
            // get product langs
            $this->db->select('*');
            $this->db->from('ProductLang');
            $this->db->where('ProductID', $id);
            $res = $this->db->get()->result();
            
            $data['product_langs'] = [];
            foreach ($res as $row)
            {
                $data['product_langs'][$row->LangID] = $row;
            }
            
            // get product url
            $url = $this->db->get_where('Url', ['Type' => 'Product', 'ObjectID' => $id], 1)->row();
            $data['product_link'] = isset($url->Link) ? $url->Link : '';
            
            // get images
            $this->db->select('*');
            $this->db->from('ProductImage');
            $this->db->where('ProductID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get product filters
            $this->db->select('f.ID, pfl.LangID, pfl.Value');
            $this->db->from('ProductFilter as pf');
            $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
            $this->db->join('Filter as f', 'f.ID = pf.FilterID');
            $this->db->join('FilterLang as fl', 'f.ID = fl.FilterID and fl.LangID = ' . $this->langID);
            $this->db->where('pf.ProductID', $id);
            $this->db->order_by('fl.Name');
            $res = $this->db->get();

            foreach ($res->result() as $row)
            {
                $data['product_filters'][$row->ID][$row->LangID] = $row->Value;
            }
            
            // get filter values
            if (count($data['product_filters']) > 0)
            {
                $this->db->select('pfl.Value, pfl.LangID, pf.FilterID');
                $this->db->from('ProductFilter as pf');
                $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
                $res = $this->db->get()->result();
                
                foreach ($res as $row)
                {
                    $data['product_filter_values'][$row->FilterID][$row->LangID][$row->Value] = $row->Value;
                }
            }
            
            // get product auto
            
            $data['product_autos'] =   $this->db->select('pa.*, mdl.name as model, mdl.ID as modelID, mdl.start, mdl.end')
                                                ->from('ProductAuto as pa')
                                                ->join('AutoMarks as mrk', 'mrk.ID = pa.markID')
                                                ->join('AutoModels as mdl', 'mdl.ID = pa.modelID')
                                                ->where('pa.productID', $id)
                                                ->order_by('mrk.name')
                                                ->get()
                                                ->result();
            
            $marks=[];
            foreach($data['product_autos'] as $paID => $paVal){
                $marks[] = $paVal->markID;
            }
            
            if(count($marks) > 0){
                $res = $this->db->select('*')->from('AutoModels')->where_in('markID', $marks)->order_by('start')->get();
                foreach ($res->result() as $row)
                {
                   $data['product_models'][$row->markID][$row->ID] = $row->name." (".$row->start." - ".$row->end.")";
                }
            }            
            
            
            $res = $this->db->select('*')->from('AutoMarks')->order_by('name', 'ASC')->get();
            foreach ($res->result() as $row)
            {
               $data['all_autos'][$row->ID] = $row->name;
            }
                        
            // get product discounts
            $data['discounts'] = $this->db->get_where('Discount', ['EntityID' => $id, 'EntityType' => 'Product'])->result();
        }
        
        $this->render('admin/edit_product', $data);
    }
    
    public function product_filter_select()
    {
        $filter_id = $this->input->post('id');
        
        $filter = $this->db->get_where('Filter', ['ID' => $filter_id], 1)->row();
        
        $this->db->select('pfl.LangID, pfl.Value');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
        $this->db->where('pf.FilterID', $filter_id);
        $this->db->group_by('pfl.Value');
        $res = $this->db->get()->result();
        
        $values = [];
        foreach ($res as $row)
        {
            $values[$row->LangID][] = $row->Value;
        }
        
        $str = "";
        foreach ($this->config->item('languages') as $langID => $lang)
        {
            $str .= " <div class=\"col-md-2 filter-value-wrap\">"
                     . "<div class=\"input-group\">
                            <span class=\"input-group-addon\">" . $lang['Slug'] . "</span>
                            <select lang-id=\"$langID\" " . ($filter->Type == 'Number' && $langID != 1 ? 'disabled' : '') . " id=\"filter-select-$filter_id-$langID\" type=\"text\" style=\"width: 100%;\" class=\"form-control filter-value-select\" multiple name=\"filter_value[$filter_id][$langID]\" placeholder=\"Value\">
                    ";
            
            if (isset($values[$langID]))
            {
                foreach ($values[$langID] as $val)
                {
                    $str .= "<option value=\"$val\">$val</option>";
                }
            }
            
            $str .= "
                            </select>
                        </div>"
                    . "</div>";
        }
        
        echo $str;
    }
    
    
    public function delete_product_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('ProductImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_news_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('NewsImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_project_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('ProjectsImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_page_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('PageImage', ['ID' => $imageID]);
        }
    }
    
    public function filter_search()
    {
        $name = $this->input->get('q');
        
        exit(json_encode([
            [
                'id' => 20,
                'text' => 20
            ]
        ]));
    }
    
    public function news()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/news');
        $config['total_rows'] = $this->db->count_all_results('News');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('n.ID, nl.Title, nl.Text, n.Date, n.Status, u.Link');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', 'nl.NewsID = n.ID and nl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = n.ID and u.Type = 'News'", 'LEFT');
        $this->db->order_by('n.Date', 'DESC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['news'] = $this->db->get()->result();
        
        $this->render('admin/news', $data);
    }
    
    public function edit_news()
    {
        $id = (int)$this->input->get('id');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status');
            $titles = $this->input->post('Title');
            $texts = $this->input->post('Text');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            $date = $this->input->post('Date');
            
            if (empty($link))
            {
                $link = str_to_url($titles[1]);
            }
            
            // save product
            $article = [
                'Date' => date('c', strtotime($date)),
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('News', $article, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('News', $article);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('NewsLang', ['NewsID' => $id]);
            
            $insert = [];
            foreach ($titles as $langID => $name)
            {
                $insert[] = [
                    'NewsID' => $id,
                    'LangID' => $langID,
                    'Title' => $titles[$langID],
                    'Text' => $texts[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('NewsLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'News')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $i;
                }
            }

            if (isset($all_urls_ot['News'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['News'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'News'
                ]);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('NewsImage', ['IsMain' => 0], ['NewsID' => $id]);
                $this->db->update('NewsImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/news',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/news/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'NewsID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('NewsImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/news',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'News',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('NewsSaved') . '</div>');
                
            $red = site_url('admin/edit_news', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            // get article
            $this->db->select('*');
            $this->db->from('News');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $this->db->limit(1);
            $data['article'] = $this->db->get()->row();
            
            // get article langs
            $this->db->select('*');
            $this->db->from('NewsLang');
            $this->db->where('NewsID', $id);
            $res = $this->db->get()->result();
            
            $data['article_langs'] = [];
            foreach ($res as $row)
            {
                $data['article_langs'][$row->LangID] = $row;
            }
            
            // get article url
            $url = $this->db->get_where('Url', ['Type' => 'News', 'ObjectID' => $id], 1)->row();
            $data['article_link'] = isset($url->Link) ? $url->Link : '';
            
            // get article images
            $this->db->select('*');
            $this->db->from('NewsImage');
            $this->db->where('NewsID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'News');
            $data['files'] = $this->db->get()->result();
        }
        
        $this->render('admin/edit_news', $data);
    }
    
    public function delete_news()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('News', ['ID' => $id]);
            $this->db->delete('NewsLang', ['NewsID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'News']);
            $this->db->delete('NewsImage', ['NewsID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'News'], 1);
        }
        
        redirect(site_url('admin/news'), $page > 0 ? ['page' => $page] : []);
    }
    
        public function projects()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/projects');
        $config['total_rows'] = $this->db->count_all_results('Projects');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('pl.*, p.*, u.Link');
        $this->db->from('Projects as p');
        $this->db->join('ProjectsLang as pl', 'pl.ProjectID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Project'", 'LEFT');
        $this->db->order_by('p.Date', 'DESC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['projects'] = $this->db->get()->result();
        
        $this->render('admin/projects', $data);
    }
    
    public function edit_projects()
    {
        $id = (int)$this->input->get('id');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status');
            $titles = $this->input->post('Title');
            $material = $this->input->post('Material');
            $constructor = $this->input->post('Constructor');
            $beneficiar = $this->input->post('Beneficiar');
            $texts = $this->input->post('Text');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            $date = $this->input->post('Date');
            
            if (empty($link))
            {
                $link = str_to_url($titles[1]);
            }
            
            // save product
            $article = [
                'Date' => date('c', strtotime($date)),
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('Projects', $article, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Projects', $article);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('ProjectsLang', ['ProjectID' => $id]);
            
            $insert = [];
            foreach ($titles as $langID => $name)
            {
                $insert[] = [
                    'ProjectID' => $id,
                    'LangID' => $langID,
                    'Title' => $titles[$langID],
                    'Material' => $material[$langID],
                    'Constructor' => $constructor[$langID],
                    'Beneficiar' => $beneficiar[$langID],
                    'Text' => $texts[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('ProjectsLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Project')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $i;
                }
            }

            if (isset($all_urls_ot['Project'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Project'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'Project'
                ]);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('ProjectsImage', ['IsMain' => 0], ['ProjectID' => $id]);
                $this->db->update('ProjectsImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/projects',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/projects/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/projects/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/projects/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/projects/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'ProjectID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('ProjectsImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/projects',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'Project',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('ProjectSaved') . '</div>');
                
            $red = site_url('admin/edit_projects', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            // get article
            $this->db->select('*');
            $this->db->from('Projects');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $this->db->limit(1);
            $data['article'] = $this->db->get()->row();
            
            // get article langs
            $this->db->select('*');
            $this->db->from('ProjectsLang');
            $this->db->where('ProjectID', $id);
            $res = $this->db->get()->result();
            
            $data['article_langs'] = [];
            foreach ($res as $row)
            {
                $data['article_langs'][$row->LangID] = $row;
            }
            
            // get article url
            $url = $this->db->get_where('Url', ['Type' => 'Project', 'ObjectID' => $id], 1)->row();
            $data['article_link'] = isset($url->Link) ? $url->Link : '';
            
            // get article images
            $this->db->select('*');
            $this->db->from('ProjectsImage');
            $this->db->where('ProjectID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'Project');
            $data['files'] = $this->db->get()->result();
        }
        
        $this->render('admin/edit_projects', $data);
    }
    
    public function delete_projects()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('Projects', ['ID' => $id]);
            $this->db->delete('ProjectsLang', ['ProjectID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'Project']);
            $this->db->delete('ProjectsImage', ['ProjectID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'Project'], 1);
        }
        
        redirect(site_url('admin/projects'), $page > 0 ? ['page' => $page] : []);
    }
    
    public function pages()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/pages');
        $config['total_rows'] = $this->db->count_all_results('Page');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('p.ID, pl.Title, u.Link, pl.Text, p.Status, p.IsSystem');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'", 'LEFT');
        $this->db->order_by('ID', 'ASC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['pages'] = $this->db->get()->result();
        
        $this->render('admin/pages', $data);
    }
    
    public function edit_page()
    {
        $id = (int)$this->input->get('id');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status') == 'Disabled' ? 'Disabled' : 'Active';
            $titles = $this->input->post('Title');
            $texts = $this->input->post('Text');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            
            if (empty($link))
            {
                $link = $id == 1 ? '/' : str_to_url($titles[1]);
            }
            
            // save page
            $article = [
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('Page', $article, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Page', $article);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('PageLang', ['PageID' => $id]);
            
            $insert = [];
            foreach ($titles as $langID => $name)
            {
                $insert[] = [
                    'PageID' => $id,
                    'LangID' => $langID,
                    'Title' => $titles[$langID],
                    'Text' => $texts[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('PageLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Page')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $i;
                }
            }

            if (isset($all_urls_ot['Page'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Page'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'Page'
                ]);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('PageImage', ['IsMain' => 0], ['PageID' => $id]);
                $this->db->update('PageImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/pages',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/pages/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'PageID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('PageImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/pages',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'Page',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('PageSaved') . '</div>');
                
            $red = site_url('admin/edit_page', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            // get article
            $this->db->select('*');
            $this->db->from('Page');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $this->db->limit(1);
            $data['article'] = $this->db->get()->row();
            
            // get article langs
            $this->db->select('*');
            $this->db->from('PageLang');
            $this->db->where('PageID', $id);
            $res = $this->db->get()->result();
            
            $data['article_langs'] = [];
            foreach ($res as $row)
            {
                $data['article_langs'][$row->LangID] = $row;
            }
            
            // get article url
            $url = $this->db->get_where('Url', ['Type' => 'Page', 'ObjectID' => $id], 1)->row();
            $data['article_link'] = isset($url->Link) ? $url->Link : '';
            
            // get article images
            $this->db->select('*');
            $this->db->from('PageImage');
            $this->db->where('PageID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'Page');
            $data['files'] = $this->db->get()->result();
        }
        
        $this->render('admin/edit_page', $data);
    }
    
    public function delete_page()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('Page', ['ID' => $id]);
            $this->db->delete('PageLang', ['PageID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'Page']);
            $this->db->delete('PageImage', ['PageID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'Page'], 1);
        }
        
        redirect(site_url('admin/pages'), $page > 0 ? ['page' => $page] : []);
    }
    
    public function delete_file()
    {
        $fileID = (int)$this->input->post('fileID');
        
        if ($fileID > 0)
        {
            $this->db->delete('File', ['ID' => $fileID]);
        }
    }
    
    public function orders()
    {
        $this->addBreadscrumb('', lang('Orders'));
        
        $this->load->helper('form');
        
        $ClientID = $this->input->get('ClientID');
        $Date = $this->input->get('Date');
        $PaymentType = $this->input->get('PaymentType');
        $Type = $this->input->get('Type');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, o.ID', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->order_by('o.Date', 'DESC');
        if (!empty($ClientID)) $this->db->where('o.UserID', $ClientID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($PaymentType))$this->db->where('o.PaymentType', $PaymentType);
        if (!empty($Type))$this->db->where('o.Type', $Type);
        if (!empty($Status))$this->db->where('o.Status', $Status);
        
        if ($this->_user->Type == 'Manager')
        {
            $this->db->where('od.Region', $this->_user->Region);
        }
        
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['orders'] = $this->db->get()->result();
        
        $total_orders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/orders', [], true);
        $config['total_rows'] = $total_orders;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $clients = $this->db->where_in('Type', ['Angro', 'Retail'])->get('User')->result();
        $data['clients'][''] = 'All';
        foreach ($clients as $client)
        {
            $data['clients'][$client->ID] = $client->Name;
        }
        
        $this->render('admin/orders', $data);
    }

    public function order($order_id = false)
    {
        if (!$order_id) redirect('admin/orders');
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.ID', $order_id);
        $data['order'] = $this->db->get()->row();
        
        if (empty($data['order'])) redirect('admin/orders');
        
        $this->db->update('Order', ['IsNew' => 0], ['ID' => $data['order']->OrderID], 1);
        
        $this->config->load('regions');
        
        $this->addBreadscrumb(site_url('admin/orders'), lang('Orders'));
        $this->addBreadscrumb('', '# ' . $data['order']->OrderID);
        
        $data['client'] = $this->db->get_where('User', ['ID' => $data['order']->UserID], 1)->row();
        
        $this->db->select('*, op.Price');
        $this->db->from('OrderProduct as op');
        $this->db->join('Product as p', 'p.ID = op.ProductID');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->where('op.OrderID', $data['order']->OrderID);
        $data['order_products'] = $this->db->get()->result();
        
        $this->render('admin/order', $data);
    }
    
    public function users()
    {
        $this->addBreadscrumb('', lang('Users'));
        
        $this->load->helper('form');
        $this->config->load('regions');
        
        $UserName = $this->input->get('Email');
        $Name = $this->input->get('Name');
        $CompanyName = $this->input->get('CompanyName');
        $Region = $this->input->get('Region');
        $Type = $this->input->get('Type');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        if (!empty($UserName)) $this->db->like('UserName', $UserName);
        if (!empty($Name)) $this->db->like('Name', $Name);
        if (!empty($CompanyName)) $this->db->like('CompanyName', $CompanyName);
        if (!empty($Region)) $this->db->where('Region', $Region);
        if (!empty($Type)) $this->db->where('Type', $Type);
        if (!empty($Status)) $this->db->where('Status', $Status);
        
        if ($this->_user->Type == 'Manager')
        {
            $this->db->where('Region', $this->_user->Region);
        }
        
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['users'] = $this->db->get('User')->result();
        
        $total_users = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/users', [], true);
        $config['total_rows'] = $total_users;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('admin/users', $data);
    }
    
    public function user($user_id = false)
    {
        $this->config->load('regions');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $data['user'] = $this->db->get_where('User', ['ID' => $user_id], 1)->row();
        
        if (empty($data['user']->UserName) || $data['user']->UserName != $this->input->post('UserName'))
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
            $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        }
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
        
        $password = $this->input->post('Password');
        if (!empty($password))
        {
            $this->form_validation->set_rules('Password', lang('Password'), 'matches[PasswordConfirmation]');
        }
        
        $this->form_validation->set_rules('ClientType', lang('ClientType'), 'required');
        $this->form_validation->set_rules('Region', lang('Region'), 'required|is_natural');
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('ClientType');
            $address = $this->input->post('Address');
            
            if (is_null($address))
            {
                $address = $this->config->item($this->input->post('Region'), 'regions');
            }
            
            $userData = [
                'UserName' => $this->input->post('UserName'),
                'Name' => $this->input->post('Name'),
                'Type' => $clientType,
                'Status' => $clientType == 'Angro' ? 'NotConfirmed' : 'Active',
                'Region' => $this->input->post('Region'),
                'Address' => $address,
                'CompanyName' => $this->input->post('CompanyName'),
                'Phone' => $this->input->post('Phone'),
                'Zip' => $this->input->post('Zip'),
                'Activitation' => $this->input->post('Activitation'),
                'RegDate' => date('c')
            ];
            
            if (!empty($password))
            {
                $userData['Password'] = $password;
            }
            
            if ($user_id > 0)
            {
                $this->db->update('User', $userData, ['ID' => $user_id], 1);
            }
            else
            {
                $this->db->insert('User', $userData);
                $user_id = $this->db->insert_id();
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect('admin/user/' . $user_id);
        }
        
        $this->render('admin/user', $data);
    }
    
    public function menus()
    {
        if (!($id = $this->input->get('id')))
        {
            $id = 1;
        }
        
        $this->load->helper('form');
        
        if (count($_POST) > 0)
        {
            $this->db->delete('MenuItem', ['MenuID' => $id]);
            
            $mi_insert = array();
            $mil_insert = array();
            $pos = 1;
            foreach($_POST['Entity'] as $Entity => $EntityData)
            {
                $this->db->insert('MenuItem', [
                    'MenuID' => $id,
                    'EntityID' => $EntityData['ID'],
                    'EntityType' => $EntityData['Type'],
                    'Position' => $pos
                ]);
                $miID = $this->db->insert_id();

                foreach ($this->config->item('languages') as $langID => $lang)
                {
                    $name = isset($_POST['Name'][$EntityData['Type']][$EntityData['ID']][$langID]) ? $_POST['Name'][$EntityData['Type']][$EntityData['ID']][$langID] : '';
                    $mil_insert[] = [
                       'MenuItemID' => $miID, 
                       'LangID' => $langID,
                       'Name' => $name,
                    ];
                }
                $pos++;
            }
            
            if (count($mil_insert) > 0)
            {
                $this->db->insert_batch('MenuItemLang', $mil_insert);
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Menu a fost salvata</div>');
            redirect(site_url('admin/menus', ['id' => $id]));
        }
        
        // menu list
        $this->db->select('*');
        $this->db->from('Menu as m');
        $this->db->join('MenuLang as ml', 'ml.MenuID = m.ID AND ml.LangID = ' . $this->langID);
        $this->db->order_by('ID');
        $res = $this->db->get()->result();
        
        $data['all_menus'] = [];
        foreach ($res as $row)
        {
            $data['all_menus'][$row->MenuID] = $row->Name;
        }
        
        // current menu
        $this->db->select('*');
        $this->db->from('Menu as m');
        $this->db->join('MenuLang as ml', 'ml.MenuID = m.ID');
        $this->db->where('m.ID', $id);
        $menu = $this->db->get()->result();
        
        $data['menu_lang'] = [];
        foreach ($menu as $row)
        {
            $data['menu_lang'][$row->LangID] = $row;
        }
        
        // current menu items
        $this->db->select('*');
        $this->db->from('MenuItem as mi');
        $this->db->join('MenuItemLang as mil', 'mil.MenuItemID = mi.ID');
        $this->db->where('mi.MenuID', $id);
        $this->db->order_by('mi.Position');
        $res = $this->db->get()->result();
        
        $data['menu_items'] = [];
        foreach ($res as $row)
        {
            $data['menu_items'][$row->ID]['data'] = $row;
            $data['menu_items'][$row->ID]['langs'][$row->LangID] = $row;
        }
        
        // page list
        $this->db->select('p.ID, pl.Title as Name, pl.LangID');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID');
        $this->db->where('p.Status', 'Active');
        $this->db->order_by('ID', 'ASC');
        $res = $this->db->get()->result();
        
        $data['pages'] = [];
        foreach ($res as $row)
        {
            $row->EntityType = 'Page';
            $data['pages'][$row->ID]['data'] = $row;
            $data['pages'][$row->ID]['langs'][$row->LangID] = $row;
        }
        
        // category list
        $this->db->select('c.ID, cl.Name, cl.LangID');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID');
        $this->db->where('c.Status', 'Active');
        $res = $this->db->get()->result();
        
        $data['categories'] = [];
        foreach ($res as $row)
        {
            $row->EntityType = 'Category';
            $data['categories'][$row->ID]['data'] = $row;
            $data['categories'][$row->ID]['langs'][$row->LangID] = $row;
        }
        
        $this->render('admin/menus', $data);
    }
    
    public function change_order_status()
    {
        $orderID = $this->input->post('orderID');
        $status = $this->input->post('status');
        
        $this->db->update('Order', ['Status' => $status], ['ID' => $orderID], 1);
    }
    
    public function slider()
    {   
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('Slider', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Imaginea a fost eliminata</div>');
            redirect(site_url('admin/slider'));
        }
        
        $data['sliders'] = $this->db->select('*')->from('Slider')->where("Type", "slider")->order_by('Position')->get()->result();
        $data['banners'] = $this->db->select('*')->from('Slider')->where("Type", "banner")->order_by('Position')->get()->result();
        
        if (count($_POST) > 0)
        {
            if (count($_FILES) > 0)
            {
                $config['upload_path'] = 'public/uploads/sliders';
                $config['allowed_types'] = 'jpg|png';
                $config['max_size']	= '10000';
                $config['max_width'] = '4500';
                $config['max_height'] = '2000';
                $config['encrypt_name'] = true;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image'))
                {
                    $slideType = $this->input->post('slide_type');

                    $image = $this->upload->data();
                    $this->db->insert('Slider', ['Image' => $image['file_name'], 'Position' => 999999, 'Type' => $slideType]);
                }
                else
                {
                    $data['errors'] = $this->upload->display_errors();
                }
            }
            
            $links = (array)$this->input->post('Link');
            foreach ($links as $sid => $link)
            {
                $this->db->update('Slider', ['Link' => $link], ['ID' => $sid], 1);
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect(site_url('admin/slider'));
        }
        
        $this->render('admin/slider', $data);
    }
    
    public function slider_sort()
    {
        $sids = $this->input->post('sid');
        
        $pos = 1;
        foreach ($sids as $sid)
        {
            $this->db->update('Slider', ['Position' => $pos], ['ID' => $sid], 1);
            $pos++;
        }
    }
    
    public function feedback()
    {
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('Feedback', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Feedback a fost eliminat</div>');
            redirect(site_url('admin/feedback'));
        }
        
        $data['feedbacks'] = $this->db->select('*')->order_by('Date', 'DESC')->get('Feedback')->result();
        
        $this->render('admin/feedback', $data);
    }
    
     public function subscribers()
    {
        if ($delID = $this->input->get('delID'))
        {
            $this->db->delete('Subscribe', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Subscriber eliminat</div>');
            redirect(site_url('admin/subscribers'));
        }
        
        $data['subscribes'] = $this->db->select('*')->order_by('Date', 'DESC')->get('Subscribe')->result();
        
        $this->render('admin/subscribers', $data);
    }
    
    public function settings()
    {   
        
        if ($delID = $this->input->get('delID'))
        {
            $this->db->where('ID', $delID);
            $this->db->update('Settings', ['Value' => ""]); 
                
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Imaginea a fost eliminata</div>');
            redirect(site_url('admin/settings'));
        }
        
        if($this->input->post('Settings')){
            $post = $this->input->post('Settings');
        
            foreach ($post as $sid => $sp)
            { 
                $this->db->where('ID', $sid);
                $this->db->update('Settings', ['Value' => $sp]);  
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Setarile au fost aplicate</div>');
        }
        
        if(isset($_FILES) && count($_FILES) > 0){
            $images = $_FILES;
            
            $config['upload_path'] = 'public/uploads/settings';
            $config['allowed_types'] = 'jpg|png';
            $config['max_size']	= '10000';
            $config['max_width'] = '4500';
            $config['max_height'] = '2000';
            $config['encrypt_name'] = false;
            $this->load->library('upload', $config);
            
            $was_uploaded = 0;
            foreach ($images as $imgid => $imgval)
            { 
                if($images[$imgid]['name']){
                    if ($this->upload->do_upload($imgid))
                    {
                        $image = $this->upload->data();
                    }
                    else
                    {
                        $data['errors'] = $this->upload->display_errors();
                        break;
                    }

                    $this->db->where('ID', (int)substr($imgid, 6));
                    $this->db->update('Settings', ['Value' => $image['file_name']]);
                    
                    $was_uploaded = 1;
                } 
            }                      
            
            if($was_uploaded > 0) 
                $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Setarile au fost aplicate</div>');
        }        
        
        $data['settings'] = $this->db->select('*')->from('Settings')->get()->result();
        $this->render('admin/settings', $data);
    }
    
    public function auto()
    {
        if ($delID = $this->input->get('mark_delID'))
        {
            $this->db->delete('AutoMarks', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Marca auto a fost eliminata!</div>');
            redirect(site_url('admin/auto'));
        }
        
        if ($delID = $this->input->get('model_delID'))
        {
            $this->db->delete('AutoModels', ['ID' => $delID], 1);
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Modelul auto a fost eliminat!</div>');
            redirect(site_url('admin/auto'));
        }
        
        $gid = (int)$this->input->get('id');
        $gtype = $this->input->get('type');
        if($gid > 0 && $gtype == 'mark'){
            $data['mark_data'] = $this->db->select('*')->where('ID=', $gid)->get('AutoMarks')->row();
        }elseif($gid > 0 && $gtype == 'model'){
            $data['model_data'] = $this->db->select('*')->where('ID', $gid)->get('AutoModels')->row();
        }
                                
        if (count($_POST) > 0)
        {            
            $id = $this->input->post('id');
            $type = $this->input->post('type');

            if($type == 'mark')
            {
                $mark = $this->input->post('mark');
                if ($id > 0)
                    $this->db->update('AutoMarks', ['name'=>$mark], ['ID' => $id]);
                else
                {
                    $this->db->insert('AutoMarks', ['name'=>$mark]);
                    $id = $this->db->insert_id();
                }
                
            }else if($type == 'model')
            {
                $model = $this->input->post('model');
                $mark = $this->input->post('mark');
                $start = $this->input->post('start');
                $end = $this->input->post('end');
                
                $sql = ['name'=>$model, 'markID'=>$mark, 'start'=>$start, 'end'=>$end];
                
                if ($id > 0)
                    $this->db->update('AutoModels', $sql, ['ID' => $id]);
                else
                {
                    $this->db->insert('AutoModels', $sql);
                    $id = $this->db->insert_id();
                }
            }
                                     
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('AutoSaved') . '</div>');            
            redirect(site_url('admin/auto'));
        }
        
        $data['marks'] = $this->db->select('*')->from('AutoMarks')->order_by('name', 'ASC')->get()->result();
        $data['models'] = $this->db->select('am.*, a.name as mark')
                                   ->from('AutoModels am')
                                   ->join('AutoMarks a', 'am.markID = a.ID', 'left')
                                   ->get()
                                   ->result();
        
        $this->render('admin/auto', $data);
    }
    
//    public function import_csv()
//    {
//
//        error_reporting(E_ALL);
//        ini_set('display_errors', 1);
//        ini_set('max_execution_time', 0);
//        ini_set('memory_limit', '-1');
//        define('CSV_PATH','E:/OpenServer/domains/nipon.loc/'); 
//        
//        $categories = [];
////        $products_url = [];
//        $today=date('Y-m-d h:i:s');
//        $insert_csv=[];
//
//        $csv_file = CSV_PATH . "nipon.csv"; 
//        $csvfile = fopen($csv_file, 'r');
//        $j = 0;
//    
//        while (!feof($csvfile))
//        {                
//            $csv_data[$j] = fgets($csvfile, 9999);
//            $csv_array = explode(";", $csv_data[$j]);
//            
//            unset($csv_data[$j]);                  
//           
//            $insert_csv['category'] = mb_convert_encoding($csv_array[0], "utf-8", "windows-1251");
//            $insert_csv['brand'] = $csv_array[1] ? $csv_array[1] : 'no-brand';
//            $insert_csv['part_nr'] = $csv_array[2] ? $csv_array[2] : '';
//            $insert_csv['orig_nr'] = $csv_array[3] ? $csv_array[3] : '';
//            $insert_csv['stock'] = $csv_array[4] ? $csv_array[4] : '0';
//            $insert_csv['analog'] = $csv_array[5] ? str_replace(' ', '', $csv_array[5]) : '';
//            $insert_csv['name'] = $csv_array[6] ? mb_convert_encoding($csv_array[6], "utf-8", "windows-1251") : '';
//            $insert_csv['price'] = (float)$csv_array[7];
//            $insert_csv['price_1'] = (float)$csv_array[8];
//            $insert_csv['price_2'] = (float)$csv_array[9];
//            $insert_csv['price_3'] = (float)$csv_array[10];
//            $insert_csv['price_4'] = (float)$csv_array[11];
//            $insert_csv['price_5'] = (float)$csv_array[12];
//            $insert_csv['price_6'] = (float)$csv_array[13];
//            $insert_csv['price_7'] = (float)$csv_array[14];
//            $insert_csv['price_8'] = (float)$csv_array[15];
//            $insert_csv['price_9'] = (float)$csv_array[16];
//            $insert_csv['price_10'] = (float)$csv_array[17];
//            $insert_csv['price_2prercent'] = $csv_array[18] ? $csv_array[18] : ''; 
//            
//           // category insertion ------------------------------------------------------------------------------------------------------           
//           $caturl = str_to_url($insert_csv['category']);
//           if(empty($caturl)) $caturl = 'no-category-'.rand(1111,9999);
//           if(in_array($caturl, $categories)){
//                $catID = array_search($caturl, $categories);
//           }else{
//               
//                // add category
//                $sql_categ = ['ParentID'=>'0', 'Image'=>'', 'Type'=>'All', 'Status'=>'Active'];
//                $this->db->insert('Category', $sql_categ); 
//                $catID = $this->db->insert_id();
//                
//                $categories[$catID] = $caturl;
//
//                // add language
//                for($i=1;$i<=3;$i++){
//                    $sql_categLang[$i] = ['CategoryID'=>$catID, 'LangID'=>$i, 'Name'=>$insert_csv['category'], 'Title'=>$insert_csv['category'], 'Text'=>'', 'Keywords'=>'', 'Description'=>''];                    
//                }
//                $this->db->insert_batch('CategoryLang', $sql_categLang);
//                                                 
//                // add url
//                $sql_url = ['Link'=>$caturl, 'ObjectID'=>$catID, 'Type'=>'Category'];
//                $this->db->insert('Url', $sql_url);
//                
//       
//            }
//            // product insertion ----------------------------------------------------------------------------------------------------
//            $sql_product = ['CategoryID'=>$catID, 'Sku'=>'', 'Stock'=>$insert_csv['stock'], 'Price'=>$insert_csv['price'], 'DiscountPrice'=>'','DiscountPriceStart'=>'','DiscountPriceEnd'=>'','IsPromo'=>'0', 'Type'=>'All', 'Created'=>$today, 'Status'=>'Active', 'AnalogGroup'=>$insert_csv['analog'],
//                            'Part_nr'=>$insert_csv['part_nr'], 'Part_nr_orig'=>$insert_csv['orig_nr'], 'Price_cat_1'=>$insert_csv['price_1'], 'Price_cat_2'=>$insert_csv['price_2'], 'Price_cat_3'=>$insert_csv['price_3'], 'Price_cat_4'=>$insert_csv['price_4'], 'Price_cat_5'=>$insert_csv['price_5'], 'Price_cat_6'=>$insert_csv['price_6'], 'Price_cat_7'=>$insert_csv['price_7'], 'Price_cat_8'=>$insert_csv['price_8'], 'Price_cat_9'=>$insert_csv['price_9'], 'Price_cat_10'=>$insert_csv['price_10'], 'Price_2_percent'=>$insert_csv['price_2prercent']];
//            
//            
//            
//            $this->db->insert('Product', $sql_product);
//            $prodID = $this->db->insert_id();
//                        
//            $prodName = trim($insert_csv['category'] . ' ' . $insert_csv['brand'] . ' ' . $insert_csv['name']);
//                        
//            // add product language
//            for($i=1;$i<=3;$i++){
//                $sql_prodLang[$i] = ['ProductID'=>$prodID, 'LangID'=>$i, 'Name'=>$prodName, 'Keywords'=>'', 'Description'=>'', 'Text'=>''];               
//            }            
//            $this->db->insert_batch('ProductLang', $sql_prodLang);            
//            
//            // add product url
//                                                           
//            $sql_produrl = ['Link'=>$this->generate_url(str_to_url($prodName)), 'ObjectID'=>$prodID, 'Type'=>'Product'];
//            $this->db->insert('Url', $sql_produrl);
//            
//            // product filter insertion --------------------------------------------------------------------------------------------
//            if($insert_csv['brand'] != 'no-brand'){
//                $sql_filter = ['ProductID'=>$prodID, 'FilterID'=>2];
//
//                $this->db->insert('ProductFilter', $sql_filter); 
//                $filterID = $this->db->insert_id();
//
//                // add product filter language
//                for($i=1;$i<=3;$i++){
//                    $sql_prodFilterLang[$i] = ['ProductFilterID'=>$filterID, 'LangID'=>$i, 'Value'=>$insert_csv['brand'] ? $insert_csv['brand'] : ''];                
//                }
//                $this->db->insert_batch('ProductFilterLang', $sql_prodFilterLang);  
//            }
//            
//            unset($csv_array);
//            unset($insert_csv[$j]);                                    
//            unset($caturl,$catID);
//            unset($sql_categ);
//            unset($sql_categLang);
//            unset($sql_url);
//            unset($sql_product);
//            unset($sql_prodLang);
//            unset($sql_produrl);
//            unset($sql_filter);
//            unset($sql_prodFilterLang);
//            unset($filterID);
//            unset($prodID);
//            unset($catID);
//            unset($prodName);            
//            
//            $j++;
//        }
//        fclose($csvfile);
//        die("File data successfully imported to database!! Affected=".$j);
//        
////        
////        print "<pre>";
//////        print_r($csv_dat);
////        die(print_r($csv));
////        exit();
//    }
//    
//    public function generate_url($link){
//         
//        $prod_links = $this->db->select('Link')
//                               ->from('Url')
//                               ->where('Type', 'Product')
//                               ->like('Link', $link)
//                               ->get()->result_array();
//       
//        
//        $link = (string)$link;
//        $all_urls = [];
//        foreach ($prod_links as $row)
//        {
//            $lrow = (string)$row['Link'];
//            $all_urls[] = $lrow;
//        }
//        
//        if (in_array($link, $all_urls))
//        {            
//            $i = 0;
//            $str = $str_ = $link;
//            $links = array_values($all_urls);                
//            while (in_array($str, $links))
//            {
//                $i++;
//                $str = $str_ . '-' . $i; 
////                print $str."<br />";
//                
//            }
//            $link .= '-' . $i;
//                        
//        }
//        
//        return $link;
//    }

    
}