<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['Home'] = 'Pagina principală';

$lang['About'] = 'Despre noi';
$lang['Contacts'] = 'Contacte';


$lang['Slogan'] = 'Succesul nostru este în calitatea produselor!';
$lang['AccountLink'] = 'Cabinet personal';
$lang['Logout'] = 'Ieșire';
$lang['AllreadySubscribed'] = 'Email-ul este deja abonat';
$lang['SubscribeSuccess'] = 'Email-ul este abonat cu succes';
$lang['Catalog'] = 'Catalog';
$lang['UserAccount'] = 'Account';
$lang['Users'] = 'Utilizatori';
$lang['NotConfirmed'] = 'Nu a fost confirmat';
$lang['RegDate'] = 'Data înregistrării';
$lang['DataSaved'] = 'Date salvate';
$lang['Managers'] = 'Manageri';
$lang['Menus'] = 'Meniuri';
$lang['Feedback'] = 'Feedback';
$lang['SelectLanguage'] = 'Selectează Limba';
$lang['Contacts'] = 'Contacte';

$lang['UserEmail'] = 'E-mail utilizator';
$lang['Password'] = 'Parola';
$lang['Login'] = 'Logare';
$lang['LostPassword'] = 'Ai uitat parola?';
$lang['Email'] = 'Adresa e-mail';
$lang['Send'] = 'Trimite';
$lang['PasswordRecovery'] = 'Recuperarea parola';
$lang['PasswordRecoveryInfo'] = 'Întroduceți adresa e-mail indicată prin registrare';
$lang['InvalidLogin'] = 'Email sau parola este incorect';

$lang['Registration'] = 'Înregistrare';
$lang['PasswordConfirmation'] = 'Confirmați parola';
$lang['ClientType'] = 'Tip client';
$lang['Retail'] = 'Retail';
$lang['Angro'] = 'Angro';
$lang['SelectClientType'] = 'Selectați tip client';
$lang['EmailAllreadyExist'] = 'Adresa email este deja înregistrată';
$lang['WaitConfirmation'] = 'Așteptați confirmarea contului';
$lang['WaitConfirmationText'] = 'Așteptați confirmarea contului din partea adminului.';

$lang['RecoveryMessageSubject'] = 'Recuperarea parola la www.agm.md';
$lang['EmailNotRegistered'] = 'Adresa e-mail întrodusă na fost înregistrată';
$lang['ResetPassword'] = 'Resetarea parolei';
$lang['RecoveryToken'] = 'Codul de resetare';
$lang['InvalidRecoveryToken'] = 'Codul de resetare este incorect';
$lang['LoginWithNewPassword'] = 'Parola a fost schimbată cu succes. Logați cu parola nouă.';


$lang['AdminAccount'] = 'Cont administrator';
$lang['ProfileSettings'] = 'Setări profil';
$lang['Wellcome'] = 'Bine ați venit';

$lang['ConfirmCategoryDelete'] = 'Category will be deleted with subcategories and its products!';
$lang['CategoryName'] = 'Denumirea categoriei';
$lang['Categories'] = 'Categorii';
$lang['CategoryTitle'] = 'Titlul categoriei';
$lang['CategoryText'] = 'Textul categoriei';
$lang['Keywords'] = 'Keywords';
$lang['Description'] = 'Description';
$lang['Save'] = 'Salvare';
$lang['CategoryParent'] = 'Parent category';
$lang['InvalidForm'] = 'Form is invalid. See errors.';
$lang['CategorySaved'] = 'Categorie a fost salvată';
$lang['CategoryImage'] = 'Poza categoriei';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Status categorie';
$lang['CatStatusActive'] = 'Activă';
$lang['CatStatusDisabled'] = 'Dezactivată';
$lang['ShowIn'] = 'Arată în';
$lang['ShowInAll'] = 'Toate';
$lang['ShowInAngro'] = 'Angro';
$lang['ShowInRetail'] = 'Retail';
$lang['CategoryDeleted'] = 'Categorie a fost eliminată';
$lang['SelectCategory'] = 'Selectati categoria';

$lang['Filters'] = 'Filtre';
$lang['AddFilter'] = 'Salvați filtrul';
$lang['FilterName'] = 'Denumirea filtrul';
$lang['FilterType'] = 'Tip filtru';
$lang['String'] = 'String';
$lang['Number'] = 'Number';
$lang['FilterSaved'] = 'Filtrul a fost salvat';
$lang['DeleteFilter'] = 'Elimina filtru?';
$lang['FilterDeleted'] = 'Filtrul a fost eliminat';

$lang['Products'] = 'Produse';
$lang['AddProduct'] = 'Adaugă produs';
$lang['ProductName'] = 'Denumirea produs';
$lang['Price'] = 'Preț';
$lang['Sorting'] = 'Sortare';
$lang['IsPromo'] = 'Promo';
$lang['IsPromoUpper'] = 'PROMO';
$lang['Filter'] = 'Filtreză';
$lang['SKU'] = 'Articol';
$lang['Stock'] = 'Stoc';
$lang['Image'] = 'Imagine';
$lang['GeneralData'] = 'Date generale';
$lang['Images'] = 'Poze';
$lang['Link'] = 'Link';
$lang['PriceAndStock'] = 'Preț și stocuri';
$lang['EditProduct'] = 'Editare produs';
$lang['ProductCategory'] = 'Categorie produs';
$lang['DiscountPrice'] = 'Preț promoțional';
$lang['DiscountPriceStart'] = 'Valabil de la data';
$lang['DiscountPriceEnd'] = 'Valabil pînă la data';
$lang['HasPromo'] = 'Promo ';
$lang['AddImages'] = 'Adaugă poze';
$lang['ProductStatus'] = 'Status produs';
$lang['ProdStatusActive'] = 'Active';
$lang['ProdStatusDisabled'] = 'Dezactivat';
$lang['ProductSaved'] = 'Produs a fost salvat';
$lang['ClearFilters'] = 'Reseta filtre';
$lang['AddProductFilter'] = 'Adaugă filtru';
$lang['FilterName'] = 'Filtru';
$lang['FilterAllreadySelected'] = 'Filtru este deja selectat';
$lang['Analog'] = 'Analog';
$lang['Analogs'] = 'Analogii';
$lang['PartNr'] = 'Numar Piesa';
$lang['PartNrOring'] = 'Numar Piesa Original';

$lang['Newslatter'] = 'Newslatter';

$lang['News'] = 'Noutăți';
$lang['NewsUpperCase'] = 'NOUTĂȚI';
$lang['AddNews'] = 'Adaugă noutate';
$lang['Title'] = 'Titlul';
$lang['Text'] = 'Text';
$lang['Date'] = 'Data';
$lang['Status'] = 'Statut';
$lang['NewsSaved'] = 'Noutatea a fost salvată';
$lang['FileName'] = 'Denumire fișier';
$lang['SeeAllNews'] = 'vezi toate noutățile';
$lang['AllNews'] = 'toate noutățile';

$lang['Pages'] = 'Pagini';
$lang['AddPage'] = 'Adaugă pagina';
$lang['PageSaved'] = 'Pagina a fost salvată';

$lang['HomePage'] = 'Principală';
$lang['buy'] = 'Cumpără';
$lang['AddedToCart'] = 'Adaugat în coș';
$lang['OutOfStock'] = 'Out Of Stock';
$lang['UserInfo'] = 'User Info';
$lang['CartChanged'] = 'Cart changed';
$lang['CartProductDeleted'] = 'Cart product deleted';
$lang['CartEmpty'] = 'Cart empty';
$lang['CartTotal'] = 'Total';
$lang['ShoppingCart'] = 'Shopping Cart';
$lang['ContinueShopping'] = 'Continue Shopping';
$lang['Region'] = 'Regiunea';
$lang['Address'] = 'Adresa';
$lang['Name'] = 'Nume';
$lang['BackToCart'] = 'Back To Cart';
$lang['PaymentType'] = 'Payment Type';
$lang['CardPayment'] = 'Card Payment';
$lang['CashPayment'] = 'Cash Payment';
$lang['Zip'] = 'Zip / Cod poștal';
$lang['Phone'] = 'Telefon';
$lang['CompanyName'] = 'Nume companie';
$lang['Activitation'] = 'Domeniu de activitate';
$lang['Files'] = 'Fișiere (documente de anexat)';
$lang['Thankyou'] = 'Thankyou';
$lang['ThankyouTitle'] = 'Mulțumim pentru comanda Dumnevoastră';
$lang['ThankyouText'] = 'Your order has been created. Our manager will be contacted with you soon.';
$lang['ThankyouText2'] = 'Yout order ID #';
$lang['DownloadInvoice'] = 'Descarcă factura';
$lang['CartProducts'] = 'produse';

$lang['Dashboard'] = 'Dashboard';
$lang['OrderHistory'] = 'Istoria comenzii';
$lang['Amount'] = 'Suma';
$lang['Order'] = 'Comanda';
$lang['Orders'] = 'Comenzi';
$lang['BackToList'] = 'ÎNAPOI LA LISTA';
$lang['OrderDelails'] = 'Detalii comanda';
$lang['OrderProducts'] = 'Order Products';
$lang['Quantity'] = 'Cantitate';

$lang['Settings'] = 'Setări';
$lang['AddToCart'] = 'Adaugă in coș';
$lang['Checkout'] = 'Achitare';
$lang['CurrencyName'] = 'LEI' ;
$lang['CurrencyNameLowerCase'] = 'lei' ;
$lang['ArticlesPerPage'] = 'Articole pe pagină';
$lang['SortBy'] = 'Sortează după';
$lang['Date'] = 'Data';
$lang['PlaceOrder'] = 'Ordinea locului';
$lang['SeeAll'] = 'vezi pe toate';
$lang['NewProducts'] = 'PRODUSE NOI';
$lang['NewLabel'] = 'Nou!';

$lang['Search'] = 'Căutare';
$lang['SearchParts'] = 'Caută piese';
$lang['SearchPartsBy'] = 'numele sau codul piesei';
$lang['AllCategories'] = 'Toate Categoriile';
$lang['aboutCompany'] = 'despre comapnie';

$lang['Slider'] = 'Slider';
$lang['Banner'] = 'Banner';

$lang['Auto'] = 'Auto';
$lang['page_auto'] = 'Automobile';
$lang['automarks'] = 'Marci';
$lang['automodels'] = 'Modele';
$lang['btn_add'] = 'Adauga';
$lang['text_mark'] = 'Marca';
$lang['text_model_name'] = 'Nume model';
$lang['text_year_start'] = 'An inceput';
$lang['text_year_end'] = 'An sfirsit';
$lang['AutoSaved'] = 'Marca/Modelul a fost salvat';

$lang['AddProductAuto'] = 'Adaugă auto';

$lang['SelectAutoMark'] = 'Selectati marca auto';
$lang['SelectAutoModel'] = 'Selectati modelul';

$lang['HotLine'] = 'Linia fierbinte:';
$lang['Address1'] = 'or.Chisinau, str. Calea Orheiului 125';

$lang['FooterDescription'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec aliquam metus, eget sollicitudin justo. Nunc ullamcorper mollis ornare.';

//BIOX

$lang['ProductsBIOX'] = 'Produse BIOX';
$lang['Call_Us'] = 'Sunaţi-ne';
$lang['Message'] = 'Mesaj';
$lang['HomeHeadTitle'] = 'BIOX COMERT SRL';
$lang['HomeHeadText'] = 'Noi oferim solutii moderne optimizate, adaptate la cerintele actuale ecologice, programe de calcul pentru lucrari de pamant armat, efectuarea calculelor de stabilitate la lucrari de consolidare, asistenta tehnica la lucrari, punerea in opera a membranelor hidroizolante si geomembranelor.';



$lang['LatestWorks'] = 'Ultimele lucrări';
$lang['Material'] = 'Material';
$lang['Constructor'] = 'Constructor';
$lang['Recipient'] = 'Beneficiar';


$lang['FooterRepublicOf'] = 'Republica Moldova, mun. Chișinău';
$lang['FooterAddress'] = 'bd. Ștefan cel Mare 200, of. 101';
$lang['Newsletter'] = 'Abonează-te';
$lang['NewsletterText'] = 'Abonați-vă la noutățile noastre, pentru a fi la curent cu toate noutățile companiei.';
$lang['NewsletterInputPlaceholder'] = 'introduce-ți adresa de email';


$lang['HeaderAddress'] = 'mun. Chișinău, bd. Ștefan cel Mare 200, of. 101';
$lang['HeaderTextSection'] = 'Nu comercializăm produse, oferim soluţii';
$lang['Schedule'] = 'Orele de lucru';
$lang['ScheduleText'] = 'Luni - Vineri 09:00-18:00';
$lang['WriteUsMessage'] = 'Scrie-ne un mesaj';
$lang['Projects'] = 'Proiecte';
$lang['AddProjects'] = 'Adaugă proiect';






