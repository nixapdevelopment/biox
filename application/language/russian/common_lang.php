<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['Slogan'] = 'Наш успех в качестве продукции!';
$lang['AccountLink'] = 'Личный кабинет';
$lang['Logout'] = 'Выход';
$lang['AllreadySubscribed'] = 'На этот Email уже есть подписка';
$lang['SubscribeSuccess'] = 'Подписка завершена';
$lang['Catalog'] = 'Каталог';
$lang['UserAccount'] = 'Аккаунт';
$lang['Users'] = 'Пользователи';
$lang['NotConfirmed'] = 'Нет подтверждения';
$lang['RegDate'] = 'Дата регистрации';
$lang['DataSaved'] = 'Данные сохранены';
$lang['Managers'] = 'Менеджеры';
$lang['Menus'] = 'Меню';
$lang['SelectLanguage'] = 'Выберите язык';
$lang['Contacts'] = 'Контакты';

$lang['UserEmail'] = 'E-mail пользователя';
$lang['Password'] = 'Пароль';
$lang['Login'] = 'Вход';
$lang['LostPassword'] = 'Забыли пароль?';
$lang['Email'] = 'E-mail адрес';
$lang['Send'] = 'Отправить';
$lang['PasswordRecovery'] = 'Восстановление пароля';
$lang['PasswordRecoveryInfo'] = 'Введите e-mail указанный при регистрации';
$lang['InvalidLogin'] = 'Email или пароль указаны неверно';

$lang['Registration'] = 'Регистрация';
$lang['PasswordConfirmation'] = 'Подтвердите пароль';
$lang['ClientType'] = 'Тип клиента';
$lang['Retail'] = 'Розница';
$lang['Angro'] = 'Оптом';
$lang['SelectClientType'] = 'Выберите тип клиента';
$lang['EmailAllreadyExist'] = 'Данный Email уже существует';
$lang['WaitConfirmation'] = 'Дождитесь подтверждения аккаунта';
$lang['WaitConfirmationText'] = 'Дождитесь подтверждения со стороны администратора.';

$lang['RecoveryMessageSubject'] = 'Восстановление пароля на www.agm.md';
$lang['EmailNotRegistered'] = 'E-mail не был зарегистрирован';
$lang['ResetPassword'] = 'Сброс пароля';
$lang['RecoveryToken'] = 'Код для сброса пароля';
$lang['InvalidRecoveryToken'] = 'Неверный код сброса пароля';
$lang['LoginWithNewPassword'] = 'Пароль был успешно восстановлен. Войдите используя новый пароль.';


$lang['AdminAccount'] = 'Аккаунт Администратора';
$lang['ProfileSettings'] = 'Настройки профиля';
$lang['Wellcome'] = 'Добро Пожаловать';

$lang['ConfirmCategoryDelete'] = 'Категория будет удалена с подкатегориями, а также продукты в ней находящиеся!';
$lang['CategoryName'] = 'Название категории';
$lang['Categories'] = 'Категории';
$lang['CategoryTitle'] = 'Название категории';
$lang['CategoryText'] = 'Текст категории';
$lang['Keywords'] = 'Ключевые слова';
$lang['Description'] = 'SEO-описание';
$lang['Save'] = 'Сохранить';
$lang['CategoryParent'] = 'Родительская категория';
$lang['InvalidForm'] = 'Проверьте правильность введенных данных. см. ошибки';
$lang['CategorySaved'] = 'Категория сохранена';
$lang['CategoryImage'] = 'Изображение категории';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Статус категории';
$lang['CatStatusActive'] = 'Активна';
$lang['CatStatusDisabled'] = 'Не активна';
$lang['ShowIn'] = 'Показать в';
$lang['ShowInAll'] = 'Везде';
$lang['ShowInAngro'] = 'Оптом';
$lang['ShowInRetail'] = 'Розница';
$lang['CategoryDeleted'] = 'Категория удалена!';
$lang['SelectCategory'] = 'Выберите категорию';

$lang['Filters'] = 'Фильтры';
$lang['AddFilter'] = 'Сохраните фильтр';
$lang['FilterName'] = 'Название фильтра';
$lang['FilterType'] = 'Тип фильтра';
$lang['String'] = 'Строка';
$lang['Number'] = 'Число';
$lang['FilterSaved'] = 'Фильтр сохранен';
$lang['DeleteFilter'] = 'Удалить фильтр?';
$lang['FilterDeleted'] = 'Фильтр удален';

$lang['Products'] = 'Продукты';
$lang['AddProduct'] = 'Добавить продукт';
$lang['ProductName'] = 'Название продукта';
$lang['Price'] = 'Цена';
$lang['Sorting'] = 'Сортировка';
$lang['IsPromo'] = 'Акция';
$lang['IsPromoUpper'] = 'АКЦИЯ';
$lang['Filter'] = 'Фильтровать';
$lang['SKU'] = 'SKU';
$lang['Stock'] = 'Остаток';
$lang['Image'] = 'Изображение';
$lang['GeneralData'] = 'Общие данные';
$lang['Images'] = 'Изображения';
$lang['Link'] = 'Ссылка';
$lang['PriceAndStock'] = 'Цена и остатки';
$lang['EditProduct'] = 'Редактирование продукта';
$lang['ProductCategory'] = 'Катогория продукта';
$lang['DiscountPrice'] = 'Цена со скидкой';
$lang['DiscountPriceStart'] = 'Скидка доступна от';
$lang['DiscountPriceEnd'] = 'Скидка доступна до';
$lang['HasPromo'] = 'Акция ';
$lang['AddImages'] = 'Добавить изображения';
$lang['ProductStatus'] = 'Статус продукта';
$lang['ProdStatusActive'] = 'Активен';
$lang['ProdStatusDisabled'] = 'Недоступен';
$lang['ProductSaved'] = 'Продукт сохранен';
$lang['ClearFilters'] = 'Сброс фильров';
$lang['AddProductFilter'] = 'Добавить фильтр';
$lang['FilterName'] = 'Фильтр';
$lang['FilterAllreadySelected'] = 'Фильтр уже выбран';
$lang['Analog'] = 'Аналог';
$lang['Analog'] = 'Аналоги';
$lang['PartNr'] = 'Лиц. номер';
$lang['PartNrOring'] = 'Оригинальный номер';

$lang['News'] = 'Новости';
$lang['NewsUpperCase'] = 'НОВОСТИ';
$lang['AddNews'] = 'Добавить новость';
$lang['Title'] = 'Название';
$lang['Text'] = 'Текст';
$lang['Date'] = 'Дата';
$lang['Status'] = 'Статус';
$lang['NewsSaved'] = 'Новость сохранена';
$lang['FileName'] = 'Название файла';
$lang['SeeAllNews'] = 'показать все новости';
$lang['AllNews'] = 'все новости';

$lang['Newslatter'] = 'Newslatter';

$lang['Pages'] = 'Страницы';
$lang['AddPage'] = 'Добавить страницу';
$lang['PageSaved'] = 'Страница сохранена';

$lang['HomePage'] = 'Главная';
$lang['buy'] = 'Купить';
$lang['AddedToCart'] = 'Добавлено в корзину';
$lang['OutOfStock'] = 'Нет в наличии';
$lang['UserInfo'] = 'Инфо. о пользователе';
$lang['CartChanged'] = 'Корзина изменена';
$lang['CartProductDeleted'] = 'Продукт удален из корзины';
$lang['CartEmpty'] = 'Корзина пуста';
$lang['CartTotal'] = 'Всего';
$lang['ShoppingCart'] = 'Корзина';
$lang['ContinueShopping'] = 'Продолжить покупки';
$lang['Region'] = 'Регион';
$lang['Address'] = 'Адрес';
$lang['Name'] = 'Иня';
$lang['BackToCart'] = 'Назад в Корзину';
$lang['PaymentType'] = 'Тип оплаты';
$lang['CardPayment'] = 'Оплата картой';
$lang['CashPayment'] = 'Оплата наличными';
$lang['Zip'] = 'Zip / Почтовый индекс';
$lang['Phone'] = 'Телефон';
$lang['CompanyName'] = 'Название компании';
$lang['Activitation'] = 'Сфера деятельности';
$lang['Files'] = 'Файлы (прикрепление документов)';
$lang['Thankyou'] = 'Спасибо!';
$lang['ThankyouTitle'] = 'Благодарим Вас за сделанный заказ';
$lang['ThankyouText'] = 'Ваш заказ был создан. Менеджер скоро с Вами свяжется.';
$lang['ThankyouText2'] = 'Номер заказа #';
$lang['DownloadInvoice'] = 'Скачать счет';
$lang['CartProducts'] = 'продуктов';

$lang['Dashboard'] = 'Панель';
$lang['OrderHistory'] = 'История заказа';
$lang['Amount'] = 'Сумма';
$lang['Order'] = 'Заказ';
$lang['Orders'] = 'Заказы';
$lang['BackToList'] = 'ОБРАТНО К СПИСКУ';
$lang['OrderDelails'] = 'Детали Заказа';
$lang['OrderProducts'] = 'Заказать продукты';
$lang['Quantity'] = 'Количество';

$lang['Settings'] = 'Настройки';
$lang['AddToCart'] = 'В корзину';
$lang['Checkout'] = 'Оплата';
$lang['CurrencyName'] = 'ЛЕЕВ' ;
$lang['CurrencyNameLowerCase'] = 'леев' ;
$lang['ArticlesPerPage'] = 'Товаров на странице';
$lang['SortBy'] = 'Сортировать по';
$lang['Date'] = 'Дата';
$lang['PlaceOrder'] = 'Порядок';
$lang['SeeAll'] = 'смотреть все';
$lang['NewProducts'] = 'НОВЫЕ ПРОДУКТЫ';
$lang['NewLabel'] = 'Новинка!';

$lang['Search'] = 'Поиск';
$lang['SearchParts'] = 'Поиск запчастей';
$lang['SearchPartsBy'] = 'название или код запчасти';
$lang['AllCategories'] = 'Все категории';
$lang['aboutCompany'] = 'о компании';

$lang['Slider'] = 'Slider';
$lang['Banner'] = 'Banner';

$lang['Auto'] = 'Авто';
$lang['page_auto'] = 'Авто';
$lang['automarks'] = 'Бренды';
$lang['automodels'] = 'Модели';
$lang['btn_add'] = 'Добавить';
$lang['text_mark'] = 'Марка';
$lang['text_model_name'] = 'Имя модели';
$lang['text_year_start'] = 'Год начало';
$lang['text_year_end'] = 'Год конец';
$lang['AutoSaved'] = 'Авто сохранен';

$lang['AddProductAuto'] = 'Добавить Авто';

$lang['SelectAutoMark'] = 'Выберите автомобиль';
$lang['SelectAutoModel'] = 'Выберите модель автомобиля';

$lang['HotLine'] = 'Горячая линия:';
$lang['Address1'] = 'г. Кишинёв, ул. Каля Орхеюлуй 125';

$lang['FooterDescription'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec aliquam metus, eget sollicitudin justo. Nunc ullamcorper mollis ornare.';

//BIOX

$lang['ProductsBIOX'] = 'Продукты BIOX';
$lang['Call_Us'] = 'Позвоните нам';
$lang['Message'] = 'Сообщение';
$lang['HomeHeadTitle'] = 'BIOX COMERT SRL';
$lang['HomeHeadText'] = 'Мы предлагаем современные решения, оптимизированные, с учетом современных экологических требований к программному обеспечению для землеройных усиления расчетов устойчивости в строительных работах, работах по оказанию технической помощи, установка гидроизоляционных мембран и геомембраны.';


$lang['LatestWorks'] = 'Последние работы';
$lang['Material'] = 'Материал';
$lang['Constructor'] = 'Исполнитель';
$lang['Recipient'] = 'Заказчик';

$lang['FooterRepublicOf'] = 'Республика Молдова, мун. Кишинев';
$lang['FooterAddress'] = 'пр-т. Штефан чел Маре 200, оф. 101';
$lang['Newsletter'] = 'Подписаться';
$lang['NewsletterText'] = 'Подписаться на наши новости, чтобы быть в курсе всех новостей компании.';
$lang['NewsletterInputPlaceholder'] = 'Введите e-mail адрес';

$lang['HeaderAddress'] = 'мун. Кишинев, пр-т. Штефан чел Маре 200, оф. 101';
$lang['HeaderTextSection'] = 'Не продаём продукцию, мы предлагаем решения';
$lang['Schedule'] = 'Часы работы';
$lang['ScheduleText'] = 'Понедельник - Пятница 09:00-18:00';
$lang['WriteUsMessage'] = 'Написать сообщение';


$lang['Projects'] = 'Проекты';
$lang['AddProjects'] = 'Добавить проект';
