<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['Slogan'] = 'Our success is in quality of products!';
$lang['AccountLink'] = 'Personal Area';
$lang['Logout'] = 'Logout';
$lang['AllreadySubscribed'] = 'Email already subscribed!';
$lang['SubscribeSuccess'] = 'Email subscribed with succes';
$lang['Catalog'] = 'Catalog';
$lang['UserAccount'] = 'Account';
$lang['Users'] = 'Users';
$lang['NotConfirmed'] = 'Not Confirmed';
$lang['RegDate'] = 'Date of registration';
$lang['DataSaved'] = 'Data Saved';
$lang['Managers'] = 'Managers';
$lang['Menus'] = 'Menus';
$lang['SelectLanguage'] = 'Select Language';
$lang['Contacts'] = 'Contacts';

$lang['UserEmail'] = 'User E-mail';
$lang['Password'] = 'Password';
$lang['Login'] = 'Log in';
$lang['LostPassword'] = 'Forgot your password?';
$lang['Email'] = 'E-mail Address';
$lang['Send'] = 'Send';
$lang['PasswordRecovery'] = 'Password recovery';
$lang['PasswordRecoveryInfo'] = 'Enter e-mail address indicated on registration';
$lang['InvalidLogin'] = 'Email or password is incorrect';

$lang['Registration'] = 'Registration';
$lang['PasswordConfirmation'] = 'Confirm password';
$lang['ClientType'] = 'Client Type';
$lang['Retail'] = 'Retail';
$lang['Angro'] = 'Wholesale';
$lang['SelectClientType'] = 'Select Client Type';
$lang['EmailAllreadyExist'] = 'Email Allready Exists';
$lang['WaitConfirmation'] = 'Wait for account confirmation';
$lang['WaitConfirmationText'] = 'Wait for account confirmation from admin side.';

$lang['RecoveryMessageSubject'] = 'Recover password on www.agm.md';
$lang['EmailNotRegistered'] = 'Email Not Registered!';
$lang['ResetPassword'] = 'Reset password';
$lang['RecoveryToken'] = 'Reset password Token';
$lang['InvalidRecoveryToken'] = 'Invalid Recovery Token';
$lang['LoginWithNewPassword'] = 'The password has been changed successfully. Login with your new password.';


$lang['AdminAccount'] = 'Admin Account';
$lang['ProfileSettings'] = 'Profile Settings';
$lang['Wellcome'] = 'Welcome';

$lang['ConfirmCategoryDelete'] = 'Category will be deleted with subcategories and its products!';
$lang['CategoryName'] = 'Category Name';
$lang['Categories'] = 'Categories';
$lang['CategoryTitle'] = 'Category Title';
$lang['CategoryText'] = 'Category Text';
$lang['Keywords'] = 'Keywords';
$lang['Description'] = 'Description';
$lang['Save'] = 'Save';
$lang['CategoryParent'] = 'Parent category';
$lang['InvalidForm'] = 'Form is invalid. See errors.';
$lang['CategorySaved'] = 'Category saved';
$lang['CategoryImage'] = 'Category Image';
$lang['CategoryLink'] = 'Seo url';
$lang['CatStatus'] = 'Category Status';
$lang['CatStatusActive'] = 'Active';
$lang['CatStatusDisabled'] = 'Disabled';
$lang['ShowIn'] = 'Show in';
$lang['ShowInAll'] = 'All';
$lang['ShowInAngro'] = 'Wholesale';
$lang['ShowInRetail'] = 'Retail';
$lang['CategoryDeleted'] = 'Category Deleted!';
$lang['SelectCategory'] = 'Select category';

$lang['Filters'] = 'Filters';
$lang['AddFilter'] = 'Save filter';
$lang['FilterName'] = 'Filter Name';
$lang['FilterType'] = 'Filter Type';
$lang['String'] = 'String';
$lang['Number'] = 'Number';
$lang['FilterSaved'] = 'Filter Saved';
$lang['DeleteFilter'] = 'Delete filter?';
$lang['FilterDeleted'] = 'Filter was deleted';

$lang['Products'] = 'Products';
$lang['AddProduct'] = 'Add product';
$lang['ProductName'] = 'Product Name';
$lang['Price'] = 'Price';
$lang['Sorting'] = 'Sort';
$lang['IsPromo'] = 'Promo';
$lang['IsPromoUpper'] = 'PROMO';
$lang['Filter'] = 'Filter';
$lang['SKU'] = 'Item';
$lang['Stock'] = 'Stock';
$lang['Image'] = 'Image';
$lang['GeneralData'] = 'Common information';
$lang['Images'] = 'Images';
$lang['Link'] = 'Link';
$lang['PriceAndStock'] = 'Price and stock';
$lang['EditProduct'] = 'Edit product';
$lang['ProductCategory'] = 'Product Category';
$lang['DiscountPrice'] = 'Promo price';
$lang['DiscountPriceStart'] = 'Available from date';
$lang['DiscountPriceEnd'] = 'Available up to date';
$lang['HasPromo'] = 'Promo ';
$lang['AddImages'] = 'Add images';
$lang['ProductStatus'] = 'Product Status';
$lang['ProdStatusActive'] = 'Active';
$lang['ProdStatusDisabled'] = 'Disabled';
$lang['ProductSaved'] = 'Product saved';
$lang['ClearFilters'] = 'Reset filters';
$lang['AddProductFilter'] = 'Add filter';
$lang['FilterName'] = 'Filter';
$lang['FilterAllreadySelected'] = 'Filter selected allready';
$lang['Analog'] = 'Analog';
$lang['Analogs'] = 'Analogs';
$lang['PartNr'] = 'Part Number';
$lang['PartNrOring'] = 'Part Number Original';


$lang['News'] = 'News';
$lang['NewsUpperCase'] = 'NEWS';
$lang['AddNews'] = 'Add News Article';
$lang['Title'] = 'Title';
$lang['Text'] = 'Text';
$lang['Date'] = 'Date';
$lang['Status'] = 'Status';
$lang['NewsSaved'] = 'News article saved';
$lang['FileName'] = 'File Name';
$lang['SeeAllNews'] = 'see all news';
$lang['AllNews'] = 'all news';

$lang['Pages'] = 'Pages';
$lang['AddPage'] = 'Add page';
$lang['PageSaved'] = 'Page saved';

$lang['HomePage'] = 'Home Page';
$lang['buy'] = 'Buy';
$lang['AddedToCart'] = 'Added to cart';
$lang['OutOfStock'] = 'Out Of Stock';
$lang['UserInfo'] = 'User Info';
$lang['CartChanged'] = 'Cart changed';
$lang['CartProductDeleted'] = 'Cart product deleted';
$lang['CartEmpty'] = 'Cart empty';
$lang['CartTotal'] = 'Total';
$lang['ShoppingCart'] = 'Shopping Cart';
$lang['ContinueShopping'] = 'Continue Shopping';
$lang['Region'] = 'Region';
$lang['Address'] = 'Address';
$lang['Name'] = 'Name';
$lang['BackToCart'] = 'Back To Cart';
$lang['PaymentType'] = 'Payment Type';
$lang['CardPayment'] = 'Card Payment';
$lang['CashPayment'] = 'Cash Payment';
$lang['Zip'] = 'Zip / Postal Code';
$lang['Phone'] = 'Phone';
$lang['CompanyName'] = 'Company Name';
$lang['Activitation'] = 'Sphere of activity';
$lang['Files'] = 'Files (documents for attachment)';
$lang['Thankyou'] = 'Thank you';
$lang['ThankyouTitle'] = 'Thanks for your order';
$lang['ThankyouText'] = 'Your order has been created. Our manager will be contacted with you soon.';
$lang['ThankyouText2'] = 'Your order ID #';
$lang['DownloadInvoice'] = 'Download bill';
$lang['CartProducts'] = 'products';

$lang['Dashboard'] = 'Dashboard';
$lang['OrderHistory'] = 'Order history';
$lang['Amount'] = 'Amount';
$lang['Order'] = 'Order';
$lang['Orders'] = 'Orders';
$lang['BackToList'] = 'BACK TO LIST';
$lang['OrderDelails'] = 'Order Delails';
$lang['OrderProducts'] = 'Order Products';
$lang['Quantity'] = 'Quantity';

$lang['Settings'] = 'Settings';
$lang['AddToCart'] = 'Add to cart';
$lang['Checkout'] = 'Checkout';
$lang['CurrencyName'] = 'LEI' ;
$lang['CurrencyNameLowerCase'] = 'lei' ;
$lang['ArticlesPerPage'] = 'Articles per page';
$lang['SortBy'] = 'Sort by';
$lang['Date'] = 'Date';
$lang['PlaceOrder'] = 'Place Order';
$lang['SeeAll'] = 'See All';
$lang['NewProducts'] = 'NEW PRODUCTS';
$lang['NewLabel'] = 'New!';

$lang['Search'] = 'Search';
$lang['SearchParts'] = 'Search Parts';
$lang['SearchPartsBy'] = 'name of part number';
$lang['AllCategories'] = 'All Catgories';
$lang['aboutCompany'] = 'about company';

$lang['Slider'] = 'Slider';
$lang['Banner'] = 'Banner';

$lang['Auto'] = 'Auto';
$lang['page_auto'] = 'Automobile';
$lang['automarks'] = 'Marci';
$lang['automodels'] = 'Modele';
$lang['btn_add'] = 'Add';
$lang['text_mark'] = 'Mark';
$lang['text_model_name'] = 'Model name';
$lang['text_year_start'] = 'Start Year';
$lang['text_year_end'] = 'End Year';
$lang['AutoSaved'] = 'Mark/Model was saved';

$lang['AddProductAuto'] = 'Add auto';

$lang['SelectAutoMark'] = 'Select a vehicle';
$lang['SelectAutoModel'] = 'Select a vehicle model';

$lang['HotLine'] = 'Hot Line:';
$lang['Address1'] = 'or.Chisinau, str. Calea Orheiului 125';

$lang['FooterDescription'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec aliquam metus, eget sollicitudin justo. Nunc ullamcorper mollis ornare.';

