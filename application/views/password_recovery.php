<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="home-first">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form id="login-form" method="post">
                        <h1 class="text-center"><?= lang('PasswordRecovery') ?></h1>
                        <br />
                        <div class="well text-danger text-center">
                            <i class="glyphicon glyphicon-alert"></i>&nbsp;&nbsp;<?= lang('PasswordRecoveryInfo') ?>
                        </div>
                        <div class="text-danger">
                            <?= validation_errors() ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?= lang('Email') ?></label>
                            <input name="UserName" type="email" class="form-control" value="<?= set_value('UserName') ?>" required />
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right"><?= lang('Send') ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>