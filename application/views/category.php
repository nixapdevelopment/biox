<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Perform Categories & Subcategories
if (count($categories) > 0) {

    $as = $categories;
    $new_as = array();
    foreach ($as as $ind => $a) {
        $new_as[$a['ID']] = $a;
        $new_as[$a['ID']]['children'] = array();
    }

    foreach ($new_as as $ind => &$new_a) {
        $parent = $new_as[$ind]['ParentID'];
        if (isset($new_as[$parent])) {
            $new_as[$parent]['children'][] = &$new_as[$ind];
        }
    }

    function print_children($children, $level = 0, $itemID = null) {

        foreach ($children as $child) {

            if ($itemID == $child['ID'])
                $active = " class='active' ";
            else
                $active = "";
            echo "<ul>";
            echo "<li" . $active . "><a href='" . site_url($child['Link']) . "'>" . $child['Name'] . "</a></li>";
            print_children($child['children'], $level + 1);
            echo "</ul>";
        }
    }

}
?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">                
                <ul class="left-categories">      
                    <h4><?=lang('ProductsBIOX')?></h4>
                    <?php 
                    foreach ($new_as as $ind => &$new_a) {
                        if($category->ID == 1 || $category->ID == 3) $ParentID = $category->ID;
                        else $ParentID = $category->ParentID;

                        if($new_a['ParentID'] == $ParentID) {
                            $category->ID == $new_a['ID'] ? $active = " class='active' " : $active = "";
                            echo "<li" . $active . "><a href=" . site_url($new_a['Link']) . ">" . $new_a['Name'] . "</a></li>";  
                        }
                    }
                    ?>          

                </ul>                
            </div>
            <div class="col-md-9">
                <div class="category-page">
                    <h1 class="page-title"><?= $category->Title ?></h1>                    
                    <!--<img src="<?= base_url("public/uploads/categories/".$category->Image) ?>" title="<?= $category->Title ?>" alt="<?= $category->Title ?>" />-->
                    <p>
                        <?= $category->Text ?>
                    </p>
                                       
                </div>

                <div class="clearfix"></div>
                
                <div id="product-wrap">

                </div>
                <input type="hidden" name="page" value="1" />


            </div>   
        </div>    
</section>

<script>
$(function() {
        
    function applyFilters()
    {
        $('#product-wrap').html(LOADER);
        
        var page = 'page=' + ($('input[name=page]').val() == undefined ? '1' : $('input[name=page]').val());

        if (history.pushState)
        {
            var stateObject = {};
            history.pushState(stateObject, $(document).find('title').text(), window.location.pathname + '?' + page);
        }

        $.post('<?= site_url('main/ajaxProducts') ?>', {query: page, categoryID: <?= $category->ID ?>}, function (html) {
            $('#product-wrap').html(html);
        });
    }

    $('#per-page li a').click(function () {
        $('#per-page li a').removeClass('active');
        $(this).addClass('active');
        $('input[name="per_page"]').val($(this).attr('data'));
        applyFilters();
    });

    
    $(document).on('click', '#products-page li a', function () {
        $('#products-page li a').removeClass('active');
        $(this).addClass('active');
        $('input[name="page"]').val($(this).attr('data'));
        applyFilters();
    });
    
    $('.select2').select2({
        theme: 'bootstrap'
    });
         
    applyFilters();
    
});
</script>