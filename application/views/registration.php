<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="home-first">
            <h1 class="text-center"><?= lang('Registration') ?></h1>
            <div class="row">
                <form id="registration-form" method="post" class="col-md-offset-2 col-md-8" enctype="multipart/form-data">
                    <div class="text-danger">
                        <?= validation_errors() ?>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label"><?= lang('UserEmail') ?> <span class="text-danger">*</span></label>
                                <input name="UserName" type="text" value="<?= set_value('UserName') ?>" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('Password') ?> <span class="text-danger">*</span></label>
                                <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('PasswordConfirmation') ?> <span class="text-danger">*</span></label>
                                <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('Name') ?> <span class="text-danger">*</span></label>
                                <input name="Name" type="text" value="<?= set_value('Name') ?>" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('Phone') ?></label>
                                <input name="Phone" type="text" value="<?= set_value('Phone') ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('Zip') ?></label>
                                <input name="Zip" type="text" value="<?= set_value('Zip') ?>" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= lang('ClientType') ?> <span class="text-danger">*</span></label>
                                <?= form_dropdown('ClientType', ['' => lang('SelectClientType'), 'Retail' => lang('Retail'), 'Angro' => lang('Angro')], set_value('ClientType'), 'class="form-control" required') ?>
                            </div>
                            <div class="form-group company-fields">
                                <label class="control-label"><?= lang('CompanyName') ?></label>
                                <input disabled name="CompanyName" type="text" value="<?= set_value('CompanyName') ?>" class="form-control" />
                            </div>
                            <div class="form-group company-fields">
                                <label class="control-label"><?= lang('Activitation') ?></label>
                                <input disabled name="Activitation" type="text" value="<?= set_value('Activitation') ?>" class="form-control" />
                            </div>
                            <div class="form-group company-fields">
                                <label class="control-label"><?= lang('Files') ?></label>
                                <input disabled name="Files[]" type="file" multiple class="form-control" accept=".pdf,.doc,.docx,.xls,.xlsx,.png,.jpg,.gif,.tiff,.tif" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('Region') ?> <span class="text-danger">*</span></label>
                                <?= form_dropdown('Region', $this->config->item('regions'), set_value('Region'), 'class="form-control select2" required') ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?= lang('Address') ?></label>
                                <input type="text" name="Address" class="form-control" value="<?= set_value('Address') ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right"><?= lang('Send') ?></button>
                        <a href="<?= site_url('login') ?>" type="submit" class="btn btn-link pull-left"><?= lang('Login') ?></a>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>

    $('select[name="ClientType"]').change(function(){
        if ($(this).val() == 'Angro')
        {
            $('.company-fields input').removeAttr('disabled');
        }
        else
        {
            $('.company-fields input').attr('disabled', 'disabled');
        }
    });

</script>