<div class="container">
    <div class="dash-block">
        <div class="page-title"><?= lang('Newslatter') ?></div>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>                    
                    <th>Email</th>
                    <th>Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($subscribes as $subscribe) { ?>
                <tr>                    
                    <td><?= $subscribe->Email ?></td>
                    <td><?= date('d.m.Y H:i', strtotime($subscribe->Date)) ?></td>
                    <td style="vertical-align: middle;" class="text-center"><a onclick="return confirm('Elimina acest email?')" href="?delID=<?= $subscribe->ID ?>"><i class="glyphicon glyphicon-trash text-danger"></i></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>