<?php

$active_langs = $this->config->item('languages');

?>

<div class="container">

    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('Filters') ?></h3>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-filter-btn" href="#" class="btn btn-sm btn-success"><i class="fa fa-save"></i> <?= lang('AddFilter') ?></a>
            </div>
        </div>
    </div>
    <div>
        <form id="add-filter-form" method="post" class="row">
            <?php foreach ($active_langs as $langID => $lang) { ?>
            <div class="form-group col-md-3">
                <label class="control-label"><?= lang('FilterName') ?> (<?= $lang['Slug'] ?>)</label>
                <input class="form-control" type="text" name="Name[<?= $langID ?>]" required value="<?= isset($filter['Name'][$langID]) ? $filter['Name'][$langID] : '' ?>" />
            </div>
            <?php } ?>
            <div class="form-group col-md-3">
                <label class="control-label"><?= lang('FilterType') ?></label>
                <?= form_dropdown('Type', ['' => '-', 'Number' => lang('Number'), 'String' => lang('String')], isset($filter['Type']) ? $filter['Type'] : '', 'class="form-control" required') ?>
            </div>
            <input type="hidden" name="FilterID" value="<?= (int)$this->input->get('id') ?>" />
            <input type="submit" class="hidden" />
        </form>
        <hr />
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <?php foreach ($active_langs as $langID => $lang) { ?>
                    <th><?= lang('FilterName') ?> (<?= $lang['Slug'] ?>)</th>
                    <?php } ?>
                    <th><?= lang('FilterType') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($filters as $filterID => $filter) { ?>
                <tr>
                    <?php foreach ($active_langs as $langID => $lang) { ?>
                    <td><?= isset($filter['Name'][$langID]) ? $filter['Name'][$langID] : '' ?></td>
                    <?php } ?>
                    <td><?= $filter['Type'] ?></td>
                    <td>
                        <?php if ($filter['System'] == 0) { ?>
                        <a href="<?= site_url('admin/filters', ['id' => $filterID]) ?>"><i class="fa fa-edit text-info"></i></a>
                        &nbsp;&nbsp;
                        <a onclick="return confirm('<?= lang('DeleteFilter') ?>')" href="<?= site_url('admin/filters', ['delId' => $filterID]) ?>"><i class="fa fa-trash text-danger"></i></a>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>

    $(document).ready(function(){
        
        $('#add-filter-btn').click(function(){
            $('#add-filter-form input[type=submit]').click();
        });
        
        $("#add-filter-form").validate({
            errorClass: "text-danger",
            validClass: "text-success",
            errorElement: "span",
            focusCleanup: false,
            focusInvalid: true,
            onsubmit: true,
            ignore: ""
        });
        
        <?php if ($message = $this->session->flashdata('success')) { ?>
        notif({
            msg: "<?= $message ?>",
            type: "success",
            position: "right"
        });
        <?php } ?>
        
    });

</script>