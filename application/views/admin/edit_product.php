<?php
$active_langs = $this->config->item('languages');
?>
<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <div class="pull-left page-title"><?= isset($product->ID) ? lang('EditProduct') : lang('AddProduct') ?></div>
                <a style="float:right; margin-left: 10px;" href="<?= site_url('admin/products', $_GET) ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> ÎNAPOI LA LISTA</a>
            </div>
        </div>
    </div>
    <div class="">
        <?= $this->session->flashdata('success') ?>
        <form id="edit-product-form" enctype="multipart/form-data" method="post">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?= lang('GeneralData') ?></a></li>
                    <li role="presentation"><a href="#tab-images" aria-controls="tab-images" role="tab" data-toggle="tab"><?= lang('Images') ?></a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <br />
                    <div role="tabpanel" class="tab-pane active" id="tab-main">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('ProductCategory') ?> <i class="text-danger">*</i></label>
                                    <?= form_dropdown('CategoryID', $all_categories, isset($product->CategoryID) ? $product->CategoryID : '', 'class="form-control select2" required') ?>
                                </div>
                            </div>
                            <div class="col-md-6 hidden">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Link') ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><?= site_url() ?></span>
                                        <input name="Link" value="<?= isset($product_link) ? $product_link : '' ?>" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <br />
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#tab-<?= $lang['LangFile'] ?>" aria-controls="tab-<?= $lang['LangFile'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
                                <?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br />
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="tab-<?= $lang['LangFile'] ?>">
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('ProductName') ?> <i class="text-danger">*</i></label>
                                            <input required class="form-control" type="text" name="Name[<?= $langID ?>]" value="<?= isset($product_langs[$langID]->Name) ? $product_langs[$langID]->Name : '' ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Text') ?> <i class="text-danger">*</i></label>
                                            <textarea class="form-control ckeditor" name="Text[<?= $langID ?>]"><?= isset($product_langs[$langID]->Text) ? $product_langs[$langID]->Text : '' ?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Keywords') ?></label>
                                                    <textarea class="form-control" name="Keywords[<?= $langID ?>]"><?= isset($product_langs[$langID]->Keywords) ? $product_langs[$langID]->Keywords : '' ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Description') ?></label>
                                                    <textarea class="form-control" name="Description[<?= $langID ?>]"><?= isset($product_langs[$langID]->Description) ? $product_langs[$langID]->Description : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-price">
                        <div class="row">                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Stock') ?> <i class="text-danger">*</i></label>
                                    <input required min="0" step="1" class="form-control" type="number" name="Stock" value="<?= isset($product->Stock) ? $product->Stock : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('ShowIn') ?></label>
                                    <select required class="form-control" name="Type">
                                        <option <?= !empty($product->Type) && $product->Type == 'All' ? 'selected' : '' ?> value="All"><?= lang('ShowInAll') ?></option>
                                        <option <?= !empty($product->Type) && $product->Type == 'Retail' ? 'selected' : '' ?> value="Retail"><?= lang('ShowInRetail') ?></option>
                                        <option <?= !empty($product->Type) && $product->Type == 'Angro' ? 'selected' : '' ?> value="Angro"><?= lang('ShowInAngro') ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('ProductStatus') ?></label>
                                    <select class="form-control" name="Status">
                                        <option <?= @$product->Status == 'Active' ? 'selected' : '' ?> value="Active"><?= lang('ProdStatusActive') ?></option>
                                        <option <?= @$product->Status == 'Disabled' ? 'selected' : '' ?> value="Disabled"><?= lang('ProdStatusDisabled') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> <i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price" value="<?= isset($product->Price) ? $product->Price : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 1<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price1" value="<?= isset($product->Price_cat_1) ? $product->Price_cat_1 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 2<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price2" value="<?= isset($product->Price_cat_2) ? $product->Price_cat_2 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 3<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price3" value="<?= isset($product->Price_cat_3) ? $product->Price_cat_3 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 4<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price4" value="<?= isset($product->Price_cat_4) ? $product->Price_cat_4 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 5<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price5" value="<?= isset($product->Price_cat_5) ? $product->Price_cat_5 : 0 ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 6<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price6" value="<?= isset($product->Price_cat_6) ? $product->Price_cat_6 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 7<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price7" value="<?= isset($product->Price_cat_7) ? $product->Price_cat_7 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 8<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price8" value="<?= isset($product->Price_cat_8) ? $product->Price_cat_8 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 9<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price9" value="<?= isset($product->Price_cat_9) ? $product->Price_cat_9 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 10<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price10" value="<?= isset($product->Price_cat_10) ? $product->Price_cat_10 : 0 ?>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Price') ?> 2%<i class="text-danger">*</i></label>
                                    <input required step="any" class="form-control" type="number" name="Price2p" value="<?= isset($product->Price_2_percent) ? $product->Price_2_percent : 0 ?>" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div style="margin-bottom: 7px;">
                            <a id="add-discount" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Adauga promotie</a>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <table id="promo-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th><?= lang('DiscountPriceStart') ?></th>
                                            <th><?= lang('DiscountPriceEnd') ?></th>
                                            <th>Tip promotie</th>
                                            <th>Valoare</th>
                                            <th>Activa</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($discounts as $discount) { ?>
                                        <tr>
                                            <td>
                                                <input required onkeydown="return false" class="form-control input-sm datepicker" type="text" name="Discount[StartDate][]" value="<?= isset($discount->StartDate) ? date('d.m.Y', strtotime($discount->StartDate)) : '' ?>" />
                                            </td>
                                            <td>
                                                <input required onkeydown="return false" class="form-control input-sm datepicker" type="text" name="Discount[EndDate][]" value="<?= isset($discount->EndDate) ? date('d.m.Y', strtotime($discount->EndDate)) : '' ?>" />
                                            </td>
                                            <td style="width: 130px;">
                                                <?= form_dropdown('Discount[PromoType][]', $this->config->item('promotion_types'), $discount->PromoType, 'class="form-control input-sm"') ?>
                                            </td>
                                            <td>
                                                <input name="Discount[Value][]" type="number" value="<?= $discount->Value ?>" min="0.01" required class="form-control input-sm" />
                                            </td>
                                            <td class="text-center">
                                                <?= form_dropdown('Discount[PromoStatus][]', [1 => 'Da', 0 => 'Nu'], $discount->PromoStatus, 'class="form-control input-sm"') ?>
                                            </td>
                                            <td class="text-center">
                                                <span style="cursor: pointer;" class="btn btn-danger btn-sm delete-discount-row"><i class="glyphicon glyphicon-trash"></i></span>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-filters">
                        <button id="add-product-filter" type="button" class="btn btn-info"><i class="fa fa-plus-circle"></i> <?= lang('AddProductFilter') ?></button>
                        <div id="product-filter-wrap" style="min-height: 250px; padding-top: 10px;">
                            <?php foreach ($product_filters as $filterID => $filterData) { ?>
                            <div class="row product-filter-item">
                                <div class="col-md-3 filter-filter-wrap">
                                    <div class="input-group">
                                        <span class="input-group-addon"><?= lang('FilterName') ?></span>
                                        <select disabled style="width: 100%;" class="form-control filter-select">
                                            <option value="">Selectati</option>
                                            <?php foreach ($all_filters as $key => $name) { ?>
                                            <option <?= $key == $filterID ? 'selected' : '' ?> value="<?= $key ?>"><?= $name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                <div class="col-md-2 filter-value-wrap">
                                    <div class="input-group">
                                        <span class="input-group-addon"><?= $lang['Slug'] ?></span>
                                        <select lang-id="<?= $langID ?>" id="filter-select-<?= $filterID ?>-<?= $langID ?>" type="text" style="width: 100%;" class="form-control filter-value-select" multiple name="filter_value[<?= $filterID ?>][<?= $langID ?>]" placeholder="Value">
                                            <?php if (!empty($product_filter_values[$filterID][$langID])) { ?>
                                            <?php foreach ($product_filter_values[$filterID][$langID] as $value) { ?>
                                            <option <?= $filterData[$langID] == $value ? 'selected' : '' ?> value="<?= $value ?>"><?= $value ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-danger delete-filter"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <br />
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-auto">
                        <button id="add-product-auto" type="button" class="btn btn-info"><i class="fa fa-plus-circle"></i> <?= lang('AddProductAuto') ?></button>
                        <div id="product-auto-wrap" style="min-height: 250px; padding-top: 10px;">
                            <?php foreach ($product_autos as $pa) { ?>
                            <div class="row product-auto-item">
                                <div class="col-md-3 auto-auto-wrap">
                                    <div class="input-group">                                            
                                        <select style="width: 100%;" class="form-control auto-select" name="auto_value[mark][]">
                                            <option value="">Selectati marca</option>
                                            <?php foreach ($all_autos as $key => $name) { ?>
                                            <option <?= $key == $pa->markID ? 'selected' : '' ?> value="<?= $key ?>"><?= $name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 auto-value-wrap">
                                    <div class="input-group">                                        
                                        <select style="width: 100%;" class="form-control auto-value-select" name="auto_value[model][]">                                            
                                            <?php foreach ($product_models[$pa->markID] as $key => $name) { ?>
                                            <option <?= $key == $pa->modelID ? 'selected' : '' ?> value="<?= $key ?>"><?= $name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-danger delete-auto"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <br />
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-images">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Images') ?></label>
                                    <input class="form-control" accept=".png,.jpg" type="file" name="Images[]" multiple />
                                </div>
                                <table id="product-images" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th>Poza</th>
                                            <th>Poza generala</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($images) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($images as $img) { ?>
                                        <?php $i++; ?>
                                        <tr data-image-id="<?= $img['ID'] ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a rel="photos" href="<?= base_url('public/uploads/products/' . $img['Image']) ?>" class="fancybox">
                                                    <img style="height: 90px;" src="<?= base_url('public/uploads/products/' . $img['Thumb']) ?>" class="img-thumbnail" />
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="PrimaryImage" <?= $img['IsMain'] == 1 ? 'checked' : '' ?> value="<?= $img['ID'] ?>">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($img['IsMain'] != 1) { ?>
                                                <a href="#" onclick="return deleteImage(<?= $img['ID'] ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="4" class="text-center">
                                                Nici o poza încarcată
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> <?= lang('Save') ?></button>
        </form>
    </div>
</div>

<script>
    
    function deleteImage(imgID)
    {
        if (confirm('Confirmați?'))
        {
            $.post('<?= site_url('admin/delete_product_image') ?>', {imgID: imgID}, function(){
                $('tr[data-image-id=' + imgID + ']').remove();
            });
        }
        return false;
    }

    $('.select2').select2({
        theme: 'bootstrap'
    });
    
    $('input.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true
    });
    
    $('input[name=IsPromo]').click(function(){
        var controls = $('input[name="DiscountPrice"], input[name="DiscountPriceStart"], input[name="DiscountPriceEnd"]');
        
        if ($(this).is(':checked'))
        {
            controls.removeAttr('disabled').attr('required', 'required');
        }
        else
        {
            controls.attr('disabled', 'disabled').removeAttr('required');
        }
    });
    
    $("#edit-product-form").validate({
        errorClass: "text-danger",
        validClass: "text-success",
        errorElement: "span",
        focusCleanup: false,
        focusInvalid: true,
        onsubmit: true,
        ignore: "",        
        invalidHandler: function ()
        {
            notif({
                msg: "<?= lang('InvalidForm') ?>",
                type: "error",
                position: "right"
            });
        }
    });
    
    $('a.fancybox').fancybox();
    
    $('select.filter-select, select.auto-select').select2({
        theme: 'bootstrap'
    }).on("change", function(e) {
        console.log(e);
    });
    
    $('select.filter-value-select').each(function(){
        init_value_select(this);
    });
    
    $('select.auto-value-select').each(function(){
        $(this).select2({
            theme: 'bootstrap'
        });
    });
    
    $('body').on('DOMNodeInserted', 'select.filter-select, select.auto-select', function () {
        $(this).select2({
            theme: 'bootstrap'
        });
    });
        
    function init_value_select(elem)
    {
        $(elem).select2({
            theme: 'bootstrap',
            tags: true,
            maximumSelectionLength: 1
        });
    }
    
    $('#product-filter-wrap').on('change', '.product-filter-item .filter-value-select[lang-id=1]', function(){
        var value = $(this).val();
        $(this).closest('.product-filter-item').find('.filter-value-select').not(this).each(function(){
            if ($(this).val() == null || $(this).val() == '')
            {
                $(this).append('<option selected value="' + value + '">' + value + '</option>').trigger('change');
            }
        });
    });
    
    $('#add-product-filter').click(function(){
        $('#filter-row-tmpl').tmpl().appendTo('#product-filter-wrap');
        var newSelect = $('#product-filter-wrap .filter-select:last');
        $('#product-filter-wrap .select2-hidden-accessible').each(function(){
            newSelect.find('option[value=' + parseInt($(this).val()) + ']').remove();
        });
        newSelect.select2({
            theme: 'bootstrap'
        }).on("select2:selecting", function(e) {
            var c = $('#product-filter-wrap select option[value=' + parseInt(e.params.args.data.id) + ']:selected').length;
            if (c > 0)
            {
                notif({
                    msg: "<?= lang('FilterAllreadySelected') ?>",
                    type: "error",
                    position: "right"
                });
                return false;
            }
        }).on("select2:select", function(e){
            $.post('/admin/product_filter_select', {id: e.params.data.id}, function(html){
                newSelect.closest('.product-filter-item').find('.filter-value-wrap').remove();
                newSelect.closest('.product-filter-item').find('.filter-filter-wrap').after(html);
                newSelect.closest('.product-filter-item').find('.filter-value-wrap .filter-value-select').each(function(){
                    init_value_select(this);
                });
            });
        });
    });
    
    $('#product-auto-wrap').on('change', '.product-auto-item .auto-value-select', function(){
        var value = $(this).val();
        $(this).closest('.product-auto-item').find('.auto-value-select').not(this).each(function(){
            if ($(this).val() == null || $(this).val() == '')
            {
                $(this).append('<option selected value="' + value + '">' + value + '</option>').trigger('change');
            }
        });
    });     
    
    $('#add-product-auto').click(function(){
        $('#auto-row-tmpl').tmpl().appendTo('#product-auto-wrap');
        var newSelect = $('#product-auto-wrap .auto-select:last');
        
        newSelect.select2({
            theme: 'bootstrap'
        }).on("select2:select", function(e){
            $.post('/admin/product_auto_select', {id: e.params.data.id}, function(html){                
                newSelect.closest('.product-auto-item').find('.auto-value-wrap').remove();
                newSelect.closest('.product-auto-item').find('.auto-auto-wrap').after(html);
                newSelect.closest('.product-auto-item').find('.auto-value-wrap .auto-value-select').each(function(){
                    $(this).select2({
                        theme: 'bootstrap'
                    });
                });
                
            });
        });
    });
    
    $('#product-auto-wrap .auto-select').on("select2:select", function(e){
        var model_select = $(this);
            $.post('/admin/product_auto_select', {id: e.params.data.id}, function(html){                
                model_select.closest('.product-auto-item').find('.auto-value-wrap').remove();
                model_select.closest('.product-auto-item').find('.auto-auto-wrap').after(html);
                model_select.closest('.product-auto-item').find('.auto-value-wrap .auto-value-select').each(function(){
                    $(this).select2({
                        theme: 'bootstrap'
                    });
                });
                
            });
        });
    
    $('#product-filter-wrap').on('click', '.delete-filter', function(){
        if (confirm('Confirm?'))
        {
            $(this).closest('.product-filter-item').remove();
        }
    });
    
    $('#product-auto-wrap').on('click', '.delete-auto', function(){
        if (confirm('Confirm?'))
        {
            $(this).closest('.product-auto-item').remove();
        }
    });
    
    $('#add-discount').click(function(){
        $('#discount-row-tmpl').tmpl().appendTo('#promo-table tbody');
        $('#promo-table tbody tr:last .datepicker').datepicker({
            format: 'dd.mm.yyyy',
            maxDate: moment(),
            clearBtn: true
        });
    });
    
    $('#promo-table').on('click', '.delete-discount-row', function(){
        $(this).closest('tr').remove();
    });

</script>

<script id="filter-row-tmpl" type="text/x-jquery-tmpl">
    <div class="row product-filter-item">
        <div class="col-md-3 filter-filter-wrap">
            <div class="input-group">
                <span class="input-group-addon"><?= lang('FilterName') ?></span>
                <select style="width: 100%;" class="form-control filter-select">
                    <option value="">Selectati</option>
                    <?php foreach ($all_filters as $key => $name) { ?>
                        <option value="<?= $key ?>"><?= $name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-2 filter-value-wrap">
            
        </div>
        <div class="col-md-2 filter-value-wrap">
            
        </div>
        <div class="col-md-2 filter-value-wrap">
            
        </div>
        <div class="col-md-1">
            <button type="button" class="btn btn-danger delete-filter"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</script>

<script id="auto-row-tmpl" type="text/x-jquery-tmpl">
    <div class="row product-auto-item">
        <div class="col-md-3 auto-auto-wrap">
            <div class="input-group">
                <select style="width: 100%;" class="form-control auto-select" name="auto_value[mark][]">
                    <option value="0">Selectati marca</option>
                    <?php foreach ($all_autos as $key => $name) { ?>
                        <option value="<?= $key ?>"><?= $name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-3 auto-value-wrap">
            <select style="width: 100%;" class="form-control auto-value-select" name="auto_value[model][]">
                <option value="0">Selectati modelul</option>
            </select>
        </div>       
        <div class="col-md-1">
            <button type="button" class="btn btn-danger delete-auto"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</script>

<script id="discount-row-tmpl" type="text/x-jquery-tmpl">
    <tr>
        <td>
            <input required onkeydown="return false" class="form-control input-sm datepicker" type="text" name="Discount[StartDate][]" value="<?= date('d.m.Y', time()) ?>" />
        </td>
        <td>
            <input required onkeydown="return false" class="form-control input-sm datepicker" type="text" name="Discount[EndDate][]" value="<?=  date('d.m.Y', time() + 86400)  ?>" />
        </td>
        <td style="width: 130px;">
            <?= form_dropdown('Discount[PromoType][]', $this->config->item('promotion_types'), 'Percent', 'class="form-control input-sm" required') ?>
        </td>
        <td>
            <input name="Discount[Value][]" type="number" value="1" min="0.01" required class="form-control input-sm" />
        </td>
        <td class="text-center">
            <?= form_dropdown('Discount[PromoStatus][]', [1 => 'Da', 0 => 'Nu'], 1, 'class="form-control input-sm"') ?>
        </td>
        <td class="text-center">
            <span style="cursor: pointer;" class="btn btn-danger btn-sm delete-discount-row"><i class="glyphicon glyphicon-trash"></i></span>
        </td>
    </tr>
</script>

<style>
    
    #product-filter-wrap .product-filter-item,
    #product-auto-wrap .product-auto-item
    {
        padding: 4px 0;
    }
    
    #promo-table td {
        vertical-align: middle;
    }
    
</style>