<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title"><?= lang('Settings') ?></div>
            </div>            
        </div>
    </div>
    <div>        
        <div>
            <?= $this->session->flashdata('success'); ?>
            <?php if (isset($errors)) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $errors ?>
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <form class="col-md-12" method="post" enctype="multipart/form-data">                
                <?php foreach ($settings as $set) { ?>                
                <?php switch($set->Type){ 
                    case "number":
                    case "text":
                    case "email": { ?>
                <div class="col-md-6 form-group">
                    <label class="form-label"><?= $set->Title ?>*</label>
                    <input required type="<?= $set->Type ?>" name="Settings[<?= $set->ID ?>]" class="form-control" value="<?= $set->Value ?>" />
                </div> 
                <?php
                        break;}
                    case "image": { ?>
                
                     <div class="form-group">
                        
                            <div class="row">
                        <div class="wall col-md-6">
                            <div class="col-md-8">
                            <label class="form-label"><?= $set->Title ?></label>
                            <input type="file" name="Image_<?= $set->ID ?>" class="form-control" /><br />
                            </div>
                            <?php if($set->Value) { ?>
                            <img class="thumbnail" src="<?= base_url( "/public/uploads/settings/" . $set->Value ) ?>" />
                            <?php } ?>
                        </div> 

                        <div class="col-md-3">
                            <?php if($set->Value) { ?><a style="display: block" href="<?= site_url("admin/settings")."?delID=".$set->ID ?>" class="btn btn-info">Sterge</a><?php } ?>
                        </div>
                    </div>                    
                </div>
                
                <?php                        
                        break;}
                    
                    
                    
                }
                ?>
                             
                <?php } ?>
                <div class="clearfix"></div>
                <center><button name="submit" class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> <?= lang('Save') ?></button></center>
            </form>
        </div>
    </div>
    
</div>