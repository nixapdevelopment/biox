<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('page_auto') ?></h3>
            </div>            
        </div>
    </div>
    <div>
        <?= $this->session->flashdata('success') ?>
        <?php if (isset($errors)) { ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $errors ?>
        </div>
        <?php } ?>
            
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-auto" aria-controls="tab-auto" role="tab" data-toggle="tab"><?= lang('automarks') ?></a></li>
                <li role="presentation"><a href="#tab-models" aria-controls="tab-models" role="tab" data-toggle="tab"><?= lang('automodels') ?></a></li>
            </ul>

            <div class="tab-content">
                <br />
                <div role="tabpanel" class="tab-pane active" id="tab-auto">

                   
                    <br /><br />
                   
                        <form method="post" class="col-md-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <input class="form-control" type="text" name="mark" value="<?= isset($mark_data->name) ? $mark_data->name : '' ?>" placeholder="Honda" required="" />
                                </div>
                                <div class="col-md-2">
                                    <input type="hidden" name="type" value="mark" />
                                    <input type="hidden" name="id" value="<?= isset($mark_data->ID) ? $mark_data->ID : '0' ?>" />
                                    <button type="submit" class="btn btn-success"><?= isset($mark_data->ID) ? lang('Save') : lang('btn_add') ?></button>
                                </div>
                            </div>
                        </form>
                        <br /><br /><br />
                        <table class="table table-striped table-bordered" id="marks">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?= lang('text_mark') ?></th>                                        
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($marks as $mark) { ?>
                                <tr>
                                    <td style="width: 50px;">
                                        <?= $mark->ID ?>
                                    </td>
                                    <td style="width: 450px;">
                                        <?= $mark->name ?>
                                    </td>
                                    <td class="text-center" style="width: 50px;">
                                        <a href="<?= site_url('admin/auto', ['id' => $mark->ID, 'type'=>'mark'], true) ?>" title="Redacteaza"><i style="cursor: pointer;" class="glyphicon glyphicon-edit"></i></a>
                                        <a href="<?= site_url('admin/auto', ['mark_delID' => $mark->ID], true) ?>" title="Sterge"><i style="cursor: pointer;" class="glyphicon glyphicon-trash text-danger"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        

                </div>
                <div role="tabpanel" class="tab-pane" id="tab-models">
                                        
                    <div class="row">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <input required="" class="form-control" type="text" name="model" value="<?= isset($model_data->name) ? $model_data->name : '' ?>" placeholder="<?= lang('text_model_name') ?>"/>
                                </div>
                                <div class="col-md-2">
                                    <select name="mark" class="form-control">
                                        <?php foreach($marks as $mark){?>
                                            <?php (isset($model_data->markID) && $model_data->markID == $mark->ID) ? $selected = 'selected' : $selected = ''; ?>
                                            <option <?= $selected; ?> value="<?= $mark->ID ?>"><?= $mark->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <select name="start" class="form-control">
                                        <?php for($i=(int)Date('Y');$i>1900;$i--){?>
                                            <?php (isset($model_data->start) && $model_data->start == $i) ? $selected = 'selected' : $selected = ''; ?>
                                            <option <?= $selected; ?> value="<?= $i ?>"><?= $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <select name="end" class="form-control">
                                        <?php for($i=(int)Date('Y');$i>1900;$i--){?>
                                            <?php (isset($model_data->end) && $model_data->end == $i) ? $selected = 'selected' : $selected = ''; ?>
                                            <option <?= $selected; ?> value="<?= $i ?>"><?= $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <input type="hidden" name="type" value="model" />
                                    <input type="hidden" name="id" value="<?= isset($model_data->ID) ? $model_data->ID : '0' ?>" />
                                    <button type="submit" class="btn btn-success"><?= isset($model_data->ID) ? lang('Save') : lang('btn_add') ?></button>
                                </div>
                            </div>
                        </form>
                        <br /><br /><br />
                        
                        <table class="table table-striped table-bordered" id="models">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th><?= lang('text_model_name') ?></th>
                                    <th><?= lang('text_mark') ?></th>
                                    <th><?= lang('text_year_start') ?></th>
                                    <th><?= lang('text_year_end') ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($models as $model) { ?>
                                <tr>
                                    <td>
                                        <?= $model->ID ?>
                                    </td>
                                    <td style="width: 400px;">
                                        <?= $model->name ?>
                                    </td>
                                    <td style="width: 250px;">
                                        <?= $model->mark ?>
                                    </td>
                                    <td style="width: 150px;">
                                        <?= $model->start ?>
                                    </td>
                                    <td style="width: 150px;">
                                        <?= $model->end ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= site_url('admin/auto', ['id' => $model->ID, 'type'=>'model#tab-models'], true) ?>" title="Redacteaza"><i style="cursor: pointer;" class="glyphicon glyphicon-edit"></i></a>
                                        <a onclick="return confirm('Elimina acest model?')" href="?model_delID=<?= $model->ID ?>#tab-models" title="Sterge"><i style="cursor: pointer;" class="glyphicon glyphicon-trash text-danger"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                            
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
                        

        
            
    
<style>
    
    table.table tr td {
        vertical-align: middle;
    }
    
    table.table tr.ui-sortable-helper
    {
        height: 100px;
    }
    
</style>

<script>
$(document).ready(function() {
    $('#marks').DataTable( {
        "order": [[ 1, "asc" ]]
    });
    $('#models').DataTable({
        "order": [[ 2, "asc" ]]
    });
    
    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    } 

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
});
</script>
