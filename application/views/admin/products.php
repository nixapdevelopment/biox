<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <div class="page-title"><?= lang('Products') ?></div>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-filter-btn" href="<?= site_url('admin/edit_product') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddProduct') ?></a>
            </div>
        </div>
    </div>
    <div>
        <table id="product-table" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th><input type="checkbox" name="select_all" /></th>
                    <th><?= lang('Image') ?></th>
                    <th><?= lang('ProductName') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product) { ?>
                <tr>
                    <td><input type="checkbox" name="select_one" /></td>
                    <td>
                        <?php if (!empty($product->Thumb)) { ?>
                        <a class="fancybox" href="<?= base_url('public/uploads/products/' . $product->Image) ?>">
                            <img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" class="img-thumbnail" style="height: 35px;" />
                        </a>
                        <?php } ?>
                    </td>
                    <td style="min-width: 200px;"><?= $product->Name ?></td>
                    
                    <td>
                        <a style="position: relative; top: 1px;" href="<?= site_url('admin/edit_product', ['id' => $product->ProductID], true) ?>"><i class="fa fa-edit text-success"></i></a>&nbsp;&nbsp;
                        <a onclick="return confirm('Confirm?')" href="<?= site_url('admin/delete_product', ['id' => $product->ProductID], true) ?>"><i class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="text-center">
            <?= $pagination ?>
        </div>
    </div>
</div>

<style>
    #product-table td {
        vertical-align: middle;
    }
</style>

<script>

    $('.select2').select2({
        theme: 'bootstrap'
    });
    
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    
    $('.update-product-price').click(function(){
        var pid = $(this).attr('pid');
        var price = $(this).closest('td').find('input').val();
        $.post('<?= site_url('admin/update_product_price') ?>', {pid: pid, price: price}, function(){
            notif({
                msg: "Prețul produsului este actualizat",
                type: "success",
                position: "right"
            });
        });
    });
    
    $('.update-product-stock').click(function(){
        var pid = $(this).attr('pid');
        var stock = $(this).closest('td').find('input').val();
        $.post('<?= site_url('admin/update_product_stock') ?>', {pid: pid, stock: stock}, function(){
            notif({
                msg: "Stocul produsului este actualizat",
                type: "success",
                position: "right"
            });
        });
    });

</script>