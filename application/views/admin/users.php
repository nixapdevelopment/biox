<div class="container">
    <div class="dash-block">
        <h3><?= lang('Users') ?></h3>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Email') ?></th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('CompanyName') ?></th>
                            <th><?= lang('Region') ?></th>
                            <th><?= lang('ClientType') ?></th>
                            <th><?= lang('Status') ?></th>
                            <th><?= lang('RegDate') ?></th>
                            <th class="text-center"></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <form>
                            <tr>
                                <td></td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('Email') ?>" type="text" name="Email" class="form-control" />
                                        <span class="input-group-btn">
                                            <button onclick="$('input[name=Email]').val('')"  class="btn btn-danger" type="button">Clear</button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('Name') ?>" type="text" name="Name" class="form-control" />
                                        <span class="input-group-btn">
                                            <button onclick="$('input[name=Name]').val('')"  class="btn btn-danger" type="button">Clear</button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('CompanyName') ?>" onchange="return false;" type="text" name="CompanyName" class="form-control" />
                                        <span class="input-group-btn">
                                            <button onclick="$('input[name=CompanyName]').val('')"  class="btn btn-danger" type="button">Clear</button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <?= form_dropdown('Region', $this->config->item('regions'), $this->input->get('Region'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <?= form_dropdown('Type', ['' => 'All', 'Manager' => 'Manager', 'Angro' => 'Angro', 'Retail' => 'Retail'], $this->input->get('Type'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <?= form_dropdown('Status', ['' => 'All', 'Active' => 'Active', 'NotConfirmed' => lang('NotConfirmed')], $this->input->get('Status'), 'class="form-control select-2"') ?>
                                </td>
                                <td></td>
                                <td style="min-width: 230px; text-align: center;">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> <?= lang('Filter') ?></button>
                                    <a href="<?= site_url('admin/users') ?>" type="submit" class="btn btn-danger"><?= lang('ClearFilters') ?></a>
                                </td>
                            </tr>
                        </form>
                        <?php foreach ($users as $user) { ?>
                        <tr>
                            <td class="text-center"><a href="<?= site_url('admin/user/' . $user->ID) ?>"><?= $user->ID ?></a></td>
                            <td><?= $user->UserName ?></td>
                            <td><?= $user->Name ?></td>
                            <td><?= $user->CompanyName ?></td>
                            <td><?= $this->config->item($user->Region, 'regions') ?></td>
                            <td><?= $user->Type ?></td>
                            <td><?= $user->Status ?></td>
                            <td><?= date('d.m.Y', strtotime($user->RegDate)) ?></td>
                            <td class="text-center">
                                <a href="<?= site_url('admin/user/' . $user->ID, $_GET) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?= $pagination ?>
                </div>
            </div>
        </div>
    </div>
</div>