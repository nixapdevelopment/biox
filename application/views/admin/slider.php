<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title"><?= lang('Slider') ?></div>
            </div>            
        </div>
    </div>
    <div>
        <?= $this->session->flashdata('success') ?>
        <?php if (isset($errors)) { ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $errors ?>
        </div>
        <?php } ?>
            
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?= lang('GeneralData') ?></a></li>
                <li role="presentation"><a href="#tab-images" aria-controls="tab-images" role="tab" data-toggle="tab"><?= lang('Banner') ?></a></li>
            </ul>

            <div class="tab-content">
                <br />
                <div role="tabpanel" class="tab-pane active" id="tab-main">

                    <form class="form-inline" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input required type="file" name="image" class="form-control" />
                        </div>
                        <input type="hidden" name="slide_type" value="slider" />
                        <button name="submit" class="btn btn-primary" type="submit">Incarca</button>
                        <a class="btn btn-link">( JPG sau PNG )</a>
                    </form>
                    <br /><br />
                    
                    <div class="row">
                        <form method="post" id="slider-form" class="col-md-8">
                            <table class="table table-striped table-bordered" id="slider_table">
                                <tbody>
                                    <?php foreach ($sliders as $slide) { ?>
                                    <tr>
                                        <td style="width: 40px; font-size: 25px;">
                                            <i style="cursor: move;" class="glyphicon glyphicon-menu-hamburger"></i>
                                        </td>
                                        <td class="text-center">
                                            <a class="fancybox" href="<?= base_url('public/uploads/sliders/' . $slide->Image) ?>">
                                                <img style="height: 100px;" src="<?= base_url('public/uploads/sliders/' . $slide->Image) ?>" class="img-thumbnail" />
                                            </a>
                                            <input type="hidden" name="sid[]" value="<?= $slide->ID ?>" />
                                        </td>
                                        <td style="width: 350px;">
                                            <input class="form-control" name="Link[<?= $slide->ID ?>]" value="<?= $slide->Link ?>" />
                                        </td>
                                        <td class="text-center">
                                            <a href="?delID=<?= $slide->ID ?>"><i style="cursor: pointer;" class="glyphicon glyphicon-trash text-danger"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-success btn-lg">Salvare</button>
                        </form>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="tab-images">
                    
                    <form class="form-inline" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input required type="file" name="image" class="form-control" />
                        </div>
                        <input type="hidden" name="slide_type" value="banner" />
                        <button name="submit" class="btn btn-primary" type="submit"><?= lang("Save") ?></button>
                        <a class="btn btn-link">( JPG sau PNG )</a>
                    </form>
                    <br /><br />
                    
                    <div class="row">
                        <form method="post" id="banner-form" class="col-md-8">
                            <table class="table table-striped table-bordered" id="banner_table">
                                <tbody>
                                    <?php foreach ($banners as $banner) { ?>
                                    <tr>
                                        <td style="width: 40px; font-size: 25px;">
                                            <i style="cursor: move;" class="glyphicon glyphicon-menu-hamburger"></i>
                                        </td>
                                        <td class="text-center">
                                            <a class="fancybox" href="<?= base_url('public/uploads/sliders/' . $banner->Image) ?>">
                                                <img style="height: 100px;" src="<?= base_url('public/uploads/sliders/' . $banner->Image) ?>" class="img-thumbnail" />
                                            </a>
                                            <input type="hidden" name="sid[]" value="<?= $banner->ID ?>" />
                                        </td>
                                        <td style="width: 350px;">
                                            <input class="form-control" name="Link[<?= $banner->ID ?>]" value="<?= $banner->Link ?>" />
                                        </td>
                                        <td class="text-center">
                                            <a href="?delID=<?= $banner->ID ?>"><i style="cursor: pointer;" class="glyphicon glyphicon-trash text-danger"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-success btn-lg">Salvare</button>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
                        

        
            
    
<style>
    
    table.table tr td {
        vertical-align: middle;
    }
    
    table.table tr.ui-sortable-helper
    {
        height: 100px;
    }
    
</style>

<script>

    var sort = [];
    $( "table#slider_table tbody" ).sortable({
        placeholder: "ui-state-highlight",
	update: function( event, ui ) {
            $.post('<?= site_url('admin/slider_sort') ?>', $('#slider-form').serialize(), function(){
                notif({
                    msg: "Sortarea salvata",
                    type: "success",
                    position: "right"
                });
            });
        },
        start: function(e, ui) {
            ui.placeholder.height(ui.item.height());
        }
    });
    
    
    $( "table#banner_table tbody" ).sortable({
        placeholder: "ui-state-highlight",
	update: function( event, ui ) {
            $.post('<?= site_url('admin/slider_sort') ?>', $('#banner-form').serialize(), function(){
                notif({
                    msg: "Sortarea salvata",
                    type: "success",
                    position: "right"
                });
            });
        },
        start: function(e, ui) {
            ui.placeholder.height(ui.item.height());
        }
    });

</script>