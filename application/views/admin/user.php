<div class="container">
    <div class="dash-block">
        <h3 class="text-center"><?= lang('Users') ?></h3>
        <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <form id="registration-form" method="post" class="col-md-offset-2 col-md-8" enctype="multipart/form-data">
            <div class="text-danger">
                <?= validation_errors() ?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('UserEmail') ?> <span class="text-danger">*</span></label>
                        <input name="UserName" type="email" value="<?= set_value('UserName', isset($user->UserName) ? $user->UserName : '') ?>" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Password') ?> <span class="text-danger">*</span></label>
                        <input name="Password" type="password" value="<?= set_value('Password') ?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('PasswordConfirmation') ?> <span class="text-danger">*</span></label>
                        <input name="PasswordConfirmation" type="password" value="<?= set_value('PasswordConfirmation') ?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Name') ?> <span class="text-danger">*</span></label>
                        <input name="Name" type="text" value="<?= set_value('Name', isset($user->Name) ? $user->Name : '') ?>" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Phone') ?></label>
                        <input name="Phone" type="text" value="<?= set_value('Phone', isset($user->Phone) ? $user->Phone : '') ?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Zip') ?></label>
                        <input name="Zip" type="text" value="<?= set_value('Zip', isset($user->Zip) ? $user->Zip : '') ?>" class="form-control" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('ClientType') ?> <span class="text-danger">*</span></label>
                        <?= form_dropdown('ClientType', ['' => lang('SelectClientType'), 'Manager' => 'Manager', 'Retail' => lang('Retail'), 'Angro' => lang('Angro')], set_value('ClientType', isset($user->Type) ? $user->Type : ''), 'class="form-control" required') ?>
                    </div>
                    <div class="form-group company-fields">
                        <label class="control-label"><?= lang('CompanyName') ?></label>
                        <input name="CompanyName" type="text" value="<?= set_value('CompanyName', isset($user->CompanyName) ? $user->CompanyName : '') ?>" class="form-control" />
                    </div>
                    <div class="form-group company-fields">
                        <label class="control-label"><?= lang('Activitation') ?></label>
                        <input name="Activitation" type="text" value="<?= set_value('Activitation', isset($user->Activitation) ? $user->Activitation : '') ?>" class="form-control" />
                    </div>
                    <div class="form-group company-fields">
                        <label class="control-label"><?= lang('Files') ?></label>
                        <input name="Files[]" type="file" multiple class="form-control" accept=".pdf,.doc,.docx,.xls,.xlsx,.png,.jpg,.gif,.tiff,.tif" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Region') ?> <span class="text-danger">*</span></label>
                        <?= form_dropdown('Region', $this->config->item('regions'), set_value('Region', isset($user->Region) ? $user->Region : ''), 'class="form-control select2" required') ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Address') ?></label>
                        <input type="text" name="Address" class="form-control" value="<?= set_value('Address', isset($user->Address) ? $user->Address : '') ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?= lang('Save') ?></button>
            </div>
        </form>
    </div>
</div>