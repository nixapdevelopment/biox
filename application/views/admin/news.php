<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <div class="page-title"><?= lang('News') ?></div>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-filter-btn" href="<?= site_url('admin/edit_news') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddNews') ?></a>
            </div>
        </div>
    </div>
    <div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th style="width: 7%;" class="text-center">ID</th>
                    <th><?= lang('Title') ?></th>
                    <th>Url</th>
                    <th><?= lang('Text') ?></th>
                    <th><?= lang('Date') ?></th>
                    <th><?= lang('Status') ?></th>
                    <th style="width: 7%;" class="text-center"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($news as $item) { ?>
                <tr>
                    <td style="width: 7%;" class="text-center"><?= $item->ID ?></td>
                    <td><?= $item->Title ?></td>
                    <td><a href="<?= site_url($item->Link) ?>" target="_blank"><?= site_url($item->Link) ?></a></td>
                    <td><?= mb_substr(strip_tags($item->Text), 0, 80) ?></td>
                    <td><?= date('d.m.Y', strtotime($item->Date)) ?></td>
                    <td><?= $item->Status ?></td>
                    <td style="width: 7%;" class="text-center">
                        <a href="<?= site_url('admin/edit_news', ['id' => $item->ID]) ?>"><i class="fa fa-edit text-primary"></i></a>
                        &nbsp;&nbsp;&nbsp;
                        <a onclick="return confirm('Confirm?')" href="<?= site_url('admin/delete_news', ['id' => $item->ID], true) ?>"><i class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div>
            <?= $pagination ?>
        </div>
    </div>
</div>