<div class="container">
    <div class="page-title">Panoul de administrare BIOX.MD</div>
    <div class="row">
        <div class="col-md-6">
            <a href="<?= site_url('admin/products') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-shopping-bag"></i>
                    <h4><?= $product_count ?> - produse total</h4>
                </div>
            </a>
        </div>
        <div class="col-md-6">
            <a href="<?= site_url('admin/categories') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-list-ul"></i>
                    <h4><?= $category_count ?> - categorii</h4>
                </div>
            </a>
        </div>
    </div>
    <div>
            <?= $this->session->flashdata('success'); ?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Message</th>
                    <th>Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($feedbacks as $feedback) { ?>
                <tr>
                    <td><?= $feedback->Name ?></td>
                    <td><?= $feedback->Email ?></td>
                    <td><?= $feedback->Phone ?></td>
                    <td style="width: 40%;"><?= $feedback->Message ?></td>
                    <td><?= date('d.m.Y H:i', strtotime($feedback->Date)) ?></td>
                    <td style="vertical-align: middle;" class="text-center"><a onclick="return confirm('Elimina acest feedback?')" href="?delID=<?= $feedback->ID ?>"><i class="glyphicon glyphicon-trash text-danger"></i></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
</div>