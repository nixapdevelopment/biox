<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php if (count($last_projects) > 0) { ?>
<div class="home-news" style="background-color: inherit;">
    <div class="container">            
        <h3><i class="fa fa-newspaper-o"></i> <?= lang('Projects')?></h3>
        <div class="row">
            <?php foreach ($last_projects as $project) { ?>
            <div class="col-md-4">
                <a href="<?= site_url($project->Link) ?>" class="hw-item">
                    <strong><?= $project->Title ?></strong>
                    <div class="hw-thumb"><img src="<?= base_url('public/uploads/projects/' . $project->Thumb) ?>" alt="<?= $project->Title ?>"></div>
                    <table class="table table-bordered">
                        <tr>
                            <td><strong>Material:</strong></td>
                            <td><?= $project->Material ?></td>
                        </tr>
                        <tr>
                            <td><strong>Constructor:</strong></td>
                            <td><?= $project->Constructor ?></td>
                        </tr>
                        <tr>
                            <td><strong>Beneficiar:</strong></td>
                            <td><?= $project->Beneficiar ?></td>
                        </tr>
                    </table>
                </a>
            </div>            
            <?php } ?>
        </div>
    </div>
    <div><?= $pagination ?></div>
</div>
<div class="clearfix"></div>
<?php } ?>