<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php if (count($last_news) > 0) { ?>
<div class="home-news" style="background-color: inherit;">
    <div class="container">            
        <h3><i class="fa fa-newspaper-o"></i> <?= lang('NewsUpperCase')?></h3>
        <div class="row">
            <?php foreach ($last_news as $news) { ?>
            <div class="nnews-item col-md-4">
                <div class="nnews-item-inn">
                    <a href="<?= site_url($news->Link) ?>">
                        <div class="nnews-thumb"><img src="<?= base_url('public/uploads/news/' . $news->Thumb) ?>" alt="<?= $news->Title ?>"></div>
                        <h4><?= $news->Title ?></h4>
                        <time><i class="fa fa-clock-o"></i> <?= date('d.m.Y', strtotime($news->Date)) ?></time>
                        <div class="nnews-item-short">
                            <?= mb_substr(strip_tags($news->Text), 0, 280) ?>...
                        </div>
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div><?= $pagination ?></div>
</div>
<div class="clearfix"></div>
<?php } ?>