<section class="content">
    <div class="container">
        <div class="home-first">
            <div class="row">
                <div class="col-md-4">
                    <?php if(count($images) > 0){ ?>
                    <div>
                        <?php $main = reset($images) ?>
                        <img class="img-thumbnail item-img" src="<?= base_url('public/uploads/products/' . $main->Image) ?>" />
                    </div>
                    <HR />
                    <div class="row">
                        <?php foreach ($images as $img) { ?>
                        <div class="col-md-3">
                            <a class="fancybox" href="<?= base_url('public/uploads/products/' . $img->Image) ?>" rel="biox-gallery"><img class="img-thumbnail" src="<?= base_url('public/uploads/products/' . $img->Thumb) ?>" /></a>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } else { ?>
                        <div>
                            <img class="img-thumbnail item-img" src="<?= base_url('public/images/no-image.png') ?>" title="Nu este imagine" />
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-8">
                   <h1><?= $product->Name ?></h1>
                    <hr /> 
                    <div><?= $product->ProductText ?></div>
                    
                </div>
            </div>            
        </div>
    </div>
</section>