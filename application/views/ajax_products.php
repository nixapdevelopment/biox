
<div class="category-list">
    <div class="row">
        <?php foreach ($products as $product) { ?>
            <div class="col-md-3">
                <a class="product-item" href="<?= site_url($product->CategoryLink . '/' . $product->Link) ?>">
                    <div class="product-thumb"><img class="item-img" src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="" /></div>
                    <strong><?= $product->Name ?></strong>
                </a>
            </div>
        <?php } ?>
    </div>
</div>
<div class="clearfix"></div>
<?php $count_pages = ceil($count / $per_page); ?>
<?php if ($count_pages > 1) { ?>
    <div class="pagination">
        <ul id="products-page" class="category-sort">
            <?php
            $pagination_start = '';
            $pagination_middle = '';
            $pagination_end = '';
            $active = '';

            for ($i = 1; $i <= $count_pages; $i++) {
                if ($i > $page - 5 && $i < $page + 5) {
                    $page == $i ? $active = 'active' : $active = '';
                    $pagination_middle .= '<li><a class="' . $active . '" data="' . $i . '">' . $i . '</a></li>';
                }
                if ($page - 5 >= 1) {
                    $page - 5 > 1 ? $dotss = "<li>...</li>" : $dotss = "";
                    $pagination_start = '<li><a data="1">1</a></li>' . $dotss;
                }
                if ($page + 5 <= $count_pages) {
                    $page + 5 < $count_pages ? $dotse = "<li>...</li>" : $dotse = "";
                    $pagination_end = $dotse . '<li><a data="' . $count_pages . '">' . $count_pages . '</a></li>';
                }
                ?>

            <?php }
            ?>

            <?= $pagination_start . $pagination_middle . $pagination_end; ?>
        </ul>
    </div>
<?php } ?>