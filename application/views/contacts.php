<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="page-title"><?= $page->Title ?></div>
        <br />
        
        <div class="row">
                    <div class="service-item col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="contact-icon"><i class="fa fa-phone"></i></div>
                            </div>
                            <div class="col-md-9">								
                                <h3><?= lang('Call_Us')?></h3>
                                <div class="clearfix"></div>
                                <p>
                                    <a href="tel:<?= $this->data['settings']['phone_number'] ?>"><?= $this->data['settings']['phone_number'] ?></a>
                                </p>
                            </div>
                        </div>
                    </div>	
                    <div class="service-item col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="contact-icon"><i class="fa fa-envelope"></i></div>
                            </div>
                            <div class="col-md-9">								
                                <h3><?=lang('Email')?></h3>
                                <div class="clearfix"></div>
                                <p>
                                    
                                </p>
                            </div>
                        </div>
                    </div>	
                    <div class="service-item col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                            </div>
                            <div class="col-md-9">								
                                <h3><?= lang('Address')?></h3>
                                <div class="clearfix"></div>
                                <p>
                                    
                                </p>
                            </div>
                        </div>
                    </div>						
                </div>
        <div class="row">
            <div class="col-md-6">
                <?= $page->Text ?>
                <br />
                         
                
                <form id="feedback-form">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Name')?></label>
                        <input type="text" class="form-control" name="Name" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Email')?></label>
                        <input type="email" class="form-control" name="Email" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Phone')?></label>
                        <input type="text" class="form-control" name="Phone" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= lang('Message')?></label>
                        <textarea class="form-control" name="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit"><?= lang('Send')?></button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <iframe width="100%" height="350" frameborder="0" src="https://point.md/ro/mawp/47.05828245628w4665/28.855075836181637/15/584e7f0e27a434000a4b69b1?embed=1"></iframe>
            </div>
        </div>
    </div>
</section>

<script>

    $('#feedback-form').submit(function(e){
        e.preventDefault();
        $.post('<?= site_url('main/feedback') ?>', $('#feedback-form').serialize(), function(){
            $('#feedback-form').trigger('reset');
            notif({
                msg: "Feedback sended",
                type: "success",
                position: "right"
            });
        });
    });

</script>