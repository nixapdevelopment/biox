<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="bread">
    <div class="container">        
        <?= breadscrumbs($this->breadscrumbs) ?>        
    </div>
</section>

<section class="content">
    <div class="container">
        <h1><?= lang("Search").": ".$search ?></h1>
        <br />  
        
        <div class="row">
            <div class="col-md-3">
               <form method="post" class="filters-block">
                   <div class="filter-item">                            
                        <div class="filter-title">
                            <select id="filter-mark" name="mark" class="form-control">
                                <option value="0"><?= lang('SelectAutoMark') ?></option>
                            <?php foreach ($auto_filters['marks'] as $mark) { ?>                                
                                <option value='<?= $mark->ID ?>'><?= $mark->name ?></option>                                
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="filter-item">
                        <div class="filter-title">
                            <select id="filter-model" name="model" class="form-control">
                                <option value="0"><?= lang('SelectAutoModel') ?></option>                                
                            </select>
                        </div>
                    </div>  
                   
                    <div class="filter-item">
                        <div class="filter-title">Preț, MDL <span class="pull-right"><?= $min_price ?> LEI - <?= $max_price ?> LEI</span></div>
                        <div class="filter-inn">
                            <input id="filter1" name="f[1]" type="text" class="span2" value="" data-slider-unit="lei" data-slider-min="<?= $min_price ?>" data-slider-max="<?= $max_price ?>" data-slider-step="0.01" data-slider-value="[<?= $this->input->get('f[1]') == "" ? "$min_price,$max_price" : $this->input->get('f[1]') ?>]"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php foreach ($filters as $filter) { ?>
                    <?php if ($filter['data']->Type == 'String') { ?>
                    <div class="filter-item">
                        <div class="filter-title"><?= $filter['data']->Name ?></div>
                        <div class="filter-inn">
                            <?php foreach ($filter['results'] as $result) { ?>
                            <div class="col-md-12">                                
                                <label>
                                    <input <?= $this->input->get("f[" . $filter['data']->ID . "][" . $result . "]") == $result ? 'checked' : '' ?> class="filter" name="<?= "f[" . $filter['data']->ID . "][" . $result . "]" ?>" value="<?= $result ?>" type="checkbox" /> <?= $result ?>
                                </label>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php } elseif ($filter['data']->Type == 'Number') { ?>
                    <div class="filter-item">
                        <div class="filter-title"><?= $filter['data']->Name ?><span class="pull-right"><?= $filter['min'] ?> - <?= $filter['max'] ?></span></div>
                        <div class="filter-inn">
                            <input id="filter<?= $filter['data']->ID ?>" name="f[<?= $filter['data']->ID ?>]" type="text" class="span2" value="" data-slider-unit="" data-slider-min="<?= $filter['min'] ?>" data-slider-max="<?= $filter['max'] ?>" data-slider-step="1" data-slider-value="[<?= $this->input->get('f[' . $filter['data']->ID . ']') == "" ? $filter['min'] . "," . $filter['max'] : $this->input->get('f[' . $filter['data']->ID . ']') ?>]"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </form>
            </div>
            <div class="col-md-9">
                <div class="category-options">
                    <div class="row">
                        <div class="col-md-6">
                            <span>Articole pe pagină:</span>
                            <ul id="per-page" class="category-sort">
                                <li><a data="8">8</a></li>
                                <li><a data="24">24</a></li>
                                <li><a data="48">48</a></li>
                                <li><a data="100">100</a></li>
                            </ul>
                            <input type="hidden" name="per_page" value="<?= $products_per_page ?>" />
                        </div>
                        <div class="col-md-6">
                            <span>Sortează după:</span>
                            <ul id="product-sort" class="category-sort">
                                <li><a data="name">Nume</a></li>
                                <li><a data="price">Preț</a></li>
                                <li><a data="date">Dată</a></li>
                            </ul>
                            <input type="hidden" name="sort" value="date" />
                        </div>
                    </div>
                </div>
                <div id="product-wrap">
                    
                </div>
                <input type="hidden" name="page" value="1" /> 
                <input type="hidden" name="search" value="<?= $search ?>" />
            </div>
        </div>
                        
        
    </div>
</section>
<script>
    $('.filter-item input[id^=filter]').each(function(){
        $('#' + $(this).attr('id')).slider({
            formatter: function(value)
            {
                var val = value.toString().split(',');
                return val[0] + ' - ' + val[1];
            }
        });
        $('#' + $(this).attr('id')).on("change", function(slideEvt) {
            var val = $(slideEvt.currentTarget).val().toString().split(',');
            $(this).closest('.filter-item').find('.filter-title > span').text(val[0] + ' ' + $(slideEvt.currentTarget).attr('data-slider-unit') + ' - ' + val[1] + ' ' + $(slideEvt.currentTarget).attr('data-slider-unit'));
        });
        $('#' + $(this).attr('id')).on("slideStop", applyFilters);
    });
    
    $('.filter-item .filter').change(applyFilters);
    
    function applyFilters()
    {
        $('#product-wrap').html(LOADER);
        var a, index, entry;
        var filters_arr = [];
        a = $('form.filters-block').serializeArray();
        for (index = 0; index < a.length; ++index)
        {
            entry = a[index];
            filters_arr.push(entry.name + "=" + entry.value);
        }
        
        var filters = filters_arr.join('&');
        
        var sort = '&sort=' + $('input[name=sort]').val();
        var per_page = '&per_page=' + $('input[name=per_page]').val();
        var page = '&page=' + ($('input[name=page]').val() == undefined ? '1' : $('input[name=page]').val());
        var q = '&q=' + $('input[name=search]').val();
        
        var query = filters + sort + per_page + page + q;
        
        if(history.pushState)
        {
            var stateObject = {};
            history.pushState(stateObject, $(document).find('title').text(), window.location.pathname + '?' + query);
        }
        
        $.post('/main/ajaxProducts', {query: query}, function(html){
            $('#product-wrap').html(html);
        });
    }
    
    $('#per-page li a').click(function(){
        $('#per-page li a').removeClass('active');
        $(this).addClass('active');
        $('input[name="per_page"]').val($(this).attr('data'));
        applyFilters();
    });
    
    $('#product-sort li a').click(function(){
        $('#product-sort li a').removeClass('active');
        $(this).addClass('active');
        $('input[name="sort"]').val($(this).attr('data'));
        $('input[name="page"]').val(1);
        applyFilters();
    });
    
    $(document).on('click', '#products-page li a', function(){
        $('#products-page li a').removeClass('active');
        $(this).addClass('active');
        $('input[name="page"]').val($(this).attr('data'));
        applyFilters();
    });
    
    $('#filter-model, #filter-mark').select2({
        theme: 'bootstrap'
    });
    
    $("#filter-mark").on('change', function () {
        if($(this).val() == 0) {
            $('#filter-model').html("<option value='0'><?= lang('SelectAutoModel') ?></option>");
            $('#filter-model').select2({
                theme: 'bootstrap'
            });
            applyFilters();
            return;
        }
        $.post('<?= site_url('main/ajaxAutoModels') ?>', {ID:$(this).val()}, function (html) {
            $('#filter-model').html(html);
            $('#filter-model').select2({
                theme: 'bootstrap'
            });
        }); 
    });
    
    $("#filter-model").on('change', function () {         
        applyFilters();
    });
    
    applyFilters();
</script>