<table>
    <tr>
        <td><?= lang('Name') ?></td>
        <td><?= $user->Name ?></td>
    </tr>
    <tr>
        <td><?= lang('Name') ?></td>
        <td><?= $this->config->item('regions')[$user->Region] ?></td>
    </tr>
    <tr>
        <td><?= lang('Address') ?></td>
        <td><?= $user->Address ?></td>
    </tr>
    <tr>
        <td><?= lang('Zip') ?></td>
        <td><?= $user->Zip ?></td>
    </tr>
    <tr>
        <td><?= lang('Phone') ?></td>
        <td><?= $user->Phone ?></td>
    </tr>
    <tr>
        <td><?= lang('Email') ?></td>
        <td><?= $user->UserName ?></td>
    </tr>
</table>
<div class="panel panel-info">
    <div class="panel-heading"><span><i class="glyphicon glyphicon-lock"></i></span> Payment</div>
    <div class="panel-body">
        <div>
            <div class="panel panel-info">
                <div class="panel-body">
                    <?php if (empty($contents)) { ?>
                    <h3 class="text-center"><?= lang('CartEmpty') ?></h3>
                    <?php } else { ?>
                    <table>
                        <tr>
                            <th>Produs</th>
                            <th>Cantitate</th>
                            <th>Pret</th>
                            <th>Subtotal</th>
                        </tr>
                        <?php foreach ($contents as $item) { ?>
                        <tr>
                            <td><?= $item['Name'] ?> (<?= $item['CategoryName'] ?>)</td>
                            <td><?= $item['Quantity'] ?></td>
                            <td><?= $item['Price'] ?></td>
                            <td><?= $item['Quantity'] * $item['Price'] ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <h4 class="text-right"><?= lang('CartTotal') ?> <strong><span id="cart-total"><?= number_format($total, 2) ?></span> MDL</strong></h4>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>