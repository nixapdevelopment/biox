<!DOCTYPE html>
<html style="overflow-y: scroll;">
    <head>
        <meta charset="UTF-8">
        <title>ADMIN: BIOX</title>
        <link rel="stylesheet" href="<?= base_url('public/css/stylesheet.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/admin/css/admin_styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/theme.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/fancybox/jquery.fancybox.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/easyTree.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/notifIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/select2.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-slider.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/jquery-ui/jquery-ui.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/jquery-ui/jquery-ui.theme.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/owl/owl.carousel.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/owl/owl.theme.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/admin/css/DataTables.css') ?>">
        
        <script>var LOADER = '<div class="text-center"><img src="<?= site_url('public/images/preloader.gif') ?>" /></div>';</script>
        <script>var SITE_LANG = '<?= $this->config->item('languages')[$this->langID]['Slug'] ?>';</script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="<?= base_url('public/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.pack.js') ?>"></script>        
        <script src="<?= base_url('public/js/easyTree.min.js') ?>"></script>
        <script src="<?= base_url('public/js/validate.js') ?>"></script>
        <script src="<?= base_url('public/js/owl/owl.carousel.js') ?>"></script>
        <script src="<?= base_url('public/js/DataTables.bootstrap.js') ?>"></script>
        <script>
        // error messages
        jQuery.extend(jQuery.validator.messages, {
            required: "Completați câmpul",
            remote: "Please fix this field.",
            email: "Vă rugăm să introduceți o adresă de e-mail validă",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
            minlength: jQuery.validator.format("Please enter at least {0} characters."),
            rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
            range: jQuery.validator.format("Please enter a value between {0} and {1}."),
            max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
            min: jQuery.validator.format("Vă rugăm să introduceți o valoare mai mare sau egală cu {0}")
        });
        </script>
        <script src="<?= base_url('public/js/notifIt.min.js') ?>"></script>
        <script src="<?= base_url('public/js/select2.min.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-datepicker.js') ?>"></script>
        <script src="<?= base_url('public/js/ckeditor/ckeditor.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery.tmpl.min.js') ?>"></script>
        <script src="<?= base_url('public/js/moment.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-slider.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery-ui/jquery-ui.min.js') ?>"></script>
        <script src="<?= base_url('public/js/scripts.js') ?>"></script>
    </head>
    <body class="admin-panel">
        <header class="header">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <?php if (!empty($this->_user)) { ?>
                        <div class="left-top-bar col-md-6">
                            <?= lang('Wellcome') ?>, <?= $this->_user->Name ?>
                        </div>
                        <div class="pull-right text-right col-md-3">
                            <a target="_blank" href="<?= site_url() ?>">Vezi front-end</a> | 
                            <a href="<?= site_url('logout') ?>"><?= lang('Logout') ?></a>
                        </div>
                        <?php } else { ?>
                        <div class="right-top-bar col-md-8">
                            <a href="<?= site_url('login') ?>"><?= lang('Login') ?></a> | 
                            <a href="<?= site_url('registration') ?>"><?= lang('Registration') ?></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            
            <div class="header-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <a class="logo-block" href="<?= site_url('../ro/admin') ?>"><img src="<?= $this->data['settings']['logo'] ? base_url('public/uploads/settings/' . $this->data['settings']['logo']) : base_url('public/images/logo.png') ?>" alt="BIOX COMERT"></a>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="header-contact header-slogan col-md-4">
                                    <strong>Nu comercializam produse, oferim solutii</strong>
                                </div>
                                <div class="header-contact col-md-4">
                                    <p><i class="fa fa-clock-o"></i> Orele de lucru</p>
                                    <strong>Luni - Vineri 09:00-18:00</strong>
                                </div>
                                <div class="header-contact col-md-4">
                                    <p><i class="fa fa-envelope"></i> Scrie-ne un mesaj</p>
                                    <strong><a href="#">info@biox.md</a></strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <a href="tel:<?= $this->data['settings']['phone_number'] ?>" class="header-phone"><i class="fa fa-phone"></i> <?= $this->data['settings']['phone_number'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
            <nav class="header-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="navigation">
                                <?= menu($this->menu[$this->menuType]) ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        <p>&nbsp;</p>