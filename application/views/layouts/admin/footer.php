
                                              
<footer class="footer">
    <div class="alternative-v-line"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4 class="mini-title">Contacte</h4>
                <ul class="footer-nav">
                    <li>BIOX COMERT SRL <br>Republica Moldova, mun. Chișinău <br>bd. Ștefan cel Mare 200, of. 101<br><br></li>
                    <li>
                        <p>Telefon: +373 22 74-73-92</p>
                        <p>Orange: +373 69 103-819</p>
                        <p>E-mail: <a href="mailto:info@biox.md">info@biox.md</a></p>
                    </li>
                </ul>
            </div>
            <div class="footer-newsletter col-md-3">
                <h4 class="mini-title">Newsletter</h4>
                <p>Abonați-vă la noutățile noastre, pentru a fi la curent cu toate noutățile companiei.</p>
                <form id="subscribe-form">
                    <input type="Email" name="Email" placeholder="introduce-ți adresa de email">
                    <button type="submit"><i class="fa fa-envelope"></i></button>
                </form>
                <div class="footer-social">
                    <a href=""><i class="fa fa-facebook-square"></i></a>
                    <a href=""><i class="fa fa-twitter-square"></i></a>
                    <a href=""><i class="fa fa-google-plus-square"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <h4 class="mini-title">Produse</h4>
                <ul class="footer-nav">    
                    <li><a href="#">Geogrile</a></li>
                    <li><a href="#">Geotextile</a></li>
                    <li><a href="#">Geocompozite</a></li>
                    <li><a href="#">Geomembrane</a></li>
                    <li><a href="#">Hidroizolatii</a></li>
                    <li><a href="#">Dispozitive de rost</a></li>
                    <li><a href="#">Aparate de reazem pentru poduri</a></li>
                    <li><a href="#">Fibre de celuloza pentru MASF</a></li>
                    <li><a href="#">Mase bituminoase</a></li>
                    <li><a href="#">Diguri PVC</a></li>
                    <li><a href="#">Sisteme Ancorare</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4 class="mini-title">Aplicații</h4>
                <ul class="footer-nav">
                    <li><a href="#">Geogrile</a></li>
                    <li><a href="#">Geotextile</a></li>
                    <li><a href="#">Geocompozite</a></li>
                    <li><a href="#">Geomembrane</a></li>
                    <li><a href="#">Hidroizolatii</a></li>
                    <li><a href="#">Dispozitive de rost</a></li>
                    <li><a href="#">Aparate de reazem pentru poduri</a></li>
                    <li><a href="#">Fibre de celuloza pentru MASF</a></li>
                    <li><a href="#">Mase bituminoase</a></li>
                    <li><a href="#">Diguri PVC</a></li>
                    <li><a href="#">Sisteme Ancorare</a></li>
                </ul>
            </div>
        </div>

        <div class="footer-copy">
            <div class="row">
                <div class="col-md-6"><?= $this->data['settings']['copyright'] ?></div>
                <div class="col-md-6">
                    <span class="by_nixap">by <a href="http://nixap.eu" target="_blank" title="">nixap.eu</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>