<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <title><?= $this->Title ?></title>
        <meta name="description" content="<?= $this->Description ?>">
        <meta name="keywords" content="<?= $this->Keywords ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="<?= base_url('public/css/stylesheet.css') ?>" />	
        <link rel="stylesheet" href="<?= base_url('public/css/responsive.css') ?>" />	
        <link rel="stylesheet" href="<?= base_url('public/css/theme.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-slider.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/select2.min.css') ?>">        
        <link rel="stylesheet" href="<?= base_url('public/js/owl/owl.carousel.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/owl/owl.theme.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/notifIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/fancybox/jquery.fancybox.css') ?>">
        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">        	
        
        <script>var LOADER = '<div class="text-center"><img src="<?= base_url('public/images/preloader.gif') ?>" /></div>';</script>
        <script>var SITE_LANG = '<?= $this->config->item('languages')[$this->langID]['Slug'] ?>';</script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url('public/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/owl/owl.carousel.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/fancybox/jquery.fancybox.pack.js') ?>"></script>
        <script src="<?= base_url('public/js/notifIt.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/validate.js') ?>"></script>
        <script>
            // error messages
            jQuery.extend(jQuery.validator.messages, {
                required: "Completați câmpul",
                remote: "Please fix this field.",
                email: "Vă rugăm să introduceți o adresă de e-mail validă",
                url: "Please enter a valid URL.",
                date: "Please enter a valid date.",
                dateISO: "Please enter a valid date (ISO).",
                number: "Please enter a valid number.",
                digits: "Please enter only digits.",
                creditcard: "Please enter a valid credit card number.",
                equalTo: "Please enter the same value again.",
                accept: "Please enter a value with a valid extension.",
                maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                minlength: jQuery.validator.format("Please enter at least {0} characters."),
                rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
                range: jQuery.validator.format("Please enter a value between {0} and {1}."),
                max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
                min: jQuery.validator.format("Vă rugăm să introduceți o valoare mai mare sau egală cu {0}")
            });
        </script>

        <script type="text/javascript" src="<?= base_url('public/js/select2.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/bootstrap-datepicker.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/jquery.tmpl.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/moment.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/bootstrap-slider.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('public/js/jquery-ui/jquery-ui.min.js') ?>"></script>

        <script src="<?= base_url('public/js/scripts.js') ?>"></script>
    </head>
    <body>
        <header class="header">
            <div class="top-bar hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-4">
                            <a href="#"><i class="fa fa-home"></i> <?=lang('HeaderAddress')?> </a>
                        </div>                    
                        <div class="col-md-5">
                            <ul class="ul-inline">
                                <li><a href="<?= site_url('about') ?>"><?= lang("About") ?></a></li>
                                <li><a href="<?= site_url('contacts') ?>"><?= lang("Contacts") ?></a></li>
                                <?php $selected = ""; ?>
                                <li><a href="#"><strong></strong></a></li>
                                <?php foreach (get_instance()->config->item('languages') as $lang) { ?>
                                    <?php
                                    if (get_instance()->config->item('language') == $lang['LangFile'])
                                        $selected = " selected";
                                    else
                                        $selected = "";
                                    ?>
                                    <li><a href="<?= switchLang($lang['Slug']) ?>" <?= $selected ?>><strong><?= $lang['Name'] ?></strong></a></li>                                                         
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <a class="logo-block" href="<?= site_url('') ?>"><img src="<?= $this->data['settings']['logo'] ? base_url('public/uploads/settings/' . $this->data['settings']['logo']) : base_url('public/images/logo.png') ?>" alt="BIOX COMERT"></a>
                        </div>
                        <div class="col-md-7 col-sm-8">
                            <div class="row">
                                <div class="header-contact header-slogan col-md-4 hidden-sm hidden-xs">
                                    <strong><?= lang('HeaderTextSection')?></strong>
                                </div>
                                <div class="header-contact col-md-4 col-sm-6 hidden-xs">
                                    <p><i class="fa fa-clock-o"></i> <?=lang('Schedule')?></p>
                                    <strong><?=lang('ScheduleText')?></strong>
                                </div>
                                <div class="header-contact col-md-4 col-sm-6 hidden-xs">
                                    <p><i class="fa fa-envelope"></i> <?=lang('WriteUsMessage')?></p>
                                    <strong><a href="mailto:<?= $this->data['settings']['email_info'] ?>"><?= $this->data['settings']['email_info'] ?></a></strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <a href="tel:<?= $this->data['settings']['phone_number'] ?>" class="hidden-sm hidden-xs header-phone"><i class="fa fa-phone"></i> <?= $this->data['settings']['phone_number'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="header-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="hidden-lg hidden-md open-menu"><i class="fa fa-bars"></i></a>
                            <a href="tel:<?= $this->data['settings']['phone_number'] ?>" class="pull-right hidden-lg hidden-md header-phone"><i class="fa fa-phone"></i> <?= $this->data['settings']['phone_number'] ?></a>
                            <ul class="navigation">
                                <li><a href="<?= site_url('') ?>"><?= lang("Home") ?></a></li>
                                <li><a href="<?= site_url($this->categoriesMenu['products']->Link) ?>"><?=$this->categoriesMenu['products']->Name?> <i class="fa fa-caret-down"></i></a>
                                    <ul> 
                                        <?php if(count($this->categories['product'])>0) foreach($this->categories['product'] as $prod){ ?>
                                        <li><a href="<?=site_url($prod->Link)?>"><?=$prod->Name?></a></li>                                        
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li><a href="<?= site_url($this->categoriesMenu['aplication']->Link) ?>"><?=$this->categoriesMenu['aplication']->Name?> <i class="fa fa-caret-down"></i></a>
                                    <ul>                                    
                                        <?php if(count($this->categories['aplication'])>0) foreach($this->categories['aplication'] as $cat){ ?>
                                        <li><a href="<?=site_url($cat->Link)?>"><?=$cat->Name?></a></li>                                        
                                        <?php } ?>
                                    </ul>
                                </li>

                                <?= menu($this->menu[$this->menuType], "navigation"); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>






