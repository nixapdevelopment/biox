<footer class="footer">
    <div class="alternative-v-line"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <h4 class="mini-title"><?=lang('Contacts')?></h4>
                <ul class="footer-nav">
                    <li>BIOX COMERT SRL <br><?=lang('FooterRepublicOf')?> <br><?= lang('FooterAddress')?><br><br></li>
                    <li>
                        <p><?=lang('Phone')?>: +373 22 74-73-92</p>
                        <p>Orange: +373 69 103-819</p>
                        <p><?=lang('Email')?>: <a href="mailto:info@biox.md"><?= $this->data['settings']['email_info'] ?></a></p>
                    </li>
                </ul>
            </div>
            <div class="footer-newsletter col-md-3 col-sm-6 col-xs-12">
                <h4 class="mini-title"><?=lang('Newsletter')?></h4>
                <p><?= lang('NewsletterText')?></p>
                <form id="subscribe-form">
                    <input type="Email" name="Email" placeholder="<?=lang('NewsletterInputPlaceholder')?>">
                    <button type="submit"><i class="fa fa-envelope"></i></button>
                </form>
                <div class="footer-social">
                    <a href=""><i class="fa fa-facebook-square"></i></a>
                    <a href=""><i class="fa fa-twitter-square"></i></a>
                    <a href=""><i class="fa fa-google-plus-square"></i></a>
                </div>
            </div>
            <div class="col-md-3 hidden-xs hidden-sm">                
                <h4 class="mini-title"><?=$this->categoriesMenu['products']->Name?></h4>
                <ul class="footer-nav"> 
                    <?php if(count($this->categories['product'])>0) foreach($this->categories['product'] as $prod){ ?>
                    <li><a href="<?=site_url($prod->Link)?>"><?=$prod->Name?></a></li>                                        
                    <?php } ?>
                </ul>                
            </div>
            <div class="col-md-3 hidden-xs hidden-sm">
                <h4 class="mini-title"><?=$this->categoriesMenu['aplication']->Name?></h4>
                <ul class="footer-nav">                                                 
                    <?php if(count($this->categories['aplication'])>0) foreach($this->categories['aplication'] as $cat){ ?>
                    <li><a href="<?=site_url($cat->Link)?>"><?=$cat->Name?></a></li>                                        
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="footer-copy">
            <div class="row">
                <div class="col-md-6"><?= $this->data['settings']['copyright'] ?></div>
                <div class="col-md-6">
                    <span class="by_nixap">by <a href="http://nixap.eu" target="_blank" title="">nixap.eu</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>