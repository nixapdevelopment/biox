        <footer class="footer">
            <div class="footer-contacts">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="email" placeholder="Abonează-te la noutăți" />
                            <button type="submit"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-md-6 text-right">
                            <tel>+373 22 223-456</tel>
                            <tel>+373 22 423-456</tel>
                            <a class="footer-contacts-btn" role="button" aria-expanded="false" data-toggle="collapse"  aria-controls="footer-map" href="#footer-map">Hartă și adresă</a>
                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>                        
                    </div>
                </div>
                <div class="collapse" id="footer-map">
                    <iframe width="100%" height="400" frameborder="0" src="https://point.md/ru/map/47.059305715302116/28.770532608032227/14/57e7c0ebbbdb22000a06f99b?embed=1"></iframe>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <nav class="col-md-2">
                        <ul>
                            <li>Pentru cumpărător</li>
                            <li><a href="">Achitare</a></li>
                            <li><a href="">Livrare</a></li>
                            <li><a href="">Garanție</a></li>
                            <li><a href="">ANGRO</a></li>
                        </ul>
                    </nav>
                    <nav class="col-md-2">
                        <ul>                        
                            <li>Companie</li>
                            <li><a href="">Despre AGM</a></li>
                            <li><a href="">Catalog</a></li>
                            <li><a href="">Noutăți</a></li>
                            <li><a href="">Contacte</a></li>
                        </ul>
                    </nav>
                    <div class="col-md-8 footer-details">
                        <div class="footer-socials">
                            <a href=""><img src="<?= base_url('public/images/fb.png') ?>"></a>
                            <a href=""><img src="<?= base_url('public/images/ok.png') ?>"></a>
                            <a href=""><img src="<?= base_url('public/images/gplus.png') ?>"></a>
                            <a href=""><img src="<?= base_url('public/images/in.png') ?>"></a>
                        </div>
                        <div class="footer-copy">
                            &copy; 2016 - VICPRIM-PLUS SRL 
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>