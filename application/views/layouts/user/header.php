<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>AGM</title>
        <link rel="stylesheet" href="<?= base_url('public/css/styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/admin_styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-theme.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/fancybox/jquery.fancybox.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/easyTree.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/notifIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/select2.min.css') ?>">
        
        <script>var LOADER = '<div class="text-center"><img src="<?= site_url('public/images/preloader.gif') ?>" /></div>';</script>
        <script>var SITE_LANG = '<?= $this->config->item('languages')[$this->langID]['Slug'] ?>';</script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="<?= base_url('public/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.pack.js') ?>"></script>        
        <script src="<?= base_url('public/js/easyTree.min.js') ?>"></script>
        <script src="<?= base_url('public/js/validate.js') ?>"></script>
        <script src="<?= base_url('public/js/notifIt.min.js') ?>"></script>
        <script src="<?= base_url('public/js/select2.min.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-datepicker.js') ?>"></script>
        <script src="<?= base_url('public/js/ckeditor/ckeditor.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery.tmpl.min.js') ?>"></script>
        <script src="<?= base_url('public/js/moment.js') ?>"></script>
    </head>
    <body>
        <header class="header">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="left-top-bar col-md-4">
                            <i class="fa fa-phone-square"></i>  <a class="top-contact" href="">+373 22 123-456</a>
                            <i class="fa fa-envelope-square"></i> <a class="top-contact" href="">office@agm.md</a>
                        </div>
                        <div class="right-top-bar col-md-8">
                            <div class="user-bar">
                                <a href="<?= site_url('login') ?>">Login</a> |
                                <a href="<?= site_url('registration') ?>">Înregistrare</a>
                            </div>
                            <div class="lang-bar dropdown">
                                <a href="" id="change-lang" data-toggle="dropdown">Selectează limba</a>
                                <ul class="dropdown-menu pull-right" aria-labelledby="change-lang">
                                    <li><a href="/">Română</a></li>
                                    <li><a href="/ru/">Русский</a></li>
                                    <li><a href="/en/">English</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="header-bar">
                    <div class="row">
                        <div class="header-logo col-md-3">
                            <a href="<?= site_url() ?>">
                                <img src="<?= base_url('public/images/logo.png') ?>" alt="">
                                <span><?= lang('Slogan') ?></span>
                            </a>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="acoperire col-md-2">
                            <a href="">
                                <i class="fa fa-map-marker"></i>
                                <h4>Acoperire</h4>
                            </a>
                        </div>
                        <div class="cart-bar col-md-3">
                            <a class="cart-block" href="<?= site_url('cart') ?>">
                                <i class="fa fa-shopping-basket"></i>
                                <span>0.00 lei</span>
                            </a>
                            <div class="search-block" href="">
                                <i class="fa fa-search"></i>
                                <input type="text" name="" value="" placeholder="Căutare" />
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="main-menu">
                    <?= menu($this->menu[$this->menuType]) ?>
                </nav>
            </div>
        </header>
        <div class="container">
            <div class="sub-header">
                <div class="row">
                    <div class="col-md-6">
                        <?= breadscrumbs($this->breadscrumbs) ?>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="search-bar">
                                <input type="text" name="" placeholder="caută pe site ..." />
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>