<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?= $ProjectArticle->Title ?></h1>
        <br />
        <div>
            <?= $ProjectArticle->Text ?>            
        </div>
        <div>           
            <p><strong>Material:</strong> <?= $ProjectArticle->Material ?></p>
            <p><strong>Constructor:</strong> <?= $ProjectArticle->Constructor ?></p>
            <p><strong>Beneficiar:</strong> <?= $ProjectArticle->Beneficiar ?></p>                  
        </div>

        <div class="row">
            <div class="col-md-<?= count($ProjectFiles) ? 9 : 12 ?>">
                <div class="row">
                    <?php foreach ($ProjectImages as $ProjectImage) { ?>
                        <div class="col-md-4">
                            <a class="fancybox" rel="news-image" href="<?= base_url('public/uploads/projects/' . $ProjectImage->Image) ?>">
                                <img class="img-thumbnail" src="<?= base_url('public/uploads/projects/' . $ProjectImage->Thumb) ?>" />
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if (!empty($ProjectFiles)) { ?>
                <div class="col-md-3">
                    <div class="list-group">
                        <?php foreach ($ProjectFiles as $ProjectFile) { ?>
                        <a target="_blank" href="<?= base_url('public/uploads/projects/' . $ProjectFile->Path) ?>" class="list-group-item"><?= $ProjectFile->Name ?></a>
                        <?php } ?>
                    </div>
                </div>     
            <?php } ?>
        </div>
    </div>
</section>