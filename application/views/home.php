<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="slideshow ">              
    <?php foreach ($slides as $slide) { ?>                    
        <div class="slideshow-item item">
            <a href="<?= empty($slide->Link) ? '#' : $slide->Link ?>">
                <img src="<?= base_url('public/uploads/sliders/' . $slide->Image) ?>" alt="" />
            </a>
        </div>
    <?php } ?>  
</section>

<section class="home-about">
    <div class="container">
        <div class="home-about-bg">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-title"><?= lang('HomeHeadTitle')?></h1>
                    <p>
                        <?= lang('HomeHeadText')?>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="<?= site_url($this->categoriesMenu['products']->Link) ?>" class="home-about-btn"><i class="fa fa-tasks"></i> <?=$this->categoriesMenu['products']->Name?></a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="<?= site_url($this->categoriesMenu['aplication']->Link) ?>" class="home-about-btn"><i class="fa fa-bars"></i> <?=$this->categoriesMenu['aplication']->Name?></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



    <section class="home-works">
        <div class="container">
            <div class="page-title text-center"><?=lang('LatestWorks')?></div>
            <div class="row">
                <?php
                if (count($this->data['projects']) > 0) {
                    foreach ($this->data['projects'] as $projects) {
                        ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="<?= site_url($projects['Link']); ?>" class="hw-item">
                        <strong><?= $projects['Title']; ?></strong>
                        <div class="hw-thumb"><img src="<?= base_url('public/uploads/projects/'.$projects['Thumb']) ?>" alt="<?= $projects['Title']; ?>" title="<?= $projects['Title']; ?>"></div>
                        <table class="table table-bordered">
                            <tr>
                                <td><strong><?=lang('Material')?>:</strong></td>
                                <td><?= $projects['Material']; ?></td>
                            </tr>
                            <tr>
                                <td><strong><?= lang('Constructor')?>:</strong></td>
                                <td><?= $projects['Constructor']; ?></td>
                            </tr>
                            <tr>
                                <td><strong><?=lang('Recipient')?>:</strong></td>
                                <td><?= $projects['Beneficiar']; ?></td>
                            </tr>
                        </table>
                    </a>
                </div>
                <?php }
                }  ?>
                
            </div>
            
            <a href="<?= site_url("projects"); ?>" class="view-all"><?=lang('SeeAll')?></a>
            
        </div>
    </section>


<section class="home-news">
    <div class="container">
        <div class="page-title text-center"><?= lang("News") ?></div>
        <div class="row">
            <?php
            if (count($this->data['news']) > 0) {
                foreach ($this->data['news'] as $news) {
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <a href="<?= site_url($news['Link']); ?>" class="hn-item" title="<?= $news['Title']; ?>">
                            <strong><?= $news['Title']; ?></strong>
                            <p><?= mb_substr(strip_tags($news['Text']), 0, 140) ?>...</p>
                            <time><?= date('d.m.Y', strtotime($news['Date'])) ?></time>
                        </a>
                    </div>

                    <?php
                }
            } else {
                ?>
                <p>Nu sunt noutati.</p>
            <?php } ?>
        </div>
        <a href="<?= site_url("news"); ?>" class="view-all"><?= lang("AllNews") ?></a>
    </div>
</section>



<section class="home-seo">
    <div class="container">
        <div class="page-title text-center"><?= $page->Title ?></div>
        <?= $page->Text ?>
    </div>
</section>  
