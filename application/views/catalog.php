<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Perform Categories & Subcategories



    if(count($categories) > 0) {
        
    $as = $categories;
    $new_as = array();
    foreach ($as as $ind => $a) {
        $new_as[$a['ID']] = $a;
        $new_as[$a['ID']]['children'] = array();
    }

    foreach ($new_as as $ind => &$new_a) {
        $parent = $new_as[$ind]['ParentID'];
        if(isset($new_as[$parent])){
            $new_as[$parent]['children'][] = &$new_as[$ind];
        }
    }

    function print_children($children, $level = 0)
    {
        foreach ($children as $child) {
            echo "<ul>";
            echo "<li><a href='".site_url($child['Link'])."'>".$child['Name']."</a></li>";
            print_children($child['children'], $level + 1);
            echo "</ul>";
        }
    }

}

?>

<section class="bread">
    <div class="container">        
        <?= breadscrumbs($this->breadscrumbs) ?>        
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="left-menu">
                    <ul>                                                
                        <?php foreach ($new_as as $ind => &$new_a) { 
                                if ($new_a['ParentID'] == 0) {                                    
                                    echo "<li><a href=".site_url($new_a['Link'])."><div class='left-img'><img src='".base_url("public/uploads/categories/".$new_a['Image'])."' alt='".$new_a['Name']."' /></div> ".$new_a['Name']."</a></li>";
                                    print_children($new_a['children'],1);
                                }
                            } 
                        ?>          
                        
                    </ul>
                </div>
            </div>

            <div class="col-md-9">
                <div class="page-description">
                    <h1><?= lang('Catalog') ?></h1>
                    <div class="row">
                        <?php foreach ($categories as $categ) { ?>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                            <div class="black-bg product-item">
                                <a href="<?= site_url($categ['Link']) ?>" title="<?= $categ['Name']; ?>">
                                    <div class="product-thumb">
                                        <img src="<?= base_url('public/uploads/categories/' . $categ['Image']) ?>" />
                                    </div>
                                    <p><?= $categ['Name']; ?></p>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>