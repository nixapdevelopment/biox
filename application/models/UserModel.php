<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
    
    public function getForLogin($username, $password)
    {
        return $this->db->get_where('User', ['UserName' => $username, 'Password' => $password], 1)->row();
    }
    
    public function getByUsername($username)
    {
        return $this->db->get_where('User', ['UserName' => $username], 1)->row();
    }
    
    public function getByID($user_id)
    {
        return $this->db->get_where('User', ['ID' => $user_id], 1)->row();
    }
    
}