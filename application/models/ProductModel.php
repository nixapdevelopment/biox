<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model
{
    
    private function productSelect($langID = 1)
    {
        $this->db->select("SQL_CALC_FOUND_ROWS *, p.ID as ProductID, pl.Name as Name, cl.Name as CategoryName, pl.Text as ProductText, pl.Keywords, pl.Description, IF(d.ID > 0, 1, 0) as `IsPromo`, p.Price as `OldPrice`, u.Link as Link, cu.Link as CategoryLink, 
            CASE d.PromoType 
                WHEN 'Percent' THEN ROUND(p.Price - (p.Price * d.Value / 100), 2) 
                WHEN 'Amount' THEN ROUND(p.Price - d.Value, 2) 
                WHEN 'Price' THEN ROUND(d.Value, 2)
                ELSE ROUND(p.Price, 2)
            END AS `Price`
        ", false);
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $langID, 'LEFT');
        $this->db->join('CategoryLang as cl', 'p.CategoryID = cl.CategoryID and cl.LangID = ' . $langID, 'LEFT');
        $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'LEFT');
        $this->db->join("Url as cu", "cu.ObjectID = p.CategoryID and cu.Type = 'Category'", 'LEFT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
        $this->db->join("Discount as d", "d.EntityID = p.ID and d.EntityType = 'Product' and NOW() >= d.StartDate and NOW() <= d.EndDate and d.PromoStatus = 1", 'LEFT');
    }

    public function getPromoProducts($langID, $limit = 4, $offset = 0)
    {
        $this->productSelect($langID);
        $this->db->where('d.PromoStatus', 1);
        $this->db->where('p.Status', 'Active');
        $this->db->group_by('p.ID');
        $this->db->order_by('Price', 'ASC');
        $this->db->limit($limit);
        $this->db->offset($offset);
        
        return $this->db->get()->result();
    }
    
    public function getLastProducts($langID, $limit = 4, $offset = 0)
    {
        $this->productSelect($langID);
        $this->db->where('p.Status', 'Active');
        $this->db->where('p.Created >', date('c', strtotime('-2 weeks')));
        $this->db->group_by('p.ID');
        $this->db->order_by('p.Created', 'DESC');
        $this->db->limit($limit);
        $this->db->offset($offset);
        
        return $this->db->get()->result();
    }
    
    public function getByID($ID, $langID = 1)
    {
        $this->productSelect($langID);
        $this->db->where('p.Status', 'Active');
        $this->db->where('p.ID', $ID);
        $this->db->group_by('p.ID');
        $this->db->limit(1);
                
        return $this->db->get()->row();
    }
    
    public function getAnalogs($analogID, $productID, $langID = 1)
    {
        $this->productSelect($langID);     
        $this->db->where('p.Status', 'Active');
        $this->db->where('p.AnalogGroup', $analogID);
        $this->db->where('p.ID!=', $productID);
        $this->db->order_by('p.Created', 'DESC');
                
        return $this->db->get()->result();
    }
    
}