<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryModel extends CI_Model
{    
    public function getCategories($langID, $noSubcategoires = false /* set TRUE to hide subcategories */)
    {
        $this->db->select('c.ID, c.ParentID, c.Image, cl.Name, cl.Title, u.Link');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', "cl.LangID = $langID and c.ID = cl.CategoryID", 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = c.ID and u.Type = 'Category'", 'LEFT');
        
        if($noSubcategoires) $this->db->where('c.ParentID', '0');
        
        $this->db->where('c.Status', 'Active');        
        $this->db->order_by('cl.Name');
        $categories = $this->db->get()->result_array();
        
        return $categories;        
    }
    
    public function getTree($langID, $frontend = false, $categoryPage = false)
    {
        
        $categories = $this->getCategories($langID);
        
        $tree = $this->categoriesToTree($categories);
        
        return $frontend ? $this->buildFrontendTree($tree, true, $categoryPage) : $this->buildTree($tree);
    }
            
    public function categoriesToTree(&$categories)
    {
        $map = array(
            0 => array('subcategories' => array())
        );

        foreach ($categories as &$category)
        {
            $category['subcategories'] = array();
            $map[$category['ID']] = &$category;
        }
        
        foreach ($categories as &$category)
        {
            $map[$category['ParentID']]['subcategories'][] = &$category;
        }

        return $map[0]['subcategories'];
    }
    
    private function buildTree($array)
    {
        $return = '<ul>';
        foreach ($array as $row)
        {
            $return .= '<li cat-id="' . $row['ID'] . '">' . $row['Name'];
            if (count($row['subcategories']) > 0)
            {
                $return .= $this->buildTree($row['subcategories']);
            }
            $return .= '</li>';
        }
        $return .= '</ul>';
        
        return $return;
    }
    
    private function buildFrontendTree($array, $first = false, $currentCategory = false)
    {
        if ($first)
        {
            array_unshift($array, [
                'ID' => '-1',
                'Name' => lang('Catalog'),
                'Link' => 'catalog',
                'ParentID' => 0,
                'subcategories' => []
            ]);
        }
        
        $return = '';
        foreach ($array as $row)
        {
            $return .= '<li><a href="' . site_url( $row['Link']) . '">' . $row['Name'] . '</a>';
            if (count($row['subcategories']) > 0 && ($first && $currentCategory == $row['ID']))
            {
                if (!$currentCategory)
                {
                    $return .= '<div class="sub-categories "><h3>' . $row['Name'] . '</h3>';
                }
                $return .= $this->buildFrontendTree($row['subcategories'], false, $currentCategory);
                if (!$currentCategory)
                {
                    $return .= '<div class="col-md-8 category-img"><img src="' . base_url('public/uploads/categories/' . $row['Image']) . '" /></div></div>';
                }
            }
            $return .= '</li>';
        }
        $return .= '';
        
        return $return;
    }
    
    public function getByParent($id, $langID)
    {
        $this->db->select('c.ID, c.Image, cl.Name, u.Link');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', "cl.LangID = $langID and c.ID = cl.CategoryID", 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = c.ID and u.Type = 'Category'", 'LEFT');
        $this->db->where('c.ParentID', $id);
        $this->db->order_by('cl.Name');
        
        return $this->db->get()->result();
    }
    
    public function getByID($id, $langID = false)
    {
        $langJoin = $langID > 0 ? "cl.LangID = $langID and c.ID = cl.CategoryID" : "c.ID = cl.CategoryID";
        
        $this->db->select('c.ID, c.ParentID, c.Image, cl.Name, cl.Title, cl.Text, cl.Keywords, cl.Description, u.Link');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', $langJoin, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = c.ID and u.Type = 'Category'", 'LEFT');
        $this->db->where('c.ID', $id);
        if ($langID > 0) $this->db->limit(1);
        $query = $this->db->get();
        
        return $langID > 0 ? $query->row() : $query->result();
    }
    
}