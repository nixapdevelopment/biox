<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectsModel extends CI_Model
{
    
    private function projectSelect($langID)
    {
        $this->db->select('*');
        $this->db->from('Projects as p');
        $this->db->join('ProjectsLang as pl', "pl.ProjectID = p.ID and pl.LangID = $langID");
        $this->db->join('ProjectsImage as pi', "pi.ProjectID = p.ID and pi.IsMain = 1");
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Project'");
    }
    
    public function getLast($langID = 1, $limit = 4, $offset = 0)
    {
        $this->projectSelect($langID);
        $this->db->where('p.Status', 'Active');
        $this->db->order_by('p.Date', 'DESC');
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get()->result();
    }
    
    public function getByID($id, $langID = 1)
    {
        $this->projectSelect($langID);
        $this->db->where('p.ID', $id);
        $this->db->where('p.Status', 'Active');
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function getImagesByProjectID($id)
    {
        $this->db->where('ProjectID', $id);
        return $this->db->get('ProjectsImage')->result();
    }
    
    public function getFilesByProjectID($id)
    {
        $this->db->where('EntityID', $id);
        $this->db->where('EntityType', 'Projects');
        return $this->db->get('File')->result();
    }
    
    public function getProjects($langID, $frontend = false)
    {
        $this->db->select('p.ID, pl.*, u.Link as Link, p.Date, pi.Image as Image, pi.Thumb as Thumb');
        $this->db->from('Projects as p');
        $this->db->join('ProjectsLang as pl', "pl.LangID = $langID and p.ID = pl.ProjectID", 'LEFT');
        $this->db->join('ProjectsImage as pi', "pi.ProjectID = p.ID and pi.IsMain = 1", 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Project'", 'LEFT');
        $this->db->where('p.Status', 'Active');
        $this->db->order_by('p.Date', 'DESC');
        
        if($frontend) $this->db->limit(6);
        
        $projects = $this->db->get()->result_array();
                
        return $projects;
    }
    
}