<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AppController extends CI_Controller
{
    
    public $header = null;
    
    public $footer = null;
    
    public $langID = 1;
    
    public $breadscrumbs = [];
    
    public $data = [];


    public function __construct()
    {
        parent::__construct();
       
        $this->parse_lang();
    }
    
    public function parse_lang()
    {
        $ar = explode('/', $_SERVER['REQUEST_URI']);
        $ar = array_filter($ar);
        
        $langs = get_instance()->config->item('languages');

        if (isset($ar[1]))
        {
            foreach ($langs as $langID => $lang)
            {
                if ($ar[1] == $lang['Slug'])
                {
                    $this->langID = $langID;
                    $this->config->set_item('language', $lang['LangFile']);
                    $this->lang->load('common', $lang['LangFile']);
                    break;
                }
                else
                {
                    $this->lang->load('common', 'romanian');
                }
            }
        }
        else
        {
            redirect(site_url('/ro' . $_SERVER['REQUEST_URI']));
            $this->langID = 1;
            $this->config->set_item('language', 'romanian');
            $this->lang->load('common', 'romanian');
        }
    }

    public function render($view, $data = [])
    {
        $this->load->view($this->header);
        $this->load->view($view, $data);
        $this->load->view($this->footer);
    }
    
    public function addBreadscrumb($link, $text)
    {
        $this->breadscrumbs[$link] = $text;
    }
    
}

class FrontController extends AppController
{
    
    public $header = 'layouts/header';
    
    public $footer = 'layouts/footer';
    
    public $_user = null;
    
    public $shopType = 'Retail';
    
    public $menu;
    
    public $menuType = '1';
    
    public $categories;
    public $categoriesMenu;
    
    public $accountLink = '';
    
    public $cartAmount = 0.00;
    
    public $cartProductCount = 0;
    
    // seo
    public $Title = 'BIOX COMERT';
    public $Keywords = 'BIOX COMERT';
    public $Description = 'BIOX COMERT';


    public function menu()
    {
        $this->menu = [
            'Admin' => [
                'admin/index' => lang('Dashboard'),
                'admin/categories' => lang('Categories'),
                'admin/products' => lang('Products'),
                'admin/news' => lang('News'),
                'admin/pages' => lang('Pages'),
                'admin/projects' => lang('Projects'),
                'admin/slider' => 'Slider',
                'admin/menus' => lang('Menus'),
                'admin/feedback' => lang('Feedback'),
                'admin/subscribers' => lang('Newslatter'),
                'admin/settings' => lang('Settings')
                
            ],
            'User' => [
                'user/index' => lang('Dashboard'),
                'user/orders' => lang('Orders'),
                'user/settings' => lang('Settings'),
            ],
            'Manager' => [
                'admin/index' => lang('Dashboard'),
                'admin/users' => lang('Users'),
                'admin/orders' => lang('Orders'),
            ]
        ];
    }

    public function __construct()
    {
        parent::__construct();
        
        $loged_user_id = $this->session->userdata('UserID');
        
        if ($loged_user_id)
        {
            $this->load->model('UserModel');
            $this->_user = $this->UserModel->getByID($loged_user_id);
            $this->shopType = $this->_user->Type;
            
            switch ($this->_user->Type)
            {
                case 'Admin':
                    $this->accountLink = 'admin';
                    break;
                case 'Manager':
                    $this->accountLink = 'admin';
                    break;
                case 'Angro':
                    $this->accountLink = 'user';
                    break;
                case 'Retail':
                    $this->accountLink = 'user';
                    break;
                default:
                    $this->accountLink = 'login';
                    break;
            }
        }
        
        $this->addBreadscrumb(site_url(), lang('HomePage'));
        
        $this->menu();
        
        $res = $this->db->select('*')
            ->from('MenuItem as mi')
            ->join('MenuItemLang as mil', 'mil.MenuItemID = mi.ID and mil.LangID = ' . $this->langID)
            ->join('Url as u', 'u.ObjectID = mi.EntityID and u.Type = mi.EntityType')
            ->order_by('mi.MenuID, mi.Position')
            ->get()->result();
        
        foreach ($res as $row)
        {
            $this->menu[$row->MenuID][$row->Link] = $row->Name;
        }
        
        $settings = $this->db->select('ID, Name, Value')->from('Settings')->get()->result();
        
        $setting_arr = [];
        foreach ($settings as $setID => $setVal){
            $setting_arr[$setVal->Name] = $setVal->Value;
        }
        
        $this->data['settings'] = $setting_arr;
        
        $this->load->model('CategoryModel');
        $this->categories['product'] = $this->CategoryModel->getByParent(1, $this->langID);
        $this->categories['aplication'] = $this->CategoryModel->getByParent(3, $this->langID);
        
        $this->categoriesMenu['products'] = $this->CategoryModel->getByID(1, $this->langID);
        $this->categoriesMenu['aplication'] = $this->CategoryModel->getByID(3, $this->langID);
        
    }
    
}

class AdminController extends FrontController
{
    
    public $header = 'layouts/admin/header';
    
    public $footer = 'layouts/admin/footer';
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user) || !in_array($this->_user->Type, ['Admin', 'Manager']))
        {
            redirect('login');
        }
    }
    
}

class UserController extends FrontController
{
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user))
        {
            redirect('login');
        }
        
        if ($this->_user->Status == 'NotConfirmed')
        {
            redirect('wait-confirmation');
        }
    }
    
}