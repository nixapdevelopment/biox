<?php

$config['languages'] = [
    1 => [
        'Name' => 'Română',
        'Slug' => 'ro',
        'LangFile' => 'romanian'
    ],
    2 => [
        'Name' => 'Русский',
        'Slug' => 'ru',
        'LangFile' => 'russian'
    ],
];