
$(document).ready(function(){
    
    $('.open-menu').click(function() {
        $('.navigation').toggle();
    });
    
     $(".slideshow").owlCarousel({
        singleItem : true,
        autoPlay : true
    });
    
    $('#subscribe-form').submit(function(event){
        event.preventDefault();
        
        var form = $(this);
        
        $.post('/main/subscribe', form.serialize(), function(html){
            form.append(html);
            form.trigger('reset');
        });
    });
    
    $('a.fancybox').fancybox();
    
    $(".owl-carousel").owlCarousel({
 
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
	  autoPlay : true,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
	});
  
	$(".footer-cars").owlCarousel({
 
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
	  autoPlay : true,
	  pagination : false,
      paginationSpeed : 400,
      singleItem:false,
 
      // "singleItem:true" is a shortcut for:
      items : 10
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
	});
  
	$( ".mobile-nav-btn" ).click(function() {
		$( ".mobile-nav" ).toggle();
	});
        
        $('#lang_select').change(function() {
            var uri = $(this).val();
            window.location.href = uri;
        });  
    
});

   
        
    $(document).on('click', 'a[oid]', function(){

        var orderID = $(this).attr('oid');
        var status = '';

        switch ($(this).attr('class'))
        {
            case 'text-warning':
                status = 'Pending';
                break;
            case 'text-danger':
                status = 'Canceled';
                break;
            case 'text-success':
                status = 'Paid';
                break;
            case 'text-primary':
                status = 'Closed';
                break;
        }

        $.post('/admin/change_order_status', {orderID: orderID, status: status}, function(){
            notif({
                msg: 'Status schimbat',
                type: 'success',
                position: "right"
            });
            $('tr[data-oid=' + orderID + '] td span[data-html="true"]').text(status);
        });
    });
        
    $(document).on('click', function (e) {
        $('[data-toggle="popover"],[data-original-title]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
            }

        });
    });
        
    $(function(){
        // Enables popover
        $("[data-toggle=popover]").popover();

        setTimeout(function(){
            $('.alert.alert-success').slideUp(500, function(){
                $(this).remove();
            });
        }, 4000);

        $('.select-2').select2({
            'theme': 'bootstrap'
        });

        $('.datepicker').datepicker({
            format: 'dd.mm.yyyy'
        });

        $('a.fancybox').fancybox();
    });
        
        